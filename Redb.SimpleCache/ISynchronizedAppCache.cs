﻿using System.Threading;
using LazyCache;

namespace Redb.SimpleCache
{
    public interface ISynchronizedAppCache : IAppCache
    {
        /// <summary>
        ///     //NOTE: kind of code smell with respect to visibility of sync members, but accepted by ms approach. See
        ///     <see cref="System.Array.SyncRoot" /> for example
        /// </summary>
        SemaphoreSlim SemaphoreSlim { get; }
    }
}