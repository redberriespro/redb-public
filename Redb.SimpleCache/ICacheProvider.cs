﻿using System;
using System.Threading.Tasks;

namespace Redb.SimpleCache
{
    public interface ICacheProvider
    {
        Task<T> GetAsync<T>(ICacheKey key) where T : class;

        Task SetAsync<T>(ICacheKey key, T obj, TimeSpan? memoryCacheExpiration = null, TimeSpan? redisExpiration = null) where T : class;

        Task RemoveAsync<T>(ICacheKey key) where T : class;
    }
}
