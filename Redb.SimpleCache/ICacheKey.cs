﻿namespace Redb.SimpleCache
{
    public interface ICacheKey
    {
        string Key { get; }
    }
}
