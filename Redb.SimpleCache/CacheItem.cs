﻿namespace Redb.SimpleCache
{
    public class CacheItem<T>
    {
        public T Obj { get; set; }
    }
}
