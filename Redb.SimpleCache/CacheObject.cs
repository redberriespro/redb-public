﻿using System;

namespace Redb.SimpleCache
{
    public class CacheObject<T> where T : class
    {
        public T Object { get; set; }

        public TimeSpan InMemoryCacheExpiration { get; set; }

        public TimeSpan DistributedCacheExpiration { get; set; }

        public CacheObject()
        {

        }

        public CacheObject(T obj, TimeSpan inMemoryCacheExpiration, TimeSpan distributedCacheExpiration)
        {
            Object = obj;
            InMemoryCacheExpiration = inMemoryCacheExpiration;
            DistributedCacheExpiration = distributedCacheExpiration;
        }
    }
}
