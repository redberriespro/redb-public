﻿
namespace Redb.SimpleCache
{
    public class CacheKey : ICacheKey
    {
        private readonly string _key;

        public string Key => $"{_key}";

        public CacheKey(string key)
        {
            _key = key;
        }
    }
}
