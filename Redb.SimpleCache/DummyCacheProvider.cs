using System;
using System.Threading.Tasks;

namespace Redb.SimpleCache
{
    public class DummyCacheProvider : ICacheProvider
    {
        public async Task<T> GetAsync<T>(ICacheKey key) where T : class
        {
            return null;
        }

        public async Task SetAsync<T>(ICacheKey key, T obj, TimeSpan? memoryCacheExpiration = null, TimeSpan? redisExpiration = null) where T : class
        {
        }

        public async Task RemoveAsync<T>(ICacheKey key) where T : class
        {
        }
    }
}