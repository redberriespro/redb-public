﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Redb.SimpleCache
{
    public class CacheProvider : ICacheProvider
    {
        private readonly IDistributedCache _distributedCache;
        private readonly IMemoryCache _memoryCache;

        private readonly ILogger<CacheProvider> _logger;

        private readonly TimeSpan _defaultInMemoryCacheExpiration = new TimeSpan(TimeSpan.TicksPerMinute * 5);
        private readonly TimeSpan _defaultDistributedCacheExpiration = new TimeSpan(TimeSpan.TicksPerMinute * 30);

        public CacheProvider(IDistributedCache distributedCache, IMemoryCache memoryCache, ILogger<CacheProvider> logger)
        {
            _distributedCache = distributedCache;
            _memoryCache = memoryCache;

            _logger = logger;
        }

        public async Task<T> GetAsync<T>(ICacheKey key) where T : class
        {
            var cacheKey = CreateKey<T>(key, typeof(T));
            T obj = null;
            if ((obj = _memoryCache.Get<T>(cacheKey)) != null)
            {
                _logger.LogInformation($"Found in-memory cache for {cacheKey}");
                return obj;
            }

            var data = await _distributedCache.GetStringAsync(cacheKey);
            var result = DeserializeData<T>(data);
            if (result != null)
            {
                _logger.LogInformation($"Found distributed cache for {cacheKey}");
                _memoryCache.Set<T>(cacheKey, result.Object, result.InMemoryCacheExpiration);
            }

            return result?.Object;
        }

        public async Task SetAsync<T>(ICacheKey key, T obj, TimeSpan? memoryCacheExpiration = null, TimeSpan? redisExpiration = null) where T : class
        {
            memoryCacheExpiration ??= _defaultInMemoryCacheExpiration;
            redisExpiration ??= _defaultDistributedCacheExpiration;

            var cacheObject = new CacheObject<T>(obj, memoryCacheExpiration.Value, redisExpiration.Value);

            var cacheKey = CreateKey<T>(key, typeof(T));
            _memoryCache.Set<T>(cacheKey, obj, memoryCacheExpiration.Value);

            await _distributedCache.SetStringAsync(cacheKey, SerializeData<T>(cacheObject), new DistributedCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = redisExpiration.Value,
            });
        }

        public async Task RemoveAsync<T>(ICacheKey key) where T : class
        {
            var cacheKey = CreateKey<T>(key, typeof(T));
            _memoryCache.Remove(cacheKey);
            await _distributedCache.RemoveAsync(cacheKey);
        }

        private string CreateKey<T>(ICacheKey key, Type type) where T : class
        {
            var cacheKey = $"{type.Name}_{key.Key}";
            return cacheKey;
        }

        private string SerializeData<T>(CacheObject<T> obj) where T : class
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        private CacheObject<T> DeserializeData<T>(string data) where T : class
        {
            if (string.IsNullOrEmpty(data))
                return null;

            return JsonConvert.DeserializeObject<CacheObject<T>>(data, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
    }
}
