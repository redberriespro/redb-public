﻿namespace Redb.SimpleCache
{
    public class UserCacheKey : ICacheKey
    {
        private readonly string _key;

        private readonly uint? _userId;

        public string Key => $"{_key}_{_userId?.ToString()}";

        public UserCacheKey(string key, uint? userId)
        {
            _key = key;
            _userId = userId;
        }
    }
}
