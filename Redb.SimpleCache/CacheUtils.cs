﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace Redb.SimpleCache
{
    public static class CacheUtils
    {
        public static async Task<T> CachingGetter<T>(ISynchronizedAppCache cache,
            string keyArea,
            string key,
            Func<Task<T>> addItemFactory,
            MemoryCacheEntryOptions cacheOptions)
            where T : class
        {
            try
            {
                await cache.SemaphoreSlim.WaitAsync();
                throw new NotImplementedException("todo");
                // var result = await cache.GetOrAddAsync(key, addItemFactory, cacheOptions);
                // AddToKeysAsync(cache, keyArea, key, cacheOptions);
                // return result;
            }
            finally
            {
                cache.SemaphoreSlim.Release();
            }
        }

        public static void RemoveAllByKeyArea(ISynchronizedAppCache cache, string keyArea)
        {
            try
            {
                cache.SemaphoreSlim.Wait();
                var keys = GetKeys(cache, keyArea);
                foreach (var key in keys)
                {
                    cache.Remove(key);
                }
                var keyAreaTemplate = GetKeyAreaTemplate(keyArea);
                cache.Remove(keyAreaTemplate);
            }
            finally
            {
                cache.SemaphoreSlim.Release();
            }
        }

        private static void AddToKeysAsync(ISynchronizedAppCache cache, string area, string key, MemoryCacheEntryOptions cacheOptions)
        {
            lock (cache.SemaphoreSlim)
            {
                var k = GetKeyAreaTemplate(area);
                var keySet = cache.Get<HashSet<string>>(k) ?? new HashSet<string>();
                keySet.Add(key);
                cache.Add(k, keySet, cacheOptions);
            }
        }

        private static HashSet<string> GetKeys(ISynchronizedAppCache cache, string area)
        {
            var k = GetKeyAreaTemplate(area);
            return cache.Get<HashSet<string>>(k) ?? new HashSet<string>();
        }

        private static string GetKeyAreaTemplate(string area) => $"_keys:{area}";
    }
}
