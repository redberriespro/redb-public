﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LazyCache;
using Microsoft.Extensions.Caching.Memory;

namespace Redb.SimpleCache
{
    internal class SynchronizedAppCache : ISynchronizedAppCache, IDisposable
    {
        private readonly IAppCache _cache;

        public SynchronizedAppCache(IAppCache cache)
        {
            _cache = cache;
        }

        public void Add<T>(string key, T item, MemoryCacheEntryOptions policy)
        {
            _cache.Add(key, item, policy);
        }

        public T Get<T>(string key)
        {
            return _cache.Get<T>(key);
        }
        

        public T GetOrAdd<T>(string key, Func<ICacheEntry, T> addItemFactory)
        {
            return _cache.GetOrAdd<T>(key, addItemFactory);
        }

        public T GetOrAdd<T>(string key, Func<ICacheEntry, T> addItemFactory, MemoryCacheEntryOptions policy)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetAsync<T>(string key)
        {
            return _cache.GetAsync<T>(key);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<ICacheEntry, Task<T>> addItemFactory)
        {
            return _cache.GetOrAddAsync<T>(key, addItemFactory);
        }
        

        public void Remove(string key)
        {
            _cache.Remove(key);
        }

        public LazyCache.ICacheProvider CacheProvider { get; }

        public CacheDefaults DefaultCachePolicy { get; }

        public SemaphoreSlim SemaphoreSlim { get; } = new SemaphoreSlim(1, 1);

        public void Dispose()
        {
            SemaphoreSlim?.Dispose();
        }
    }
}