using Autofac;
using Redb.Catalog.Common.Web.Binders.Parsers;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Web.Binders.Parsers;

namespace Redb.Catalog;

public class CatalogDictionaryWebAutofac : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<DictionaryDataFilterParser>().SingleInstance();
        builder.RegisterType<SorterParser<DictionaryTypeSortColumn>>().SingleInstance();
        builder.RegisterType<SorterParser<DictionarySortColumn>>().SingleInstance();
    }
}