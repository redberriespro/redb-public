﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Exceptions;
using Redb.Catalog.Common.Web.Binders.Parsers;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Models;
using Redb.Catalog.Dictionary.Services;
using Redb.Catalog.Dictionary.Web.Binders.Parsers;
using System.Globalization;

namespace Redb.Catalog.Dictionary.Web.Binders;

public sealed class DictionaryFilteredListRequestBinder : IModelBinder
{
    private readonly DictionaryDataFilterParser _dataFilterParser;
    private readonly SorterParser<DictionarySortColumn> _sorterParser;
    private readonly DictionaryTypeService _dictionaryTypeService;

    public DictionaryFilteredListRequestBinder(
        DictionaryDataFilterParser dataFilterParser,
        SorterParser<DictionarySortColumn> sorterParser,
        DictionaryTypeService dictionaryTypeService)
    {
        _dataFilterParser = dataFilterParser;
        _sorterParser = sorterParser;
        _dictionaryTypeService = dictionaryTypeService;
    }

    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        ArgumentNullException.ThrowIfNull(bindingContext, nameof(bindingContext));

        var searchQuery = bindingContext.ValueProvider.GetValue("searchQuery").FirstOrDefault();
        var type = bindingContext.ValueProvider.GetValue("type").FirstOrDefault();
        var name = bindingContext.ValueProvider.GetValue("name").FirstOrDefault();
        var offset = bindingContext.ValueProvider.GetValue("offset").FirstOrDefault();
        var limit = bindingContext.ValueProvider.GetValue("limit").FirstOrDefault();
        var showDeletedRaw = bindingContext.ValueProvider.GetValue("showDeleted").FirstOrDefault();

        if (type == null)
        {
            throw new CatalogBadRequestException($"Filter 'type' is required");
        }

        var typeModel = await _dictionaryTypeService.GetDictionaryType(type);

        var result = new DictionaryFilteredListRequest
        {
            Type = type,
            Name = name,
            SearchQuery = searchQuery,
            ShowDeleted = showDeletedRaw != null ? bool.Parse(showDeletedRaw) : false,
            Offset = offset != null ? int.Parse(offset) : 0,
            Limit = limit != null ? int.Parse(limit) : -1
        };

        // created=2023-04-21T20:37:36.0000000Z;2023-04-21T20:37:37.0000000Z
        var createdRangeRaw = bindingContext.ValueProvider.GetValue("created").FirstOrDefault();
        var createdRange = createdRangeRaw is not null ? createdRangeRaw.Split(";", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries) : Array.Empty<string>();

        if (createdRangeRaw is not null && createdRange.Length != 2)
        {
            throw new CatalogBadRequestException($"Invalid filter definition for created '{createdRangeRaw}'");
        }

        if (createdRange.Length == 2)
        {
            if (DateTime.TryParse(createdRange[0], CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out var createdStart)
                && DateTime.TryParse(createdRange[1], CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out var createdEnd))
            {
                result.Created = new Range<DateTime>
                {
                    Start = createdStart,
                    End = createdEnd
                };
            }
            else
            {
                throw new CatalogBadRequestException($"Invalid filter definition for created '{createdRangeRaw}'");
            }
        }

        // modified=2023-04-21T20:37:36.0000000Z;2023-04-21T20:37:37.0000000Z
        var modifiedRangeRaw = bindingContext.ValueProvider.GetValue("modified").FirstOrDefault();
        var modifiedRange = modifiedRangeRaw is not null ? modifiedRangeRaw.Split(";", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries) : Array.Empty<string>();

        if (modifiedRangeRaw is not null && modifiedRange.Length != 2)
        {
            throw new CatalogBadRequestException($"Invalid filter definition for modified '{modifiedRangeRaw}'");
        }

        if (modifiedRange.Length == 2)
        {
            if (DateTime.TryParse(modifiedRange[0], CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out var modifiedStart)
                && DateTime.TryParse(modifiedRange[1], CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out var modifiedEnd))
            {
                result.Modified = new Range<DateTime>
                {
                    Start = modifiedStart,
                    End = modifiedEnd
                };
            }
            else
            {
                throw new CatalogBadRequestException($"Invalid filter definition for modified '{modifiedRangeRaw}'");
            }
        }

        // id=100;200
        var idRangeRaw = bindingContext.ValueProvider.GetValue("id").FirstOrDefault();
        var idRange = idRangeRaw is not null ? idRangeRaw.Split(";", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries) : Array.Empty<string>();

        if (idRangeRaw is not null && idRange.Length != 2)
        {
            throw new CatalogBadRequestException($"Invalid filter definition for id '{idRangeRaw}'");
        }

        if (idRange.Length == 2)
        {
            if (int.TryParse(idRange[0], out var idStart)
                && int.TryParse(idRange[1], out var idEnd))
            {
                result.Id = new Range<int>
                {
                    Start = idStart,
                    End = idEnd
                };
            }
            else
            {
                throw new CatalogBadRequestException($"Invalid filter definition for id '{idRangeRaw}'");
            }
        }

        result.Filters = (await _dataFilterParser.ParseAsync(bindingContext.HttpContext.Request.Query, typeModel)).ToArray();
        result.Sorter = await _sorterParser.ParseAsync(bindingContext, new[] { typeModel.TypeSchema });


        bindingContext.Result = ModelBindingResult.Success(result);
    }
}