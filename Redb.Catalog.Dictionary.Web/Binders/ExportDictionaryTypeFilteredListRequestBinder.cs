﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Redb.Catalog.Common.Web.Binders.Parsers;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Web.Binders;

public sealed class ExportDictionaryTypeFilteredListRequestBinder : IModelBinder
{
    private readonly DictionaryTypeFilteredListRequestBinder _binder;
    private readonly ExportedColumnsParser _exportedColumnsParser;

    public ExportDictionaryTypeFilteredListRequestBinder(
        SorterParser<DictionaryTypeSortColumn> sorterParser,
        ExportedColumnsParser exportedColumnsParser)
    {
        ArgumentNullException.ThrowIfNull(sorterParser, nameof(sorterParser));

        _binder = new DictionaryTypeFilteredListRequestBinder(sorterParser);
        _exportedColumnsParser = exportedColumnsParser;
    }

    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        ArgumentNullException.ThrowIfNull(bindingContext, nameof(bindingContext));

        await _binder.BindModelAsync(bindingContext);

        var request = (DictionaryTypeFilteredListRequest?)bindingContext.Result.Model;

        var result = new ExportDictionaryTypeFilteredListRequest
        {
            ItemCount = request.ItemCount,
            Limit = request.Limit,
            Name = request.Name,
            Offset = request.Offset,
            SearchQuery = request.SearchQuery,
            Sorter = request.Sorter,
            Columns = _exportedColumnsParser.Parse(bindingContext)
        };

        bindingContext.Result = ModelBindingResult.Success(result);
    }
}