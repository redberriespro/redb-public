﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Redb.Catalog.Common.Web.Binders.Factories;
using Redb.Catalog.Common.Web.Binders.Parsers;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Web.Binders.Factories;

public class ExportDictionaryTypeFilteredListRequestBinderFactory : IBinderFactory
{
    public Type ModelType { get; } = typeof(ExportDictionaryTypeFilteredListRequest);

    public IModelBinder Create(ModelBinderProviderContext context)
    {
        var sorterParser = context.Services.GetRequiredService<SorterParser<DictionaryTypeSortColumn>>();
        var exportedColumnsParser = context.Services.GetRequiredService<ExportedColumnsParser>();

        return new ExportDictionaryTypeFilteredListRequestBinder(sorterParser, exportedColumnsParser);
    }
}
