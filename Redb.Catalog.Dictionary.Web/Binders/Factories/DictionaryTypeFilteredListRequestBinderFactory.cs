﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Redb.Catalog.Common.Web.Binders.Factories;
using Redb.Catalog.Common.Web.Binders.Parsers;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Web.Binders.Factories;

public class DictionaryTypeFilteredListRequestBinderFactory : IBinderFactory
{
    public Type ModelType { get; } = typeof(DictionaryTypeFilteredListRequest);

    public IModelBinder Create(ModelBinderProviderContext context)
    {
        var sorterParser = context.Services.GetRequiredService<SorterParser<DictionaryTypeSortColumn>>();

        return new DictionaryTypeFilteredListRequestBinder(sorterParser);
    }
}
