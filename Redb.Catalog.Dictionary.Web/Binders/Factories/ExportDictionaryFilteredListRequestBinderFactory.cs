﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Redb.Catalog.Common.Web.Binders.Factories;
using Redb.Catalog.Common.Web.Binders.Parsers;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Models;
using Redb.Catalog.Dictionary.Services;
using Redb.Catalog.Dictionary.Web.Binders.Parsers;

namespace Redb.Catalog.Dictionary.Web.Binders.Factories;

public class ExportDictionaryFilteredListRequestBinderFactory : IBinderFactory
{
    public Type ModelType { get; } = typeof(ExportDictionaryFilteredListRequest);

    public IModelBinder Create(ModelBinderProviderContext context)
    {
        var sorterParser = context.Services.GetRequiredService<SorterParser<DictionarySortColumn>>();
        var exportedColumnsParser = context.Services.GetRequiredService<ExportedColumnsParser>();
        var dataFilterParser = context.Services.GetRequiredService<DictionaryDataFilterParser>();
        var dictionaryTypeService = context.Services.GetRequiredService<DictionaryTypeService>();

        return new ExportDictionaryFilteredListRequestBinder(dataFilterParser, sorterParser, dictionaryTypeService, exportedColumnsParser);
    }
}
