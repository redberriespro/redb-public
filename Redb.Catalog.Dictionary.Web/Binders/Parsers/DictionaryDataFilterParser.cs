﻿using Microsoft.AspNetCore.Http;
using Redb.Catalog.Common.Enums;
using Redb.Catalog.Common.Exceptions;
using Redb.Catalog.Common.Models.DataFilters;
using Redb.Catalog.Dictionary.Entities;
using Redb.Catalog.Dictionary.Models;
using System.Collections.Immutable;
using System.Text.RegularExpressions;

namespace Redb.Catalog.Dictionary.Web.Binders.Parsers;

//  prop1.search=str
//  prop2.oneOf=AMD;INTEL

//  prop3.oneOf=AMD
//  prop3.oneOf=INTEL

//  prop4.range=10;20
public class DictionaryDataFilterParser
{
    private const string RegexPattern = @"^(.+?)?\.(.+?)?(\[.+\])?$";
    private const string DataFilterPrefix = "d.";
    private readonly Regex Regex = new(RegexPattern, RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);

    public async Task<ImmutableArray<DataFilter>> ParseAsync(IQueryCollection query, DictionaryTypeModel type)
    {
        var filters = new List<DataFilter>();

        foreach (var kv in query.Where(kv => kv.Key.StartsWith(DataFilterPrefix)))
        {
            var parsingString = kv.Key.Substring(DataFilterPrefix.Length);

            var match = Regex.Match(parsingString);

            if (match.Success)
            {
                var parts = match.Groups.Values.Select(gr => gr.Value).Skip(1).Where(v => !string.IsNullOrWhiteSpace(v)).ToArray();

                if (parts.Length > 1)
                {
                    var property = parts[0];

                    if (Enum.TryParse<DataFilterType>(parts[1], ignoreCase: true, out var filterType))
                    {
                        switch (filterType)
                        {
                            case DataFilterType.Exist:
                            {
                                if (!bool.TryParse(kv.Value.First(), out var exist))
                                    throw new CatalogBadRequestException($"Exist filter accept true or false only");
                                var filter = DataFilter.CreateExist<DictionaryEntity>(property, exist);

                                filters.Add(filter);

                                break;
                            }
                            case DataFilterType.Equal:
                            {
                                var propertyType = GetPropertyType(property, type);

                                var value = kv.Value.First();

                                var filter = DataFilter.CreateEqual<DictionaryEntity>(property, propertyType, value);

                                filters.Add(filter);

                                break;
                            }
                            case DataFilterType.Search:
                                {
                                    var propertyType = GetPropertyType(property, type);

                                    var value = kv.Value.First();

                                    var filter = DataFilter.CreateSearch<DictionaryEntity>(property, value);

                                    filters.Add(filter);

                                    break;
                                }
                            case DataFilterType.OneOf:
                                {
                                    var values = kv.Value.First().Split(";", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

                                    var propertyType = GetPropertyType(property, type);

                                    var filter = DataFilter.CreateOneOf<DictionaryEntity>(property, propertyType, values);

                                    filters.Add(filter);

                                    break;
                                }
                            case DataFilterType.Range:
                                {
                                    var values = kv.Value.First().Split(";", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

                                    if (values.Length != 2)
                                    {
                                        throw new CatalogBadRequestException($"Range filter accept two values separated by a comma");
                                    }

                                    var propertyType = GetPropertyType(property, type);

                                    var filter = DataFilter.CreateRange<DictionaryEntity>(property, propertyType, values[0], values[1]);

                                    filters.Add(filter);

                                    break;
                                }
                            default:
                                {
                                    throw new InvalidOperationException($"Unknown filter type '{filterType}'");
                                }
                        }

                        continue;
                    }
                }
            }

            throw new CatalogBadRequestException($"Invalid filter definition '{kv.Key}'");
        }

        return filters.ToImmutableArray();
    }

    private string GetPropertyType(string property, DictionaryTypeModel typeSchema)
    {
        var p = typeSchema.TypeSchema["properties"][property];

        if (p is not null)
        {
            return p.Value<string>("type");
        }

        throw new CatalogBadRequestException($"Dictiinary type '{typeSchema.Id}' does not contains property '{property}");
    }
}
