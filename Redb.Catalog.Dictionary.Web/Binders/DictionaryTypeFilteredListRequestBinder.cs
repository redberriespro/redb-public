﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Exceptions;
using Redb.Catalog.Common.Web.Binders.Parsers;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Web.Binders;

public sealed class DictionaryTypeFilteredListRequestBinder : IModelBinder
{
    private readonly SorterParser<DictionaryTypeSortColumn> _sorterParser;

    public DictionaryTypeFilteredListRequestBinder(SorterParser<DictionaryTypeSortColumn> sorterParser)
    {
        _sorterParser = sorterParser;
    }

    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        ArgumentNullException.ThrowIfNull(bindingContext, nameof(bindingContext));

        var searchQuery = bindingContext.ValueProvider.GetValue("searchQuery").FirstOrDefault();
        var name = bindingContext.ValueProvider.GetValue("name").FirstOrDefault();
        var offset = bindingContext.ValueProvider.GetValue("offset").FirstOrDefault();
        var limit = bindingContext.ValueProvider.GetValue("limit").FirstOrDefault();


        var result = new DictionaryTypeFilteredListRequest
        {
            SearchQuery = searchQuery,
            Name = name,
            Offset = offset != null ? int.Parse(offset) : 0,
            Limit = limit != null ? int.Parse(limit) : -1,
            Sorter = await _sorterParser.ParseAsync(bindingContext)
        };

        var itemCountRangeRaw = bindingContext.ValueProvider.GetValue("itemCount").FirstOrDefault();
        var itemCountRange = itemCountRangeRaw is not null ? itemCountRangeRaw.Split(";", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries) : Array.Empty<string>();

        if (itemCountRangeRaw is not null && itemCountRange.Length != 2)
        {
            throw new CatalogBadRequestException($"Invalid filter definition for id '{itemCountRangeRaw}'");
        }

        if (itemCountRange.Length == 2)
        {
            if (int.TryParse(itemCountRange[0], out var idStart)
                && int.TryParse(itemCountRange[1], out var idEnd))
            {
                result.ItemCount = new Range<int>
                {
                    Start = idStart,
                    End = idEnd
                };
            }
            else
            {
                throw new CatalogBadRequestException($"Invalid filter definition for itemCount '{itemCountRangeRaw}'");
            }
        }

        bindingContext.Result = ModelBindingResult.Success(result);
    }
}