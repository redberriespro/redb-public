﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Redb.Catalog.Common.Web.Binders.Parsers;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Models;
using Redb.Catalog.Dictionary.Services;
using Redb.Catalog.Dictionary.Web.Binders.Parsers;

namespace Redb.Catalog.Dictionary.Web.Binders;

public sealed class ExportDictionaryFilteredListRequestBinder : IModelBinder
{
    private readonly DictionaryFilteredListRequestBinder _binder;
    private readonly ExportedColumnsParser _exportedColumnsParser;

    public ExportDictionaryFilteredListRequestBinder(
        DictionaryDataFilterParser dataFilterParser,
        SorterParser<DictionarySortColumn> sorterParser,
        DictionaryTypeService dictionaryTypeService,
        ExportedColumnsParser exportedColumnsParser)
    {
        ArgumentNullException.ThrowIfNull(sorterParser, nameof(sorterParser));

        _binder = new DictionaryFilteredListRequestBinder(dataFilterParser, sorterParser, dictionaryTypeService);
        _exportedColumnsParser = exportedColumnsParser;
    }

    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        ArgumentNullException.ThrowIfNull(bindingContext, nameof(bindingContext));

        await _binder.BindModelAsync(bindingContext);

        var request = (DictionaryFilteredListRequest?)bindingContext.Result.Model;

        var result = new ExportDictionaryFilteredListRequest
        {
            Created = request.Created,
            Filters = request.Filters,
            Id = request.Id,
            Modified = request.Modified,
            ShowDeleted = request.ShowDeleted,
            Type = request.Type,
            Limit = request.Limit,
            Name = request.Name,
            Offset = request.Offset,
            SearchQuery = request.SearchQuery,
            Sorter = request.Sorter,
            Columns = _exportedColumnsParser.Parse(bindingContext)
        };

        bindingContext.Result = ModelBindingResult.Success(result);
    }
}