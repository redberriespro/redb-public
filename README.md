# Библиотеки Redberries.Public

`Redberries.Public` - набор открытых библиотек, позволяющий решать различные задачи:<br>

- `Redb.Telemetry` - набор расширений для организации трассировки, сборки метрики и ведения журналирования использованием стандатра [OpenTelemetry][opentelemetry-link].
- `Redb.Notifications` - сервер рассылки уведомлений через почту, смс и телеграмм.
- `Redb.SimpleCache` - реализация кэша.
- `Redb.LifetimeSynchronizedCache` - реализация in-memory кэша с возможность синхронизации различных экземпляров кэша.
- `Redb.Telemetry.SignalR` - инструментарий `open telemetry` для сбора телеметрии SignalR.
- `Redb.Catalog.Common.*` - набор базовых концепций и классов для библиотек Redb.Catalog.*.
- `Redb.Catalog.Dictionary.*` - реализация настраиваемых справочников.

## Redb.Telemetry

Раздел в разработке :)

## Redb.Notifications

Раздел в разработке :)

## Redb.SimpleCache

Раздел в разработке :)

## Redb.LifetimeSynchronizedCache

Библиотека `Redb.LifetimeSynchronizedCache` предоставляет `in-memory` кэш, поддерживающий синхронизацию времени жизни элементов в различных его экземплярах. Иными словами при удалении элемента из одного экзепляра кэша, он будет удален и из других связанных экземпляров.<br> 
`LifetimeSynchronizedCache` реализует стандартный интерфейс `IMemoryCache`. Синхронизация времени жизни элементов выполняется чере транспорт, опеределенный интерфейсов `ITransport`. Библиотека `Redb.LifetimeSynchronizedCache.Transport.RabbitMq` предоставляет реализацию транспорта на основе `RabbitMQ`.<br>
`LifetimeSynchronizedCache` позволяет создать несколько именованых независимых кэшей. Синхронизации времени жизни будет выполняться только в рамках кэшей с одним именем.

### Конфигурирование кэша

Конфигурация кэша задается в `LifetimeSynchronizedMemoryCacheOptions`. `LifetimeSynchronizedMemoryCacheOptions` наследуется от `MemoryCacheOptions` и  позволяет настраивать кэш аналогично `MemoryCache`. Дополнительно `LifetimeSynchronizedMemoryCacheOptions` предоставляет следующие настройки:<br>

- `SyncEnabled` - включение синхронизации. По умолчанию отключена. При отключенной синхронизации кэш ведет себя аналогично `MemoryCache`.
- `Transport` - тип транспорта для синхронизации. Обязателен при включенной синхронизации. Для транспорта `RabbitMQ` указывается значение `rabbtimq`.
- `Name` - имя кэша. Не обязательный параметр. Имя по умолчанию - `default`.

### Подключения кэша

Библиотека `LifetimeSynchronizedCache` предоставляет методы расширения `AddLifetimeSynchronizedMemoryCache` для регистрации кэша в стадартном DI.

```c#
services.AddLifetimeSynchronizedMemoryCache(options => {
                options.Name = "web";
                options.SyncEnabled = true;
                options.Transport = "rabbitmq";
            });
```

Библиотека `Redb.LifetimeSynchronizedCache.Autofac` предоставляет модуль `LifetimeSynchronizedMemoryCacheAutofacModule` для регистрации кэша в `Autofac`. Отдельно в `Autofac` необходимо зарегистрировать экземпляр `LifetimeSynchronizedMemoryCacheOptions`.

```c#
builder.RegisterInstance(new LifetimeSynchronizedMemoryCacheOptions
            {
                Name = "collector",
                SyncEnabled = true,
                Transport = "rabbitmq"
            });

builder.RegisterModule<LifetimeSynchronizedMemoryCacheAutofacModule>();            
```

### Транспорт RabbitMQ

Для каждого кэша транспорт создает `fanout` обменник с именем `redb.cache-{имя кэша}`. Для каждого экземпляра кэша создается очередь с именем `redb.cache-{имя кэша}` и подключается к обменнику. И обменник и очередь являются временными и создаеются с флагом `autoDelete = true`.

#### Конфигурирование транспорта RabbitMQ

Конфигурация транспорта задается в `RabbitMqTransportOptions`. `RabbitMqTransportOptions` предоставляет следующие настройки:<br>

- `ApplicationName` - имя приложения. Иcпользуется в отображеним подключения в rabbitmq.
- `HostNames` - адреса RabbitMQ.
- `VirtualHost` - виртуальный хост.
- `Port` - порт RabbitMQ. Порт по-умолчанию - `5672`.
- `UserName` - имя пользователя.
- `Password` - пароль пользователя.
- `ExchangeArguments` - дополнительные параметры для создания обменника.
- `QueueArguments` - дополнительные параметры для создания очереди.

#### Подключения транспорта RabbitMQ

Библиотека `Redb.LifetimeSynchronizedCache.Transport.RabbitMq` предоставляет методы расширения `AddLifetimeSynchronizedMemoryCacheRabbitMqTransport` для регистрации транспорта в стадартном DI:

```c#
services.AddLifetimeSynchronizedMemoryCacheRabbitMqTransport(options =>
            {
                options.UserName = "guest";
                options.Password = "guest";
                options.VirtualHost = "test";
                options.HostNames = new List<string>
                {
                    "localhost"
                };
            });
```

Библиотека `Redb.LifetimeSynchronizedCache.Transport.RabbitMq.Autofac` предоставляет модуль `LifetimeSynchronizedMemoryCacheRabbitMqTransportAutofacModule` для регистрации кэша в `Autofac`. Отдельно в `Autofac` необходимо зарегистрировать экземпляр `RabbitMqTransportOptions`:

```c#
builder.Register(c => {
                var config = c.Resolve<RabbitMqConfig>();

                return new RabbitMqTransportOptions
                {
                    HostNames = new List<string>
                    {
                        "localhost"
                    },
                    Password = "guest",
                    UserName = "guest",
                    VirtualHost = "test"
                };
            })
            .AsSelf()
            .SingleInstance();

builder.RegisterModule<LifetimeSynchronizedMemoryCacheRabbitMqTransportAutofacModule>();          
```

### Разработка собственного транспорта

Разработка собственного транспорта требует реализации двух интерфейсов `ITransport` и `ITransportFactory`.
`ITransport` представляет транспорт и требует реализовать метод `NotifyForRemove` и свойство `RemovalNoticeListiner`. Метод `NotifyForRemove` должен реализовать логику отправки в другие экземпляры кэша уведомения об удалении элемента по ключу. Cвойство `RemovalNoticeListiner` представляет слушатель уведомлений об удалении элемента. Слушатель должен быть вызван в момент получения транспортом уведомления об удалении элемента кэша от других экземпляров кэша.

`ITransportFactory` отвечает за создание экземпляра транспорта и его подключение к кэшу. Интерфейс требует реализовать метод `Create`, создающий транспорт для кэша, и свойство `TransportType`, определяющее строковый тип транспорта. Это значение указывается в `LifetimeSynchronizedMemoryCacheOptions` в `Transport`.

Для использования транспорта кэшем реализация `ITransportFactory` должна быть зарегистрирована в DI.

## Redb.Telemetry.SignalR

Библиотека `Redb.Telemetry.SignalR` предоставляет инструментарий `open telemetry` для сбора телеметрии SignalR. На текущий момент реализована трассировка входящих и исходящий запросов хаба, события подключения и отключения клиентов хаба.<br> 

### Подключение сбора телеметрии

Для подключения телеметрии необходимо:<br> 
- После подключения SignalR вызвать метод расширения `AddTelemetry` для `ISignalRServerBuilder`:
```c#
services.AddSignalR().AddTelemetry();
```
- Добавить источник телеметрии `SignalR`.



## Redb.Catalog.Common

Набор базовых концепций и классов для библиотек `Redb.Catalog.*`:<br>

- `BasicTypes` - базовые типы. К примеру, для описания сортировки эелементов.
- `Data` - типы для организации работы с данными на уровне БД.
    - `Filters` - типы для организации фильтров данных на уровне БД, включая фильтрацию по json-полям.
- `Exceptions` - типы базовых исключений.
- `Extensions` - набор методов расширений.
- `Models` - модели для сортировки, постраничных списков и т. д.
    - `DataFilters` - модели для организации фильтров по json-полям.

## Redb.Catalog.Common.Web

Набор классов для реализации веб-апи:<br>

- `Binders` - интерфейсы и классы для реализации `IModelBinder` и их регистрации в ASP.Net Core MVC.

## Redb.Catalog.Dictionary.Interfaces

Набор интерфейсов и классов, описывающий потребительское апи (consumer api) справочников.<br>
Справочники описываются метаданными и являются версионироваными.

### Потребительский апи справочников

- `DictionaryManifest` - метаданные справочника.
    - `TypeName` - наименование справочника.
    - `TypeSchema` - json-схема, описывающая набор полей и вложений элемента справочника .
- `IDictionaryTypeData` - тип справочника.
    - `Id` - идентификатор конкретной версии типа справочника.
    - `Code` - код типа справочника. У разных версий одного типа справочника всегда один код.
    - `Version` - версия типа справочника.
- `DictionaryInstance` - представление элемента справочника.
    - `Id` - идентификатор элемента справочника.
    - `DictionaryType` - идентификатор справочника.
    - `Data` - данные элемента справочника в соответствии с json-схемой.
    - `Name` - наименования элемента справочника. Отображается конечному потребителю.
- `DictionaryInstanceAttachment` - представления вложения элемента справочника.
    - `DictionaryInstanceId` -  идентификатор элемента справочника, к которому относится вложение.
    - `DictionaryAttachmentType` - тип вложения, значение соответствует имени атрибута, описывающего вложение в json-схеме типа справочника.
    - `Data` - дополнительные характеристики, описывающие вложение.
    - `Content` - бинарные данные вложений.
    - `ContentType` - mime-тип вложения.
- `IDictionaryTypeStorage` - описывает хранилище типов справочников. Позволяет получить информацию обо всех или конкретном типе справочника.
- `IDictionaryStorage` - описываехранилище элементов справочника. Позволяет получить все элементы конкретного справочника и соответствующие указаному условию.

## Redb.Catalog.Dictionary

Реализация потребительского апи и апи управления справочниками и их версиями.<br>
Библиотека ориентирована на работу с БД Postgresql и использует EFCore.

### Апи управления справочникамии их версиями

- `DictionaryTypeService` - позволяет получить типы справочников с учетом различных фильтров.
- `DictionaryTypeExporterService` - позволяет экспортировать типы справочников в excel.
- `DictionaryService` - позволяет получить элементы справочника с учетом различных фильтров. А также добавлять новые элементы, изменять или удалять существующие и вести историю изменений элементов.
- `DictionaryExporterService` - позволять экспортировать элементы справочников в excel.
- `DictionaryAttachmentService` - позвоялет управлять вложениями элементов справочников.

### Использование библиотеки

Библиотека использует `Autofac` в качестве контейнера зависимостей.<br>

Для подключения библиотеки необходимо:<br>

- зарегистрировать модуль `CatalogDictionaryAutofac` в контейнере `Autofac`.
- в `DBContext` приложений реализовать интерфейсы `ICatalogDictionaryContext` и `IChangeTrackeredContext` (из `Redb.Catalog.Common`).
- переопределить метод `SaveChangesAsync` `DBContext` следующим образом
- в метод `OnModelCreating` `DBContext` добавить вызов метода `CatalogDictionaryConfiguration.ConfigureModel(...)`
- реализовать интерфейс `ICatalogDictionaryContextFactory` и зарегистрировать реализацию в DI.
- реализовать интерфейс `IDictionaryTypeStorage` и зарегистрировать реализацию в DI.
    - в `Library` предоставляется набор утилит для реализации хранилища типов справочников на основе json-файлов.

Пример переопределения `SaveChangesAsync`:

```c#
public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
{
    var dictionaryVersionTracker = new DictionaryVersionTracker<MetadataContext>(this);

    dictionaryVersionTracker.CollectChanges();

    await base.SaveChangesAsync(cancellationToken);

    var authorName = ...// получение имени автора изменений по UserId из ICatalogDictionaryContext

    dictionaryVersionTracker.GenerateVersions(UserId, authorName ?? "");

    return await base.SaveChangesAsync(cancellationToken);
}
```
#### Описание типов справочников

Справочники описываются json-схемой.

Пример json-файла для определения типа справочника без вложений:

```json
{
  "typeName": "Справочник 1",

  "typeSchema": {
    "type": "object",
    "properties": {
      
      "code": {
        "title": "Код",
        "type": "string"
      },

      "description": {
        "title": "Описание",
        "type": "string"
      }
    },
    "required": [ "code", "description" ]
  }
}
```

json-схема вложения

```json
{
  "type": "object",
  "attachment": true,
  "properties": {
    "content": {
      "type": "string",
      "contentEncoding": "base64",
      "contentMediaType": "image/png;application/pdf"
    }
    ///произвольный набор характеристик, описыващий вложение.
    ///"label": {
    ///  "type": "string"
    //}
  },
  "required": []
}
```
<br>

- Атрибут "attachment": true указывает на то, что это вложение.

- content описывает формат вложения:

  - type - данные передаются в виде строки

  - contentEncoding - формат кодирования данных в строке, всегда base64

  - contentMediaType - принимаемые mime-типы. Набор типов, перечисленных через `;` или `*/*` для указания всех типов.

Возможно указать произвольные набор атрибутов, которые предоставляют дополнительное описание вложения. Например, label.<br>

В схеме справочника можно указать одинарное вложение либо описать набор вложений. Возможно указать несколько одинарных вложений и наборов вложений для одного справочника.

Пример json-файла для определения типа справочника с одинарным вложением icon. attachment": true после "type": "object" указывает на то, icon не просто атрибут, а именно вложение:

```json
{
  "typeName": "Справочник 1",

  "typeSchema": {
    "type": "object",
    "properties": {
      
      "code": {
        "title": "Код",
        "type": "string"
      },

      "description": {
        "title": "Описание",
        "type": "string"
      },
      
      "icon": {
        "type": "object",
        "attachment": true,
        "properties": {
          "content": {
            "type": "string",
            "contentEncoding": "base64",
            "contentMediaType": "image/png"
          },
          "label": {
            "type": "string"
          }
        },
        "required": ["label" ]
      }
    },
    "required": [ "code", "description" ]
  }
}
``` 

Пример json-файла для определения типа справочника набором вложений images. attachments": true после type": "array" указывает на то, что это не просто набор объектов, а именно набор вложений:

```json
{
  "typeName": "Справочник 1",

  "typeSchema": {
    "type": "object",
    "properties": {
      
      "code": {
        "title": "Код",
        "type": "string"
      },

      "description": {
        "title": "Описание",
        "type": "string"
      },

      "images": {
        "title": "Иконки",
        "type": "array",
        "attachments": true,
        "items": {
          "type": "object",
          "attachment": true,
          "properties": {
            "content": {
              "type": "string",
              "contentEncoding": "base64",
              "contentMediaType": "image/png"
            },
            "label": {
              "type": "string"
            }
          },
          "required": [ "label" ]
        }
      }
    },
    "required": [ "code", "description" ]
  }
}
```

## Redb.Catalog.Dictionary.Web

Предоставляет класс для реализации веб апи управления справочниками.<br>
 
- `Binders` - реализации `IModelBinder` для формирования запросов к апи управления справочниками и способ их регистрации в ASP.Net Core MVC.
- При подключении библиотеки необходимо в DI зарегистрировать модули `CatalogDictionaryWebAutofac` и `CatalogCommonWebAutofac` (из `Redb.Catalog.Common.Web`)
 
[opentelemetry-link]: https://opentelemetry.io/