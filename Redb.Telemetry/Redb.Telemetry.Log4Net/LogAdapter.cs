﻿using log4net.Core;
using Microsoft.Extensions.Logging;
using OpenTelemetry.Exporter;
using OpenTelemetry.Logs;
using OpenTelemetry.Resources;

namespace Redb.Telemetry.Log4Net
{
    internal class LogAdapter: IDisposable
    {
        private readonly ILogger<LogAdapter> _logger;
        private readonly LoggingOptions _options;
        private ILoggerFactory? _loggerFactory;
        private bool _disposed = false;

        public LogAdapter(LoggingOptions options)
        {
            ArgumentNullException.ThrowIfNull(options, nameof(options));

            _options = options;

            _loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.ClearProviders();

                ConfigureLogging(builder);
            });

            _logger = _loggerFactory.CreateLogger<LogAdapter>();
        }

        public void Log(LoggingEvent loggingEvent)
        {
            var logLevel = LogLevel.None;

            if (loggingEvent.Level == Level.Error)
            {
                logLevel = LogLevel.Error;
            }
            else if (loggingEvent.Level == Level.Critical)
            {
                logLevel = LogLevel.Critical;
            }
            else if (loggingEvent.Level == Level.Fatal)
            {
                logLevel = LogLevel.Critical;
            }
            else if (loggingEvent.Level == Level.Warn)
            {
                logLevel = LogLevel.Warning;
            }
            else if (loggingEvent.Level == Level.Info)
            {
                logLevel = LogLevel.Information;
            }
            else if (loggingEvent.Level == Level.Debug)
            {
                logLevel = LogLevel.Debug;
            }
            else
            {
                logLevel = LogLevel.Information;
            }

            _logger.Log(
                logLevel,
                new EventId(0),
                loggingEvent,
                loggingEvent.ExceptionObject,
                (state, exception) =>
                {
                    if (exception is not null && string.IsNullOrEmpty(loggingEvent.RenderedMessage))
                    {
                        return exception.Message;
                    }
                    else
                    {
                        return loggingEvent.RenderedMessage;
                    }
                });
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _loggerFactory?.Dispose();
                    _loggerFactory = null;
                }

                _disposed = true;
            }
        }

        private void ConfigureLogging(ILoggingBuilder builder)
        {
            builder.SetMinimumLevel(_options.LogLevel);

            Action<ResourceBuilder> configureResource = r =>
            {
                r.AddEnvironmentVariableDetector();
                r.AddTelemetrySdk();
                r.AddService(
                    serviceName: _options.ServiceName,
                    serviceVersion: _options.ServiceVersion);
            };

            builder.AddOpenTelemetry(options =>
            {
                options.IncludeScopes = true;
                options.ParseStateValues = true;
                options.IncludeFormattedMessage = true;
                var resourceBuilder = ResourceBuilder.CreateDefault();
                configureResource(resourceBuilder);
                options.SetResourceBuilder(resourceBuilder);
                options.AddProcessor(new LoggingEventProcessor());
                options.AddOtlpExporter(o =>
                {
                    o.Endpoint = new Uri($"{_options.CollectorEndpoint}/v1/logs");
                    o.Protocol = OtlpExportProtocol.Grpc;
                });

                if (_options.Debug)
                {
                    options.AddConsoleExporter();
                }
            });
        }
    }
}
