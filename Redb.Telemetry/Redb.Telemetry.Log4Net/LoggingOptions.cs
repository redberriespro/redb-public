﻿using Microsoft.Extensions.Logging;

namespace Redb.Telemetry.Log4Net
{
    internal class LoggingOptions
    {
        public string ServiceName { get; }

        public string? ServiceVersion { get; }

        public string CollectorEndpoint { get; }

        public LogLevel LogLevel { get; set; } = LogLevel.Information;

        public bool Debug { get; set; } = false;

        public LoggingOptions(
            string collectorEndpoint,
            string serviceName,
            string? serviceVersion = null)
        {
            ArgumentNullException.ThrowIfNull(collectorEndpoint, nameof(collectorEndpoint));
            ArgumentNullException.ThrowIfNull(serviceName, nameof(serviceName));

            CollectorEndpoint = collectorEndpoint;
            ServiceName = serviceName;
            ServiceVersion = serviceVersion;
        }
    }
}
