﻿using OpenTelemetry;
using OpenTelemetry.Logs;
using System.Collections;
using System.Runtime.CompilerServices;

namespace Redb.Telemetry.Log4Net
{
    internal class LoggingEventProcessor : BaseProcessor<LogRecord>
    {
        private const string Log4NetContextPropertyPrefix = "log4net:";
        private const string LoggingEventContextPropertyPrefix = "LoggingEvent.Context.";

        public override void OnEnd(LogRecord data)
        {
            if (data.StateValues == null || data.StateValues.Count == 0) return;

            if (data.StateValues[0].Value is not log4net.Core.LoggingEvent loggingEvent) return;

            // Custom state information
            var logState = new List<KeyValuePair<string, object?>>
            {
                new("LoggingEvent.Domain", loggingEvent.Domain),
                new("LoggingEvent.Level", loggingEvent.Level),
                new("LoggingEvent.LoggerName ", loggingEvent.LoggerName),
                new("LoggingEvent.MessageObject", loggingEvent.RenderedMessage),
                new("LoggingEvent.ThreadName", loggingEvent.ThreadName),
                new("LoggingEvent.TimeStamp", loggingEvent.TimeStamp),
                new("LoggingEvent.TimeStampUtc", loggingEvent.TimeStampUtc),
                new("LoggingEvent.UserName", loggingEvent.UserName),
            };

            var context = loggingEvent.GetProperties();

            foreach (var key in context.GetKeys())
            {
                if (key.StartsWith(Log4NetContextPropertyPrefix))
                {
                    continue;
                }

                logState.Add(new KeyValuePair<string, object?>($"{LoggingEventContextPropertyPrefix}{key}", context[key]));
            }

            if (data.Exception?.Data.Count > 0)
            {
                foreach (DictionaryEntry entry in data.Exception.Data)
                {
                    logState.Add(new KeyValuePair<string, object?>($"Exception.Data.{entry.Key}", entry.Value));
                }
            }

            data.StateValues = new ReadOnlyCollectionBuilder<KeyValuePair<string, object?>>(logState).ToReadOnlyCollection();

            base.OnEnd(data);
        }
    }
}
