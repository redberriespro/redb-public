﻿using log4net.Appender;
using log4net.Core;
using Microsoft.Extensions.Logging;

namespace Redb.Telemetry.Log4Net
{
    public class OpenTelemetryAppender : AppenderSkeleton
    {
        private readonly Lazy<LogAdapter> _adapter;

        public string ServiceName { get; set; }

        public string ServiceVersion { get; set; }

        public string CollectorEndpoint { get; set; }

        public LogLevel LogLevel { get; set; } = LogLevel.Information;

        public bool Debug { get; set; }

        public OpenTelemetryAppender()
        {
            _adapter = new Lazy<LogAdapter>(() => {
                var options = new LoggingOptions(CollectorEndpoint, ServiceName, ServiceVersion)
                {
                    Debug = Debug,
                    LogLevel = LogLevel
                };

                return new LogAdapter(options);
            }, LazyThreadSafetyMode.ExecutionAndPublication);
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            _adapter.Value.Log(loggingEvent);
        }

        protected override void OnClose() {
            if (_adapter.IsValueCreated)
            {
                _adapter.Value.Dispose();
            }
        }
    }
}