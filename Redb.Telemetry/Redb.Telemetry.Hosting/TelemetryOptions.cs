﻿using Microsoft.Extensions.Logging;
using System.Reflection;

namespace Redb.Telemetry.Hosting
{
    public class TelemetryOptions
    {
        public string ServiceName { get; }

        public string ServiceVersion { get; }

        public string CollectorEndpoint { get; }

        public string[]? TracingSources { get; set; }

        public string[]? Meters { get; set; }

        public string[]? EventSources { get; set; }

        public LogLevel LogLevel { get; set; } = LogLevel.Information;

        public bool Debug { get; set; }

        public TelemetryOptions(
            string serviceName,
            string serviceVersion,
            string collectorEndpoint)
        {
            ArgumentNullException.ThrowIfNull(serviceName, nameof(serviceName));
            ArgumentNullException.ThrowIfNull(serviceVersion, nameof(serviceVersion));
            ArgumentNullException.ThrowIfNull(collectorEndpoint, nameof(collectorEndpoint));

            ServiceName = serviceName;
            ServiceVersion = serviceVersion;
            CollectorEndpoint = collectorEndpoint;
        }

        public TelemetryOptions(
            Assembly assembly,
            string collectorEndpoint)
        {
            ArgumentNullException.ThrowIfNull(assembly, nameof(assembly));
            ArgumentNullException.ThrowIfNull(collectorEndpoint, nameof(collectorEndpoint));

            var name = assembly.GetName();
            ServiceName = name.Name!;
            ServiceVersion = name.Version!.ToString();
            CollectorEndpoint = collectorEndpoint;
        }
    }
}
