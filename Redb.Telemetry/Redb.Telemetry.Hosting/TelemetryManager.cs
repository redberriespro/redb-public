using OpenTelemetry;
using OpenTelemetry.Logs;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace Redb.Telemetry.Hosting;

public static class TelemetryManager
{
    public static TracerProvider SetupTracing(TelemetryOptions options, Action<TracerProviderBuilder>? configureTracingInstrumentations = null)
    {
        ArgumentNullException.ThrowIfNull(options, nameof(options));

        var builder = Sdk.CreateTracerProviderBuilder();

        ConfigureTracing(builder, options, configureTracingInstrumentations);

        return builder.Build();
    }

    public static MeterProvider SetupMetrics(TelemetryOptions options, Action<MeterProviderBuilder>? configureMetricsInstrumentations = null)
    {
        ArgumentNullException.ThrowIfNull(options, nameof(options));

        var builder = Sdk.CreateMeterProviderBuilder();

        ConfigureMetrics(builder, options, configureMetricsInstrumentations);

        return builder.Build();
    }

    public static void ConfigureTracing(TracerProviderBuilder builder, TelemetryOptions options, Action<TracerProviderBuilder>? configureTracingInstrumentations)
    {
        ArgumentNullException.ThrowIfNull(options, nameof(options));

        builder.AddSource(options.TracingSources ?? Array.Empty<string>());

        if (configureTracingInstrumentations is not null)
        {
            configureTracingInstrumentations(builder);
        }

        builder.SetResourceBuilder(
                ResourceBuilder
                    .CreateDefault()
                    .AddEnvironmentVariableDetector()
                    .AddTelemetrySdk()
                    .AddService(serviceName: options.ServiceName, serviceVersion: options.ServiceVersion)
            );

        if (options.Debug)
        {
            builder.AddConsoleExporter();
        }

        builder.AddOtlpExporter(oltp =>
        {
            oltp.Endpoint = new Uri($"{options.CollectorEndpoint}/v1/traces");
        });
    }

    public static void ConfigureMetrics(MeterProviderBuilder builder, TelemetryOptions options, Action<MeterProviderBuilder>? configureMetricsInstrumentations)
    {
        ArgumentNullException.ThrowIfNull(options, nameof(options));

        builder.AddMeter(options.Meters ?? Array.Empty<string>());

        builder.AddRuntimeInstrumentation();

        if (options.EventSources?.Any() ?? false)
        {
            builder.AddEventCountersInstrumentation(c => c.AddEventSources(options.EventSources));
        }

        if (configureMetricsInstrumentations is not null)
        {
            configureMetricsInstrumentations(builder);
        }

        builder.SetResourceBuilder(
                ResourceBuilder
                    .CreateDefault()
                    .AddEnvironmentVariableDetector()
                    .AddTelemetrySdk()
                    .AddService(serviceName: options.ServiceName, serviceVersion: options.ServiceVersion)
            );

        if (options.Debug)
        {
            builder.AddConsoleExporter();
        }

        builder.AddOtlpExporter(oltp =>
        {
            oltp.Endpoint = new Uri($"{options.CollectorEndpoint}/v1/metrics");
        });
    }
}
