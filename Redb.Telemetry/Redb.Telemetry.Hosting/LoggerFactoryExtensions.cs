﻿using log4net.Repository.Hierarchy;
using Redb.Telemetry.Hosting;
using ILoggerFactory = Microsoft.Extensions.Logging.ILoggerFactory;

namespace Redb.Telemetry.Log4Net
{
    public static class LoggerFactoryExtensions
    {
        public static ILoggerFactory AddOpenTelemetryAppender(this ILoggerFactory builder, TelemetryOptions options)
        {
            ArgumentNullException.ThrowIfNull(options, nameof(options));

            var hierarchy = (Hierarchy)log4net.LogManager.GetRepository();

            var appender = new OpenTelemetryAppender
            {
                Name = nameof(OpenTelemetryAppender),
                ServiceName = options.ServiceName,
                ServiceVersion = options.ServiceVersion,
                CollectorEndpoint = options.CollectorEndpoint,
                LogLevel = options.LogLevel,
                Debug = options.Debug
            };
            hierarchy.Root.AddAppender(appender);

            return builder;
        }
    }
}
