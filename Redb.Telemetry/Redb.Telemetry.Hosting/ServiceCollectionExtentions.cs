﻿using Microsoft.Extensions.DependencyInjection;
using OpenTelemetry.Metrics;
using OpenTelemetry.Trace;

namespace Redb.Telemetry.Hosting
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddTracing(this IServiceCollection services,
            TelemetryOptions options,
            Action<TracerProviderBuilder>? configureTracingInstrumentations = null)
        {
            ArgumentNullException.ThrowIfNull(options, nameof(options));

            services.AddOpenTelemetryTracing(builder => TelemetryManager.ConfigureTracing(builder, options, configureTracingInstrumentations));

            return services;
        }

        public static IServiceCollection AddMetrics(this IServiceCollection services,
            TelemetryOptions options,
            Action<MeterProviderBuilder>? configureMetricsInstrumentations = null)
        {
            ArgumentNullException.ThrowIfNull(options, nameof(options));

            services.AddOpenTelemetryMetrics(builder => TelemetryManager.ConfigureMetrics(builder, options, configureMetricsInstrumentations));

            return services;
        }
    }
}
