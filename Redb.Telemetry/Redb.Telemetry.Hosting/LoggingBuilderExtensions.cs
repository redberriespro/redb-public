﻿using log4net.Repository.Hierarchy;
using Microsoft.Extensions.Logging;
using Redb.Telemetry.Hosting;

namespace Redb.Telemetry.Log4Net
{
    public static class LoggingBuilderExtensions
    {
        public static ILoggingBuilder AddOpenTelemetryAppender(this ILoggingBuilder builder, TelemetryOptions options)
        {
            ArgumentNullException.ThrowIfNull(options, nameof(options));

            var hierarchy = (Hierarchy)log4net.LogManager.GetRepository();

            var appender = new OpenTelemetryAppender
            {
                Name = nameof(OpenTelemetryAppender),
                ServiceName = options.ServiceName,
                ServiceVersion = options.ServiceVersion,
                CollectorEndpoint = options.CollectorEndpoint,
                LogLevel = options.LogLevel,
                Debug = options.Debug
            };
            hierarchy.Root.AddAppender(appender);

            return builder;
        }
    }
}
