﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace Redb.Telemetry.SignalR
{
    internal sealed class TelemetryHubLifetimeManager<THub> : DefaultHubLifetimeManager<THub> where THub : Hub
    {
        private readonly HubConnectionStore _connections;
        private readonly string _hubName = typeof(THub).Name;


        public TelemetryHubLifetimeManager(ILogger<DefaultHubLifetimeManager<THub>> logger) : base(logger)
        {
            _connections = GetConnectionsFromBase();
        }

        public override async Task SendAllAsync(string methodName, object?[] args, CancellationToken cancellationToken = default)
        {
            var callInfo = new CallInfo
            {
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            activity?.SetTag(Tags.SignalR.Recipient.Connections, "all");

            try
            {
                await base.SendAllAsync(methodName, args, cancellationToken);

            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public override async Task SendAllExceptAsync(string methodName, object?[] args, IReadOnlyList<string> excludedConnectionIds, CancellationToken cancellationToken = default)
        {
            var callInfo = new CallInfo
            {
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            var ids = string.Join(",", excludedConnectionIds);
            activity?.SetTag(Tags.SignalR.Recipient.ConnectionsExcept, ids);

            try
            {
                await base.SendAllExceptAsync(methodName, args, excludedConnectionIds, cancellationToken);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public override async Task SendConnectionAsync(string connectionId, string methodName, object?[] args, CancellationToken cancellationToken = default)
        {
            var context = _connections[connectionId];

            var callInfo = new CallInfo
            {
                ConnectionId = connectionId,
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent,
                Transport = context?.GetHttpTransportType(),
                User = context?.GetUser(),
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            activity?.SetTag(Tags.SignalR.Recipient.Connection, connectionId);

            try
            {
                await base.SendConnectionAsync(connectionId, methodName, args, cancellationToken);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public override async Task SendConnectionsAsync(IReadOnlyList<string> connectionIds, string methodName, object?[] args, CancellationToken cancellationToken = default)
        {
            var callInfo = new CallInfo
            {
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            var ids = string.Join(",", connectionIds);
            activity?.SetTag(Tags.SignalR.Recipient.Connections, ids);

            try
            {
                await base.SendConnectionsAsync(connectionIds, methodName, args, cancellationToken);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public override async Task SendGroupAsync(string groupName, string methodName, object?[] args, CancellationToken cancellationToken = default)
        {
            var callInfo = new CallInfo
            {
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            activity?.SetTag(Tags.SignalR.Recipient.Group, groupName);

            try
            {
                await base.SendGroupAsync(groupName, methodName, args, cancellationToken);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public override async Task SendGroupsAsync(IReadOnlyList<string> groupNames, string methodName, object?[] args, CancellationToken cancellationToken = default)
        {
            var callInfo = new CallInfo
            {
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            var groups = string.Join(",", groupNames);
            activity?.SetTag(Tags.SignalR.Recipient.Groups, groups);

            try
            {
                await base.SendGroupsAsync(groupNames, methodName, args, cancellationToken);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public override async Task SendGroupExceptAsync(string groupName, string methodName, object?[] args, IReadOnlyList<string> excludedConnectionIds, CancellationToken cancellationToken = default)
        {
            var callInfo = new CallInfo
            {
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            var ids = string.Join(",", excludedConnectionIds);
            activity?.SetTag(Tags.SignalR.Recipient.Group, groupName);
            activity?.SetTag(Tags.SignalR.Recipient.ConnectionsExcept, ids);

            try
            {
                await base.SendGroupExceptAsync(groupName, methodName, args, excludedConnectionIds, cancellationToken);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public override async Task SendUserAsync(string userId, string methodName, object?[] args, CancellationToken cancellationToken = default)
        {
            var callInfo = new CallInfo
            {
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            activity?.SetTag(Tags.SignalR.Recipient.User, userId);

            try
            {
                await base.SendUserAsync(userId, methodName, args, cancellationToken);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public override async Task SendUsersAsync(IReadOnlyList<string> userIds, string methodName, object?[] args, CancellationToken cancellationToken = default)
        {
            var callInfo = new CallInfo
            {
                Hub = _hubName,
                Method = methodName,
                Type = Constants.MessageTypeSent
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            var users = string.Join(',', userIds);
            activity?.SetTag(Tags.SignalR.Recipient.Users, users);

            try
            {
                await base.SendUsersAsync(userIds, methodName, args, cancellationToken);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        private HubConnectionStore GetConnectionsFromBase()
        {
            return (HubConnectionStore)typeof(DefaultHubLifetimeManager<THub>)
                                                .GetField("_connections", BindingFlags.NonPublic | BindingFlags.Instance)!
                                                .GetValue(this)!;
        }
    }
}
