﻿namespace Redb.Telemetry.SignalR
{
    internal static class Tags
    {
        public static class Rpc
        {
            public const string System = "rpc.system";
            public const string Service = "rpc.service";
            public const string Method = "rpc.method";

            public static class Message
            {
                public const string Type = "rpc.message.type";
            }
        }

        public static class Server
        {
            public const string Address = "server.address";
        }

        public static class Otel
        {
            public const string StatusCode = "otel.status_code";
        }

        public static class SignalR
        {
            public static class Connection
            {
                public const string Id = "signalr.connection.id";
                public const string Ids = "signalr.connection.ids";
                public const string Group = "signalr.connection.group";
                public const string Groups = "signalr.connection.groups";
                public const string Status = "signalr.connection.status";
            }

            public static class Recipient
            {
                public const string Connection = "signalr.recipient.connection";
                public const string Connections = "signalr.recipient.connections";
                public const string ConnectionsExcept = "signalr.recipient.connections.except";
                public const string Group = "signalr.recipient.group";
                public const string Groups = "signalr.recipient.groups";
                public const string User = "signalr.recipient.user";
                public const string Users = "signalr.recipient.users";
            }

            public const string User = "signalr.user.name";
            public const string Transport = "signalr.transport";
        }
    }
}
