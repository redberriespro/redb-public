﻿using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Http.Connections.Features;
using Microsoft.AspNetCore.SignalR;

namespace Redb.Telemetry.SignalR
{
    internal static class ContextsExtensions
    {
        public static HttpTransportType? GetHttpTransportType(this HubConnectionContext hubConnectionContext) => hubConnectionContext.Features.Get<IHttpTransportFeature>()!.TransportType;

        public static HttpTransportType? GetHttpTransportType(this HubCallerContext hubCallerContext) => hubCallerContext.Features.Get<IHttpTransportFeature>()!.TransportType;

        public static HttpTransportType? GetHttpTransportType(this HubInvocationContext hubInvocationContext) => hubInvocationContext.Context.GetHttpTransportType();

        public static HttpTransportType? GetHttpTransportType(this HubLifetimeContext hubLifetimeContext) => hubLifetimeContext.Context.GetHttpTransportType();

        public static string? GetUser(this HubConnectionContext hubConnectionContext) => hubConnectionContext.User.Identity?.Name;

        public static string? GetUser(this HubCallerContext hubCallerContext) => hubCallerContext.User?.Identity?.Name;

        public static string? GetUser(this HubInvocationContext hubInvocationContext) => hubInvocationContext.Context.GetUser();

        public static string? GetUser(this HubLifetimeContext hubLifetimeContext) => hubLifetimeContext.Context.GetUser();
    }
}
