﻿using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR;

namespace Redb.Telemetry.SignalR
{
    internal class TelemetryHubFilter : IHubFilter
    {
        public async ValueTask<object?> InvokeMethodAsync(HubInvocationContext invocationContext, Func<HubInvocationContext, ValueTask<object?>> next)
        {
            var callInfo = new CallInfo
            {
                Address = invocationContext.Context.GetHttpContext()?.Request.Host.Value,
                ConnectionId = invocationContext.Context.ConnectionId,
                Hub = invocationContext.Hub.GetType().Name,
                Method = invocationContext.HubMethodName,
                Transport = invocationContext.GetHttpTransportType(),
                Type = Constants.MessageTypeReceived,
                User = invocationContext.GetUser(),
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            try
            {
                return await next(invocationContext);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public async Task OnConnectedAsync(HubLifetimeContext context, Func<HubLifetimeContext, Task> next)
        {
            var callInfo = new CallInfo
            {
                Address = context.Context.GetHttpContext()?.Request.Host.Value,
                ConnectionId = context.Context.ConnectionId,
                Hub = context.Hub.GetType().Name,
                Method = "OnConnected",
                Transport = context.Context.GetHttpTransportType(),
                User = context.GetUser(),
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }

        public async Task OnDisconnectedAsync(
        HubLifetimeContext context,
        Exception? exception,
        Func<HubLifetimeContext, Exception?, Task> next)
        {
            var callInfo = new CallInfo
            {
                Address = context.Context.GetHttpContext()?.Request.Host.Value,
                ConnectionId = context.Context.ConnectionId,
                Hub = context.Hub.GetType().Name,
                Method = "OnDisconnected",
                Transport = context.Context.GetHttpTransportType(),
                User = context.GetUser(),
            };

            using var activity = SignalRActivitySource.StartActivity(callInfo);

            if(exception is null)
            {
                activity?.SetTag(Tags.SignalR.Connection.Status, "normal_closure");
            }

            try
            {
                await next(context, exception);
            }
            catch (Exception ex)
            {
                SignalRActivitySource.RecordException(activity, ex);

                throw;
            }
        }
    }
}