﻿using Microsoft.AspNetCore.Http.Connections;
using Redb.Telemetry.Api;
using System.Diagnostics;

namespace Redb.Telemetry.SignalR
{
    internal static class SignalRActivitySource
    {
        public const string Name = "SignalR";

        private static readonly ActivitySource ActivitySource = new(Name, typeof(SignalRActivitySource).Assembly.GetVersion());

        public static Activity? StartActivity(CallInfo callInfo)
        {
            ArgumentNullException.ThrowIfNull(callInfo);

            var activity = ActivitySource.CreateActivity(GetSpan(callInfo), ActivityKind.Server);

            if (activity is null || activity.IsAllDataRequested == false)
            {
                return null;
            }

            activity.SetTag(Tags.Rpc.System, Constants.RpcSystemSignalR);
            activity.SetTag(Tags.Rpc.Service, callInfo.Hub);
            activity.SetTag(Tags.Rpc.Method, callInfo.Method);
            activity.SetTag(Tags.Rpc.Message.Type, callInfo.Type);
            activity.SetTag(Tags.Server.Address, callInfo.Address);
            activity.SetTag(Tags.SignalR.Connection.Id, callInfo.ConnectionId);
            activity.SetTag(Tags.SignalR.Connection.Id, callInfo.ConnectionId);
            activity.SetTag(Tags.SignalR.Transport, GetTransport(callInfo));
            activity.SetTag(Tags.SignalR.User, callInfo.User);

            return activity.Start();
        }

        public static void RecordException(Activity? activity, Exception exception)
        {
            if (activity is null || activity.IsAllDataRequested == false)
            {
                return;
            }

            activity.SetErrorStatus(exception);
            activity.RecordException(exception);
        }

        private static string GetSpan(CallInfo operationInfo) => $"{operationInfo.Hub}/{operationInfo.Method}";

        private static string? GetTransport(CallInfo operationInfo) => operationInfo.Transport switch
        {
            HttpTransportType.WebSockets => "web_sockets",
            HttpTransportType.ServerSentEvents => "server_sent_events",
            HttpTransportType.LongPolling => "long_polling",
            _ => null
        };
    }
}
