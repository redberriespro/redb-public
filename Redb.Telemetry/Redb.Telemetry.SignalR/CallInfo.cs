﻿using Microsoft.AspNetCore.Http.Connections;

namespace Redb.Telemetry.SignalR
{
    internal class CallInfo
    {
        public string Hub { get; init; } = null!;

        public HttpTransportType? Transport { get; init; }

        public string? ConnectionId { get; init; }

        public string? User { get; init; }

        public string Method { get; init; } = null!;

        public string? Address { get; init; }

        public string? Type { get; init; }
    }
}
