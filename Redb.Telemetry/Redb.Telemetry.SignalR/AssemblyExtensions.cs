﻿using System.Reflection;

namespace Redb.Telemetry.SignalR
{
    internal static class AssemblyExtensions
    {
        public static string GetVersion(this Assembly assembly)
        {
            return assembly.GetName().Version?.ToString() ?? string.Empty;
        }
    }
}
