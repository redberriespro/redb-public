﻿namespace Redb.Telemetry.SignalR
{
    internal static class Constants
    {
        public const string RpcSystemSignalR = "signalr";
        public const string MessageTypeReceived = "RECEIVED";
        public const string MessageTypeSent = "SENT";
    }
}
