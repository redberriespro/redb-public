﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Redb.Telemetry.SignalR
{
    public static class SignalRServerBuilderExtensions
    {
        public static ISignalRServerBuilder AddTelemetry(this ISignalRServerBuilder builder)
        {
            ArgumentNullException.ThrowIfNull(builder);

            builder.Services.TryAddSingleton<TelemetryHubFilter>();

            builder.Services.Replace(ServiceDescriptor.Singleton(typeof(HubLifetimeManager<>), typeof(TelemetryHubLifetimeManager<>)));

            builder.Services.PostConfigure<HubOptions>(options =>
            {
                options.AddFilter<TelemetryHubFilter>();
            });

            return builder;
        }
    }
}
