﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Redb.Telemetry.Api
{
    public static class ActivitySourceExtensions
    {
        public static Activity? StartActivity(this ActivitySource activitySource, object caller, [CallerMemberName] string method = "", ActivityKind kind = ActivityKind.Internal)
        {
            if (caller is null)
            {
                throw new ArgumentNullException(nameof(caller));
            }

            return activitySource.StartActivityInternal(caller.GetType().Name, method, kind);
        }

        public static Activity? StartActivity(this ActivitySource activitySource, Type caller, [CallerMemberName] string method = "", ActivityKind kind = ActivityKind.Internal)
        {
            if (caller is null)
            {
                throw new ArgumentNullException(nameof(caller));
            }

            return activitySource.StartActivityInternal(caller.Name, method, kind);
        }

        private static Activity? StartActivityInternal(this ActivitySource activitySource, string caller, string method, ActivityKind kind)
        {
            return activitySource.StartActivity($"{caller}:{method}", kind);
        }
    }
}
