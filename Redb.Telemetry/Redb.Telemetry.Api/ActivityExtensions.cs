﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Redb.Telemetry.Api
{
    public static class ActivityExtensions
    {
        public static Activity? RecordException(this Activity? activity, Exception exception, bool escaped = true)
        {
            if (activity != null && exception != null)
            {
                ActivityTagsCollection activityTagsCollection = new ActivityTagsCollection
                {
                    {
                        Tags.Exception.Type,
                        exception.GetType().FullName
                    },
                    {
                        Tags.Exception.Stacktrace,
                        exception.ToInvariantString()
                    },
                    {
                         Tags.Exception.Escaped,
                        escaped
                    }
                };

                if (!string.IsNullOrWhiteSpace(exception.Message))
                {
                    activityTagsCollection.Add(Tags.Exception.Message, exception.Message);
                }

                if (exception.Data.Count > 0)
                {
                    foreach (DictionaryEntry entry in exception.Data)
                    {
                        activityTagsCollection.Add(new KeyValuePair<string, object?>($"{Tags.Exception.DataPrefix}.{entry.Key.ToString().ToSnakeCase()}", entry.Value));
                    }
                }

                activity?.AddEvent(new ActivityEvent("exception", default, activityTagsCollection));
            }

            return activity;
        }

        public static Activity? SetErrorStatus(this Activity? activity, Exception exception)
        {
            if (activity != null && exception != null)
            {
                activity.SetStatus(ActivityStatusCode.Error, exception.Message);
            }

            return activity;
        }

        public static Activity? SetErrorStatus(this Activity? activity, string description)
        {
            return activity?.SetStatus(ActivityStatusCode.Error, description);
        }
    }
}
