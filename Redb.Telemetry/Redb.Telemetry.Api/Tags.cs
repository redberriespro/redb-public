﻿namespace Redb.Telemetry.Api
{
    internal static class Tags
    {
        public static class Exception
        {
            public const string Type = "exception.type";
            public const string Stacktrace = "exception.stacktrace";
            public const string Escaped = "exception.escaped";
            public const string Message = "exception.message";
            public const string DataPrefix = "exception.data";
        }
    }
}

