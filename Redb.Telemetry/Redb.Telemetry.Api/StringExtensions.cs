﻿using System.Linq;

namespace Redb.Telemetry.Api
{
    internal static class StringExtensions
    {
        public static string? ToSnakeCase(this string? str)
        {
            if (str == null)
            {
                return null;
            }

            return string.Concat(str.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString())).ToLowerInvariant();
        }
    }
}
