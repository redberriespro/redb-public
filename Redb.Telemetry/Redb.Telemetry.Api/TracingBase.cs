﻿using System.Diagnostics;

namespace Redb.Telemetry.Api
{
    public abstract class TracingBase
    {
        public ActivitySource Source { get; }

        public TracingBase()
        {
            var name = GetType().Assembly.GetName();

            Source = new ActivitySource(name.Name!, name.Version!.ToString());
        }    
    }
}
