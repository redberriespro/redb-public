using Newtonsoft.Json;

namespace Redb.Catalog.Dictionary.Interfaces;

public class ManifestUiSettings
{
    [JsonProperty("listColumns")]
    public string[] ListColumns { get; set; }
}