﻿namespace Redb.Catalog.Dictionary.Interfaces
{
    public interface IDictionaryStorage
    {
        /// <summary>
        /// получить список всех элементов справочника данного типа
        /// </summary>
        public IReadOnlyCollection<DictionaryInstance> GetItemsOfType(string dictionaryType);

        /// <summary>
        /// получить все узлы данного типа справочника, у которых значение определенного свойства в Data
        /// соответствует требуемому значению 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<DictionaryInstance> FindDictionatyItemByDataProperty(string dictType, string propertyName, string propertyValue);

        /// <summary>
        /// Получение вложений элемента справочника по типу
        /// </summary>
        public IReadOnlyCollection<DictionaryInstanceAttachment> GetAttachments(int dictionaryInstanceId, string type, bool excludeContent);

        /// <summary>
        /// Получение вложеня элемента справочника по его идентификатору
        /// </summary>
        DictionaryInstanceAttachment GetAttachment(int dictionaryAttachmentId, bool excludeContent);

    }
}
