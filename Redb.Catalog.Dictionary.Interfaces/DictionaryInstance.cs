﻿using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Interfaces
{
    public class DictionaryInstance
    {
        public int Id;
        public string DictionaryType;
        public JObject Data;
        public string Name;
    }
}