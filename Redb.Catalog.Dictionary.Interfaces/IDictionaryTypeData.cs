﻿namespace Redb.Catalog.Dictionary.Interfaces
{
    public interface IDictionaryTypeData
    {
        string Id { get; }
        string Code { get; }
        int Version { get; }

        DictionaryManifest DictionaryManifest { get; }
    }
}
