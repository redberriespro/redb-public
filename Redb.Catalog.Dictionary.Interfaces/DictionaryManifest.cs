using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Interfaces;

public class DictionaryManifest
{
    [JsonProperty("typeName")]
    public string TypeName { get; set; }

    [JsonProperty("typeSchema")]
    public JObject TypeSchema { get; set; }

    [JsonProperty("modelSchema")]
    public JObject ModelSchema { get; set; }
    
    [JsonProperty("ui")]
    public ManifestUiSettings? UI { get; set; }
}