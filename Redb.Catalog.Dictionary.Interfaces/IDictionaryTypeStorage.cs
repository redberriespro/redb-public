﻿namespace Redb.Catalog.Dictionary.Interfaces
{
    /// <summary>
    /// Хранилище типов словарей.
    /// </summary>
    public interface IDictionaryTypeStorage
    {
        /// <summary>
        /// Получение типа словаря по идентификатору.
        /// </summary>
        /// <param name="typeId">Строковый идентификатор типа.</param>
        /// <returns></returns>
        Task<IDictionaryTypeData> GetDictionaryTypeAsync(string typeId, CancellationToken cancellationToken = default);

        /// <summary>
        /// Получение списка всех типов словарей.
        /// </summary>
        Task<IReadOnlyCollection<IDictionaryTypeData>> GetDictionaryTypesAsync(CancellationToken cancellationToken = default);
    }
}