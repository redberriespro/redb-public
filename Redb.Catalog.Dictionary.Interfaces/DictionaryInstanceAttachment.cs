﻿using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Interfaces
{
    public class DictionaryInstanceAttachment
    {
        public int DictionaryInstanceId;
        public string DictionaryAttachmentType = null!;
        public int DictionaryAttachmentId;
        public JObject Data = null!;
        public byte[]? Content;
        public string ContentType = null!;
        public string? FileName;
    }
}