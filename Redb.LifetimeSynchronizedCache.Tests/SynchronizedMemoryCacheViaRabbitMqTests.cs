using FluentAssertions;
using FluentAssertions.Execution;
using Microsoft.Extensions.Caching.Memory;
using Redb.LifetimeSynchronizedCache;
using Redb.LifetimeSynchronizedCache.Transport.RabbitMq;

namespace Redb.SynchronizedCache.Tests;

public class LifetimeSynchronizedMemoryCacheViaRabbitMqTests
{
    private RabbitMqTransportFactory TransportFactory;

    [OneTimeSetUp]
    public void Setup()
    {
        var options = new RabbitMqTransportOptions
        {
            ApplicationName = "Tests",
            HostNames = new string[] { "localhost" },
            UserName = "guest",
            Password = "guest",
            VirtualHost = "tests"
        };

        TransportFactory = new RabbitMqTransportFactory(options);
    }

    [Test]
    public async Task TestDefaultCache()
    {
        var options = new LifetimeSynchronizedMemoryCacheOptions
        {
            SyncEnabled = true,
            Transport = "rabbitmq"
        };

        IMemoryCache cache1 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });
        IMemoryCache cache2 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });

        var key = "some-key";
        var value = "some-value";

        cache1.Set(key, value);
        cache2.Set(key, value);

        cache1.Remove(key);

        await Task.Delay(1000);

        using var scope = new AssertionScope();

        cache1.TryGetValue(key, out _).Should().BeFalse();
        cache2.TryGetValue(key, out _).Should().BeFalse();
    }

    [Test]
    public async Task TestNamedCache()
    {
        var options = new LifetimeSynchronizedMemoryCacheOptions
        {
            Name = "test-" + Guid.NewGuid().ToString("N"),
            SyncEnabled = true,
            Transport = "rabbitmq"
        };

        IMemoryCache cache1 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });
        IMemoryCache cache2 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });

        var key = "some-key";
        var value = "some-value";

        cache1.Set(key, value);
        cache2.Set(key, value);

        cache1.Remove(key);

        await Task.Delay(1000);

        using var scope = new AssertionScope();

        cache1.TryGetValue(key, out _).Should().BeFalse();
        cache2.TryGetValue(key, out _).Should().BeFalse();
    }

    [Test]
    public async Task TestDefaulCacheAndNamedCaches()
    {
        var options = new LifetimeSynchronizedMemoryCacheOptions
        {
            SyncEnabled = true,
            Transport = "rabbitmq"
        };

        IMemoryCache defaultCache1 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });
        IMemoryCache defaultCache2 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });

        options.Name = "test-" + Guid.NewGuid().ToString("N");
        IMemoryCache namedCache = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });

        var key = "some-key";
        var value = "some-value";

        defaultCache1.Set(key, value);
        defaultCache2.Set(key, value);
        namedCache.Set(key, value);

        defaultCache1.Remove(key);

        await Task.Delay(1000);

        using var scope = new AssertionScope();

        defaultCache1.TryGetValue(key, out _).Should().BeFalse();
        defaultCache2.TryGetValue(key, out _).Should().BeFalse();
        namedCache.TryGetValue(key, out _).Should().BeTrue();
    }

    [Test]
    public async Task TestSeveralNamedCaches()
    {
        var options = new LifetimeSynchronizedMemoryCacheOptions
        {
            Name = "test-" + Guid.NewGuid().ToString("N"),
            SyncEnabled = true,
            Transport = "rabbitmq"
        };

        IMemoryCache namedCache1 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });
        IMemoryCache namedCache2 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });

        options.Name = "test-" + Guid.NewGuid().ToString("N");
        IMemoryCache namedCache3 = new LifetimeSynchronizedMemoryCache(options, new[] { TransportFactory });

        var key = "some-key";
        var value = "some-value";

        namedCache1.Set(key, value);
        namedCache2.Set(key, value);
        namedCache3.Set(key, value);

        namedCache1.Remove(key);

        await Task.Delay(1000);

        using var scope = new AssertionScope();

        namedCache1.TryGetValue(key, out _).Should().BeFalse();
        namedCache2.TryGetValue(key, out _).Should().BeFalse();
        namedCache3.TryGetValue(key, out _).Should().BeTrue();
    }
}