﻿using Autofac;
using Microsoft.Extensions.Caching.Memory;
using Redb.LifetimeSynchronizedCache.Synchronization;

namespace Redb.LifetimeSynchronizedCache.Autofac;

public class LifetimeSynchronizedMemoryCacheAutofacModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.Register(c =>
        {
            var options = c.Resolve<LifetimeSynchronizedMemoryCacheOptions>();
            var transportFactories = c.Resolve<IEnumerable<ITransportFactory>>();

            return new LifetimeSynchronizedMemoryCache(
                options,
                transportFactories);
        })
        .As<IMemoryCache>()
        .SingleInstance();
    }
}