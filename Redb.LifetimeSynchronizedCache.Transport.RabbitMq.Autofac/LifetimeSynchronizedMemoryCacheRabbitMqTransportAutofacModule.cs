﻿using Autofac;
using Redb.LifetimeSynchronizedCache.Synchronization;

namespace Redb.LifetimeSynchronizedCache.Transport.RabbitMq.Autofac;

public class LifetimeSynchronizedMemoryCacheRabbitMqTransportAutofacModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.Register(c =>
        {
            var options = c.Resolve<RabbitMqTransportOptions>();

            return new RabbitMqTransportFactory(options);
        })
        .As<ITransportFactory>()
        .SingleInstance();
    }
}