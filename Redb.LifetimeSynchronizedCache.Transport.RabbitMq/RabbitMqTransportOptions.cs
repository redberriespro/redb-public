﻿using Microsoft.Extensions.Options;
using RabbitMQ.Client;

namespace Redb.LifetimeSynchronizedCache.Transport.RabbitMq;

public class RabbitMqTransportOptions : IOptions<RabbitMqTransportOptions>
{
    public string ApplicationName { get; set; } = null!;

    public IReadOnlyCollection<string> HostNames { get; set; } = null!;

    public string VirtualHost { get; set; } = ConnectionFactory.DefaultVHost;

    public int Port { get; set; } = Constants.RabbitMqDefaultPort;

    public string UserName { get; set; } = null!;

    public string Password { get; set; } = null!;

    public IReadOnlyDictionary<string, object>? ExchangeArguments { get; set; } = null;

    public IReadOnlyDictionary<string, object>? QueueArguments { get; set; } = null;

    public IReadOnlyDictionary<string, object>? ClientProperties { get; set; } = null;

    RabbitMqTransportOptions IOptions<RabbitMqTransportOptions>.Value => this;
}
