﻿namespace Redb.LifetimeSynchronizedCache.Transport.RabbitMq.Serialization;

internal interface IKeySerializer
{
    byte[] Serialize(Guid producerId, object key);

    (Guid, object) Deserialize(byte[] key);
}
