﻿using System.Text;
using System.Text.Json;

namespace Redb.LifetimeSynchronizedCache.Transport.RabbitMq.Serialization;

internal class JsonKeySerializer : IKeySerializer
{
    public (Guid, object) Deserialize(byte[] key)
    {
        var keyContainerJson = Encoding.UTF8.GetString(key);

        var keyContainer = JsonSerializer.Deserialize<KeyContainer>(keyContainerJson);

        var type = Type.GetType(keyContainer!.Type)!;

        return (keyContainer.ProcuderId, JsonSerializer.Deserialize(keyContainer!.Value, type))!;
    }

    public byte[] Serialize(Guid procuderId, object key)
    {
        var keyContainer = new KeyContainer
        {
            ProcuderId = procuderId,
            Type = key.GetType().FullName!,
            Value = JsonSerializer.Serialize(key)
        };

        var keyContainerJson = JsonSerializer.Serialize(keyContainer);

        return Encoding.UTF8.GetBytes(keyContainerJson);
    }

    private record class KeyContainer
    {
        public Guid ProcuderId { get; init; }

        public string Type { get; init; } = null!;

        public string Value { get; init; } = null!;
    }
}
