﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using Redb.LifetimeSynchronizedCache.Synchronization;
using Redb.LifetimeSynchronizedCache.Transport.RabbitMq;

namespace Microsoft.Extensions.DependencyInjection;

public static class RabbitMqTransportServiceCollectionExtensions
{
    public static IServiceCollection AddLifetimeSynchronizedMemoryCacheRabbitMqTransport(this IServiceCollection services)
    {
        ArgumentNullException.ThrowIfNull(services);

        services.AddOptions();
        services.TryAdd(ServiceDescriptor.Singleton<ITransportFactory, RabbitMqTransportFactory>());

        return services;
    }

    public static IServiceCollection AddLifetimeSynchronizedMemoryCacheRabbitMqTransport(this IServiceCollection services, Action<RabbitMqTransportOptions> setupAction)
    {
        ArgumentNullException.ThrowIfNull(services);
        ArgumentNullException.ThrowIfNull(setupAction);

        services.AddLifetimeSynchronizedMemoryCacheRabbitMqTransport();
        services.Configure(setupAction);

        return services;
    }
}
