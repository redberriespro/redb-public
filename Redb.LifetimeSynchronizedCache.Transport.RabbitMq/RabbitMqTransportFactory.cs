﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using Redb.LifetimeSynchronizedCache.Synchronization;

namespace Redb.LifetimeSynchronizedCache.Transport.RabbitMq;

public class RabbitMqTransportFactory : ITransportFactory
{
    public string TransportType => Constants.TransportType;

    private readonly RabbitMqTransportOptions _options;
    private readonly ConnectionFactory _connectionFactory;
    private readonly ILoggerFactory _loggerFactory;

    public RabbitMqTransportFactory(IOptions<RabbitMqTransportOptions> optionsAccessor)
        : this(optionsAccessor, NullLoggerFactory.Instance)
    {
    }

    public RabbitMqTransportFactory(
        IOptions<RabbitMqTransportOptions> optionsAccessor,
        ILoggerFactory loggerFactory)
    {
        ArgumentNullException.ThrowIfNull(optionsAccessor);
        ArgumentNullException.ThrowIfNull(loggerFactory);

        _options = optionsAccessor.Value;

        _connectionFactory = new ConnectionFactory
        {
            VirtualHost = _options.VirtualHost,
            UserName = _options.UserName,
            Password = _options.Password,
            Port = _options.Port,
            AutomaticRecoveryEnabled = true,
        };

        if (_options.ClientProperties?.Any() is true)
        {
            foreach (var prop in _options.ClientProperties)
            {
                if (prop.Value is not null)
                {
                    _connectionFactory.ClientProperties.TryAdd(prop.Key, prop.Value);
                }
            }
        }

        _loggerFactory = loggerFactory;
    }

    public ITransport Create(string cacheName)
    {
        ArgumentNullException.ThrowIfNull(cacheName);

        return new RabbitMqTransport(
            _options,
            cacheName,
            _connectionFactory,
            _loggerFactory.CreateLogger<RabbitMqTransport>());
    }
}
