﻿namespace Redb.LifetimeSynchronizedCache.Transport.RabbitMq;

internal static class Constants
{
    public const string TransportType = "rabbitmq";
    public const int RabbitMqDefaultPort = 5672;
    public const string ExchangeNameTemplate = "redb.cache-{0}";
    public const string ExchangeTypeFanout = "fanout";
}
