﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Redb.LifetimeSynchronizedCache.Synchronization;
using Redb.LifetimeSynchronizedCache.Transport.RabbitMq.Serialization;

namespace Redb.LifetimeSynchronizedCache.Transport.RabbitMq;

internal class RabbitMqTransport : ITransport
{
    private readonly ILogger<RabbitMqTransport> _logger;
    private bool disposedValue;

    private readonly Guid _procuderId = Guid.NewGuid();
    private readonly RabbitMqTransportOptions _options;
    private readonly string _cacheName;
    private readonly string _exchangeName;
    private readonly string _queueName;
    private readonly IConnection _connection;
    private readonly IModel _inputChannel;
    private readonly IModel _outputChannel;
    private readonly IKeySerializer _keySerializer = new JsonKeySerializer();
    private readonly EventingBasicConsumer _consumer;
    private readonly EventHandler<BasicDeliverEventArgs> _receivedAction;
    private readonly string _consumerTag = string.Empty;

    public Action<object>? RemovalNoticeListiner { get; set; } = null;

    public RabbitMqTransport(
        IOptions<RabbitMqTransportOptions> optionsAccessor,
        string cacheName,
        ConnectionFactory connectionFactory,
        ILogger<RabbitMqTransport> logger)
    {
        ArgumentNullException.ThrowIfNull(optionsAccessor);
        ArgumentNullException.ThrowIfNull(cacheName);
        ArgumentNullException.ThrowIfNull(connectionFactory);
        ArgumentNullException.ThrowIfNull(logger);

        _options = optionsAccessor.Value;
        _cacheName = cacheName;
        _logger = logger;

        _connection = connectionFactory.CreateConnection(_options.HostNames.ToList(), _options.ApplicationName);

        _outputChannel = _connection.CreateModel();
        _inputChannel = _connection.CreateModel();

        _exchangeName = string.Format(Constants.ExchangeNameTemplate, _cacheName);

        _outputChannel.ExchangeDeclare(
            _exchangeName,
            Constants.ExchangeTypeFanout,
            durable: false,
            autoDelete: true,
           _options.ExchangeArguments?.ToDictionary(x => x.Key, x => x.Value));

        _queueName = _inputChannel.QueueDeclare(autoDelete: true).QueueName;

        _inputChannel.QueueBind(
            queue: _queueName,
            exchange: _exchangeName,
            routingKey: string.Empty);

        _consumer = new EventingBasicConsumer(_inputChannel);

        _receivedAction = (ch, ea) =>
        {
            try
            { 
                if (RemovalNoticeListiner is not null)
                {
                    var (procuderId, key) = _keySerializer.Deserialize(ea.Body.ToArray()); // TODO SPAN MEMORY WARNING

                    if (procuderId != _procuderId)
                    {
                        RemovalNoticeListiner.Invoke(key);
                    }
                }

                _inputChannel.BasicAck(ea.DeliveryTag, false);
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Unable to process message, nack-ing it", ex);
                _inputChannel.BasicNack(ea.DeliveryTag, false, true);
            }
        };

        _consumer.Received += _receivedAction;

        _consumerTag = _inputChannel.BasicConsume(
            queue: _queueName,
            autoAck: false,
            consumer: _consumer);
    }

    public Task NotifyForRemove(object key, CancellationToken cancellationToken = default)
    {
        try
        {
            var message = _keySerializer.Serialize(_procuderId, key);

            _outputChannel.BasicPublish(
                exchange: _exchangeName,
                routingKey: string.Empty,
                basicProperties: null,
                body: message);
        }
        catch (Exception ex)
        {
            var msg = "Error occurred on publish notice for key remove from cache.";
            _logger.LogError(msg, ex);
        }

        return Task.CompletedTask;
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                _consumer.Received -= _receivedAction;

                _inputChannel.BasicCancel(_consumerTag);

                _inputChannel.Close();
                _inputChannel.Dispose();

                _outputChannel.Close();
                _outputChannel.Dispose();

                _connection.Dispose();
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}