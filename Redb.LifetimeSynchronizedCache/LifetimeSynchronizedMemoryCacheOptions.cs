﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace Redb.LifetimeSynchronizedCache;

public class LifetimeSynchronizedMemoryCacheOptions : MemoryCacheOptions, IOptions<LifetimeSynchronizedMemoryCacheOptions>
{
    private string name = Constants.DefaultCacheName;

    public string Name
    {
        get => name;
        set
        {
            ArgumentNullException.ThrowIfNull(value);

            name = value;
        }
    }

    public bool SyncEnabled { get; set; } = false;

    public string? Transport { get; set; } = null;

    LifetimeSynchronizedMemoryCacheOptions IOptions<LifetimeSynchronizedMemoryCacheOptions>.Value => this;
}
