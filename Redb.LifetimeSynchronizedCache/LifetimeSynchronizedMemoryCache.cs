﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Redb.LifetimeSynchronizedCache.Synchronization;

namespace Redb.LifetimeSynchronizedCache;

public class LifetimeSynchronizedMemoryCache : IMemoryCache
{
    private readonly ILogger<LifetimeSynchronizedMemoryCache> _logger;

    private readonly LifetimeSynchronizedMemoryCacheOptions _options;
    private readonly MemoryCache _memoryCache;
    private readonly ITransport? _transport = null;

    private bool disposedValue;

    public LifetimeSynchronizedMemoryCache(
        IOptions<LifetimeSynchronizedMemoryCacheOptions> optionsAccessor,
        IEnumerable<ITransportFactory> transportFactories)
        : this(
              optionsAccessor,
              NullLoggerFactory.Instance,
              transportFactories)
    {
    }

    public LifetimeSynchronizedMemoryCache(
        IOptions<LifetimeSynchronizedMemoryCacheOptions> optionsAccessor,
        ILoggerFactory loggerFactory,
        IEnumerable<ITransportFactory> transportFactories)
    {
        ArgumentNullException.ThrowIfNull(optionsAccessor);
        ArgumentNullException.ThrowIfNull(loggerFactory);
        ArgumentNullException.ThrowIfNull(transportFactories);

        _options = optionsAccessor.Value;

        _memoryCache = new MemoryCache(optionsAccessor, loggerFactory);

        _logger = loggerFactory.CreateLogger<LifetimeSynchronizedMemoryCache>();

        if (_options.SyncEnabled)
        {
            _transport = CreateTransport(transportFactories);
            _transport.RemovalNoticeListiner = ListenRemovalNotice;
        }
    }

    public ICacheEntry CreateEntry(object key) => _memoryCache.CreateEntry(key);

    public void Remove(object key)
    {
        _memoryCache.Remove(key);

        _transport?.NotifyForRemove(key);
    }

    public bool TryGetValue(object key, out object value) => _memoryCache.TryGetValue(key, out value);

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                if (_transport is not null)
                {
                    _transport.RemovalNoticeListiner = null;
                    _transport.Dispose();
                }
                _memoryCache.Dispose();
            }

            disposedValue = true;
        }
    }

    private ITransport CreateTransport(IEnumerable<ITransportFactory> transportFactories)
    {
        var requiredTransportType = _options.Transport!.ToLowerInvariant();

        var factory = transportFactories.FirstOrDefault(x => x.TransportType.ToLowerInvariant() == requiredTransportType);

        if (factory is null)
        {
            var msg = $"Transport factory for transport '{requiredTransportType}' is not defined";

            _logger.LogError(msg);

            throw new InvalidOperationException(msg);
        }

        return factory.Create(_options.Name);
    }

    private void ListenRemovalNotice(object key)
    {
        _logger.LogInformation($"Remove {key}" );
        _memoryCache.Remove(key);
    }
}
