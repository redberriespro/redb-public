﻿namespace Redb.LifetimeSynchronizedCache.Synchronization;

public interface ITransport : IDisposable
{
    Action<object>? RemovalNoticeListiner { get; set; }

    Task NotifyForRemove(object key, CancellationToken cancellationToken = default);
}