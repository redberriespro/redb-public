﻿namespace Redb.LifetimeSynchronizedCache.Synchronization;

public interface ITransportFactory
{
    string TransportType { get; }

    ITransport Create(string cacheName);
}
