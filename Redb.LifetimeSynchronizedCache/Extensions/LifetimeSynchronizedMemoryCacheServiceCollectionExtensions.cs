﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Redb.LifetimeSynchronizedCache;

namespace Microsoft.Extensions.DependencyInjection;

public static class LifetimeSynchronizedMemoryCacheServiceCollectionExtensions
{
    public static IServiceCollection AddLifetimeSynchronizedMemoryCache(this IServiceCollection services)
    {
        ArgumentNullException.ThrowIfNull(services);

        services.AddOptions();
        services.TryAdd(ServiceDescriptor.Singleton<IMemoryCache, LifetimeSynchronizedMemoryCache>());

        return services;
    }

    public static IServiceCollection AddLifetimeSynchronizedMemoryCache(this IServiceCollection services, Action<LifetimeSynchronizedMemoryCacheOptions> setupAction)
    {
        ArgumentNullException.ThrowIfNull(services);
        ArgumentNullException.ThrowIfNull(setupAction);

        services.AddLifetimeSynchronizedMemoryCache();
        services.Configure(setupAction);

        return services;
    }
}
