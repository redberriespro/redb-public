﻿using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Enums;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Models;
using Redb.Catalog.Models.Dictionary;

namespace Redb.Catalog.Dictionary.Extensions
{
    public static class DictionaryTypeExtensions
    {
        public static DictionaryTypeMiniModel ToDictionaryTypeMiniModel(this IDictionaryTypeData dictionaryTypeData) =>
            new()
            {
                Id = dictionaryTypeData.Id,
                Code = dictionaryTypeData.Code,
                Version = dictionaryTypeData.Version,
                UIShowListColumns = dictionaryTypeData.DictionaryManifest.UI?.ListColumns ?? Array.Empty<string>(),
                TypeName = dictionaryTypeData.DictionaryManifest.TypeName,
                TypeSchema = dictionaryTypeData.DictionaryManifest.TypeSchema,
                ModelSchema = dictionaryTypeData.DictionaryManifest.ModelSchema
            };

        public static DictionaryTypeModel ToDictionaryTypeModel(this IDictionaryTypeData dictionaryTypeData) =>
            new()
            {
                Id = dictionaryTypeData.Id,
                Code = dictionaryTypeData.Code,
                Version = dictionaryTypeData.Version,
                UIShowListColumns = dictionaryTypeData.DictionaryManifest.UI?.ListColumns ?? Array.Empty<string>(),
                TypeName = dictionaryTypeData.DictionaryManifest.TypeName,
                TypeSchema = dictionaryTypeData.DictionaryManifest.TypeSchema,
                ModelSchema = dictionaryTypeData.DictionaryManifest.ModelSchema
            };

        public static IEnumerable<SortableDictionaryTypeData> Apply(this IEnumerable<SortableDictionaryTypeData> dictionaryTypes, Sorter<DictionaryTypeSortColumn> sorter)
        {
            if (sorter != null)
            {
                switch (sorter.SortType)
                {
                    case SortType.Property:
                        {
                            switch (sorter.Property)
                            {
                                case DictionaryTypeSortColumn.Name:
                                    return sorter.SortDirection == SortDirection.Asc
                                         ? dictionaryTypes.OrderBy(x => x.DictionaryTypeData.DictionaryManifest.TypeName)
                                         : dictionaryTypes.OrderByDescending(x => x.DictionaryTypeData.DictionaryManifest.TypeName);
                                case DictionaryTypeSortColumn.ItemCount:
                                    return sorter.SortDirection == SortDirection.Asc
                                         ? dictionaryTypes.OrderBy(x => x.ItemCount).ThenBy(x => x.DictionaryTypeData.Id)
                                         : dictionaryTypes.OrderByDescending(x => x.ItemCount).ThenByDescending(x => x.DictionaryTypeData.Id);
                            }

                            break;
                        }
                }
            }

            return dictionaryTypes;
        }
    }
}
