﻿using Autofac;
using Redb.Catalog.Dictionary.Data;

namespace Redb.Catalog.Dictionary.Extensions
{
    public static class AutofacExtension
    {
        public static void RegisterCatalogDictionaryDbContextFactory<TFactory>(this ContainerBuilder builder) where TFactory : ICatalogDictionaryContextFactory
        {
            ArgumentNullException.ThrowIfNull(builder);

            builder.RegisterType<TFactory>().As<ICatalogDictionaryContextFactory>();
        }
    }
}
