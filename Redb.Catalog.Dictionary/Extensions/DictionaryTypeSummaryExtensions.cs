﻿using Redb.Catalog.Dictionary.Data.Responses;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Extensions
{
    public static class DictionaryTypeSummaryExtensions
    {
        public static DictionaryTypeSummaryModel ToModel(this DictionaryTypeSummaryInfo info)
        {
            return new DictionaryTypeSummaryModel()
            {
                Count = info.Count
            };
        }
    }
}
