﻿using Redb.Catalog.Dictionary.Data.Requests;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Extensions
{
    public static class DictionaryAttachmentSingleDataRequestExtensions
    {
        public static DictionaryAttachmentListDataRequest ToDataRequest(this DictionaryAttachmentSingleRequest request)
        {
            return new DictionaryAttachmentListDataRequest(request.Type, request.DictionaryItemId, request.ExcludeContent)
            {
                Limit = int.MaxValue,
                Offset = 0,
             };
        }
    }
}
