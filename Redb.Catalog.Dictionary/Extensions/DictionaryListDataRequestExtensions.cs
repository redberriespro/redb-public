﻿using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Enums;
using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Dictionary.Data.Requests;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Extensions
{
    public static class DictionaryListDataRequestExtensions
    {
        public static DictionaryListDataRequest ToDataRequest(this DictionaryListRequest request)
        {
            return new DictionaryListDataRequest()
            {
                Type = request.Type,
                Limit = request.Limit > 0 ? request.Limit : int.MaxValue,
                Offset = request.Offset,
                SortBy = request.SortBy ?? DictionarySortColumn.Id,
                SortDirection = request.SortDirection ?? SortDirection.Desc,
                SearchQuery = request.SearchQuery,
            };
        }

        public static DictionaryFilteredListDataRequest ToDataRequest(this DictionaryFilteredListRequest request)
        {
            var req = new DictionaryFilteredListDataRequest()
            {
                SearchQuery = request.SearchQuery,
                Type = request.Type,              
                Name= request.Name,
                Id= request.Id,
                Created = request.Created,
                Modified = request.Modified,
                ShowDeleted = request.ShowDeleted,
                Sorter = request.Sorter ?? Sorter<DictionarySortColumn>.CreateSorterByProperty(DictionarySortColumn.Name, SortDirection.Asc),
                Limit = request.Limit > 0 ? request.Limit : int.MaxValue,
                Offset = request.Offset,
            };

            if (request.Filters?.Any() ?? false)
            {
                req.Filter = request.Filters.Select(f => f.ToFilter()).ComposeAnd();
            }

            return req;
        }
    }

    public static class DictionaryVersionListDataRequestExtensions
    {
        public static DictionaryVersionListDataRequest ToDataRequest(this DictionaryVersionListRequest request)
        {
            return new DictionaryVersionListDataRequest()
            {
                Limit = request.Limit > 0 ? request.Limit : int.MaxValue,
                Offset = request.Offset,
                SearchQuery = request.SearchQuery,
            };
        }
    }
}
