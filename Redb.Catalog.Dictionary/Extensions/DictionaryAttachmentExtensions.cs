﻿using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Dictionary.Entities;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Extensions
{
    public static class DictionaryAttachmentExtensions
    {
        public static DictionaryInstanceAttachment ToInstance(this DictionaryAttachmentEntity entity)
        {
            return new DictionaryInstanceAttachment()
            {
                Content = entity.Content?.Data,
                ContentType = entity.ContentType,
                FileName = entity.FileName,
                DictionaryAttachmentType = entity.Type,
                DictionaryAttachmentId = entity.Id,
                DictionaryInstanceId = entity.DictionaryEntityId,
                Data = entity.Data.ToJObject(),
            };
        }

        public static DictionaryAttachmentMiniModel ToMiniModel(this DictionaryAttachmentEntity entity)
        {
            return new DictionaryAttachmentMiniModel()
            {
                Id = entity.Id,
                Type = entity.Type,
                ContentType = entity.ContentType,
                FileName = entity.FileName,
                Created = entity.Created,
                LastModified = entity.LastModified,
                Data = entity.Data.ToJObject(),
            };
        }

        public static DictionaryAttachmentModel ToModel(this DictionaryAttachmentEntity entity)
        {
            return new DictionaryAttachmentModel()
            {
                Id = entity.Id,
                Type = entity.Type,
                ContentType = entity.ContentType,
                FileName = entity.FileName,
                Content = entity.Content?.Data,
                Created = entity.Created,
                LastModified = entity.LastModified,
                Data = entity.Data.ToJObject(),
            };
        }

        public static DictionaryAttachmentMiniModel ToMiniModel(this DictionaryAttachmentModel model)
        {
            return new DictionaryAttachmentMiniModel()
            {
                Id = model.Id,
                Type = model.Type,
                ContentType = model.ContentType,
                FileName = model.FileName,
                Created = model.Created,
                LastModified = model.LastModified,
                Data = model.Data,
            };
        }
    }
}