﻿using Redb.Catalog.Dictionary.Data.Requests;
using Redb.Catalog.Dictionary.Models;

namespace Redb.Catalog.Dictionary.Extensions
{
    public static class DictionaryAttachmentListDataRequestExtensions
    {
        public static DictionaryAttachmentListDataRequest ToDataRequest(this DictionaryAttachmentListRequest request)
        {
            return new DictionaryAttachmentListDataRequest(
                request.Type,
                request.DictionaryItemId,
                request.ExcludeContent)
            {
                Limit = request.Limit > 0 ? request.Limit : int.MaxValue,
                Offset = request.Offset,
             };
        }
    }
}
