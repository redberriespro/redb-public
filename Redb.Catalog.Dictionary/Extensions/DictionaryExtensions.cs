﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Redb.Catalog.Common.Enums;
using Redb.Catalog.Dictionary.Entities;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Models;
using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Common.BasicTypes;

namespace Redb.Catalog.Dictionary.Extensions
{
    public static class DictionaryExtensions
    {
        public static DictionaryEntity ToEntity(this DictionaryInstance instance)
        {
            return new DictionaryEntity()
            {
                Id = instance.Id,
                Type = instance.DictionaryType,
                Data = instance.Data.ToJsonDocument(),
                Name = instance.Name
            };
        }
        
        public static DictionaryInstance ToInstance(this DictionaryEntity entity)
        {
            return new DictionaryInstance()
            {
                Id = entity.Id,
                DictionaryType = entity.Type,
                Data = entity.Data.ToJObject(),
                Name = entity.Name
            };
        }

        public static DictionaryMiniModel ToMiniModel(this DictionaryEntity entity)
        {
            return new DictionaryMiniModel()
            {
                Id = entity.Id,
                Created = entity.Created,
                LastModified = entity.LastModified,
                Data = entity.Data.ToJObject(),
                Type = entity.Type,
                Name = entity.Name
            };
        }

        public static DictionaryModel ToModel(this DictionaryEntity entity)
        {
            return new DictionaryModel()
            {
                Id = entity.Id,
                Type = entity.Type,
                Created = entity.Created,
                LastModified = entity.LastModified,
                Data = entity.Data.ToJObject(),
                Name = entity.Name
            };
        }

        public static IQueryable<DictionaryEntity> Apply(this IQueryable<DictionaryEntity> nodes, Sorter<DictionarySortColumn> sorter)
        {
            if (sorter != null)
            {
                switch (sorter.SortType)
                {
                    case SortType.Property:
                        {
                            switch (sorter.Property)
                            {
                                case DictionarySortColumn.Id:
                                    return sorter.SortDirection == SortDirection.Asc
                                         ? nodes.OrderBy(x => x.Id)
                                         : nodes.OrderByDescending(x => x.Id);
                                case DictionarySortColumn.DictionaryTypeName:
                                    return sorter.SortDirection == SortDirection.Asc
                                        ? nodes.OrderBy(x => x.Type)
                                        : nodes.OrderByDescending(x => x.Type);
                                case DictionarySortColumn.Name:
                                    return sorter.SortDirection == SortDirection.Asc
                                        ? nodes.OrderBy(x => x.Name)
                                        : nodes.OrderByDescending(x => x.Name);
                                case DictionarySortColumn.LastModifiedDate:
                                    return sorter.SortDirection == SortDirection.Asc
                                        ? nodes.OrderBy(x => x.LastModified)
                                        : nodes.OrderByDescending(x => x.LastModified);
                            }

                            break;
                        }
                    case SortType.JsonProperty:
                        {
                            return sorter.SortDirection == SortDirection.Asc
                                ? nodes.OrderBy(x => x.Data.RootElement.GetProperty(sorter.JsonProperty!))
                                : nodes.OrderByDescending(x => x.Data.RootElement.GetProperty(sorter.JsonProperty!));
                        }
                }
            }

            return nodes;
        }
    }

    public static class DictionaryVersionExtension
    {
        
        public static DictionaryVersionModel ToModel(this DictionaryVersionEntity entity)
        {
            return new DictionaryVersionModel()
            {
                Id = entity.Id,
                Version= entity.Version,
                VersionDate= entity.VersionDate,
                Comment= entity.Comment,
                UserId= entity.UserId,
                UserName= entity.UserName,
                Created = entity.Created,
                LastModified = entity.LastModified,
                Data = JsonConvert.DeserializeObject<JObject>(entity.Data),
                Type = entity.Type,
                Name = entity.Name
            };
        }

    }
}
