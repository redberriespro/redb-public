﻿using Redb.Catalog.Dictionary.Extensions;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Repositories;
using Redb.Catalog.Dictionary.Telemetry;
using Redb.Telemetry.Api;

namespace Redb.Catalog.Dictionary.Storages
{
    internal class DictionaryStorage : IDictionaryStorage
    {
        private readonly DictionaryRepository _dictionaryRepository;
        private readonly DictionaryAttachmentRepository _dictionaryAttachmentRepository;

        public DictionaryStorage(
            DictionaryRepository dictionaryRepository,
            DictionaryAttachmentRepository dictionaryAttachmentRepository)
        {
            _dictionaryRepository = dictionaryRepository;
            _dictionaryAttachmentRepository = dictionaryAttachmentRepository;
        }

        public IReadOnlyCollection<DictionaryInstance> GetItemsOfType(string dictionaryType)
        {
            using var activity = Tracing.Activity.Source.StartActivity(this);

            var records =
                _dictionaryRepository
                    .GetRecords(dictionaryType)
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult()
                    .Select(r => r.ToInstance())
                    .ToList();

            return records;
        }

        public IReadOnlyCollection<DictionaryInstance> FindDictionatyItemByDataProperty(
            string dictType,
            string propertyName,
            string propertyValue)
        {
            using var activity = Tracing.Activity.Source.StartActivity(this);

            return _dictionaryRepository
                .Get(dictType, propertyName, propertyValue)
                .Where(x => !x.IsDeleted)
                .Select(x => x.ToInstance())
                .ToArray();
        }

        public IReadOnlyCollection<DictionaryInstanceAttachment> GetAttachments(
            int dictionaryInstanceId,
            string type,
            bool excludeContent)
        {
            using var activity = Tracing.Activity.Source.StartActivity(this);

            return _dictionaryAttachmentRepository
                .GetAttachments(dictionaryInstanceId, type, excludeContent, default)
                .GetAwaiter()
                .GetResult()
                .Select(x => x.ToInstance())
                .ToArray();
        }

        public DictionaryInstanceAttachment GetAttachment(int dictionaryAttachmentId, bool excludeContent)
        {
            using var activity = Tracing.Activity.Source.StartActivity(this);

            return _dictionaryAttachmentRepository
                .Get(dictionaryAttachmentId, excludeContent, default)
                .GetAwaiter()
                .GetResult()
                .ToInstance();
        }
    }
}
