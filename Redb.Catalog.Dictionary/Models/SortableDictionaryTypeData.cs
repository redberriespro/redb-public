﻿using Redb.Catalog.Dictionary.Interfaces;

namespace Redb.Catalog.Models.Dictionary
{
    public class SortableDictionaryTypeData
    {
        public IDictionaryTypeData DictionaryTypeData { get; }

        public int? ItemCount { get; }

        public SortableDictionaryTypeData(IDictionaryTypeData dictionaryTypeData, int? itemCount)
        {
            ArgumentNullException.ThrowIfNull(dictionaryTypeData, nameof(dictionaryTypeData));

            if(itemCount is not null && itemCount < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(itemCount));
            }

            DictionaryTypeData = dictionaryTypeData;
            ItemCount = itemCount;
        }
    }
}
