using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Models;

public class DictionaryUpdateRequest
{
    [JsonProperty("data")]
    public JObject Data { get; set; }

    [JsonProperty("name")]
    [Required(AllowEmptyStrings = false)]
    public string Name { get; set; }
}