﻿using Redb.Catalog.Common.Enums;
using Redb.Catalog.Common.Models;
using Redb.Catalog.Dictionary.Enums;

namespace Redb.Catalog.Dictionary.Models;

public class DictionaryListRequest : IPaginatable, ISortable<DictionarySortColumn?>, ISearchable
{
    public string Type { get; set; }
    public int Offset { get; set; }
    public int Limit { get; set; }
    public SortDirection? SortDirection { get; set; }
    public DictionarySortColumn? SortBy { get; set; }
    public string? SearchQuery { get; set; }
}