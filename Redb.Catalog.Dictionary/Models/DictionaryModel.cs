﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Models;

public class DictionaryMiniModel
{
    public int Id { get; set; }
    public DateTime Created { get; set; }
    public DateTime LastModified { get; set; }
    public JObject Data { get; set; }
    public string Type { get; set; }
    public string Name { get; set; }
}

public class DictionaryModel : DictionaryMiniModel
{
}

public class DictionaryVersionModel : DictionaryMiniModel
{
    public int Version { get; set; }

    public DateTime VersionDate { get; set; }

    public string Comment { get; set; }

    public int UserId { get; set; }

    public string UserName { get; set; }

    public List<DictionaryVersionDiffReportItem> DiffReport { get; set; }
}

public class DictionaryVersionDiffReportItem
{    
    [JsonProperty("property")]
    public string Property { get; set; }

    [JsonProperty("originalValue")]
    public JToken? OriginalValue { get; set; }

    [JsonProperty("newValue")]
    public JToken? NewValue { get; set; }
}