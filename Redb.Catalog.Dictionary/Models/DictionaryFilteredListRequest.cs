﻿using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Models;
using Redb.Catalog.Common.Models.DataFilters;
using Redb.Catalog.Dictionary.Enums;

namespace Redb.Catalog.Dictionary.Models;

public class DictionaryFilteredListRequest : IPaginatable, ISearchable
{
    public string? SearchQuery { get; set; }
    public string Type { get; set; } = null!;
    public string? Name { get; set; }
    public Range<int>? Id { get; set; }
    public Range<DateTime>? Created { get; set; }
    public Range<DateTime>? Modified { get; set; }
    public bool ShowDeleted { get; set; } = false;
    public DataFilter[]? Filters { get; set; }
    public Sorter<DictionarySortColumn>? Sorter { get; set; }
    public int Offset { get; set; }
    public int Limit { get; set; } = -1;
} 