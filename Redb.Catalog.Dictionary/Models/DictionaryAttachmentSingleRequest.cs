﻿namespace Redb.Catalog.Dictionary.Models;

public class DictionaryAttachmentSingleRequest
{
    public string Type { get; init; } = null!;

    public int DictionaryItemId { get; init; }

    public bool ExcludeContent { get; init; }
}