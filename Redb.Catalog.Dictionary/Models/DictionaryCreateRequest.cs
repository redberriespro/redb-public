﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Models
{
    public class DictionaryCreateRequest
    {
        [Required(AllowEmptyStrings = false)]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [JsonProperty("typeId")]
        public string TypeId { get; set; }

        [JsonProperty("data")]
        public JObject Data { get; set; }
    }
}
