﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;

namespace Redb.Catalog.Dictionary.Models
{
    public record DictionaryAttachmentCreateRequest
    {
        [Required(AllowEmptyStrings = false)]
        [JsonProperty("content")]
        public byte[] Content { get; init; } = null!;

        [Required(AllowEmptyStrings = false)]
        [JsonProperty("contentType")]
        public string ContentType { get; init; } = null!;

        [JsonProperty("fileName")]
        public string? FileName { get; init; }

        [JsonProperty("data")]
        public JObject Data { get; init; } = null!;
    }
}
