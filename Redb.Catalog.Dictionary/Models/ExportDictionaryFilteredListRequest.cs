﻿namespace Redb.Catalog.Dictionary.Models
{
    public class ExportDictionaryFilteredListRequest : DictionaryFilteredListRequest
    {
        public string[]? Columns { get; set; }
    }
}
