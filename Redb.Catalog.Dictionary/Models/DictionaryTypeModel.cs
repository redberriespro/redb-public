﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Models
{
    public class DictionaryTypeModel : DictionaryTypeMiniModel
    {
    }

    public class DictionaryTypeMiniModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("version")]
        public int Version { get; set; }

        [JsonProperty("typeName")]
        public string TypeName { get; set; }
        
        /// <summary>
        /// какие колонки показывать в гриде по умолчанию
        /// </summary>
        [JsonProperty("uiShowListColumns")]
        public string[] UIShowListColumns { get; set; } = { };

        [JsonProperty("typeSchema")]
        public JObject TypeSchema { get; set; }

        [JsonProperty("modelSchema")]
        public JObject ModelSchema { get; set; }
    }
}
