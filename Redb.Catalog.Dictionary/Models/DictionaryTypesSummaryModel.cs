﻿namespace Redb.Catalog.Dictionary.Models
{
    public class DictionaryTypesSummaryModel
    {
        // key is Dictionary Type ID.
        public IReadOnlyDictionary<string, DictionaryTypeSummaryModel> DictionaryTypes { get; set; }
    }
}
