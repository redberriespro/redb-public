﻿namespace Redb.Catalog.Dictionary.Models.Internals
{
    public class DictionaryTypeAttachmentsSchema
    {
        public IReadOnlyDictionary<string, DictionaryTypeAttachmentSchema> MultipleAttachments { get; }
        public IReadOnlyDictionary<string, DictionaryTypeAttachmentSchema> SingleAttachments { get; }

        public DictionaryTypeAttachmentsSchema(IEnumerable<(string, DictionaryTypeAttachmentSchema)> multipleAttachments, IEnumerable<(string, DictionaryTypeAttachmentSchema)> singleAttachments)
        {
            ArgumentNullException.ThrowIfNull(multipleAttachments);
            ArgumentNullException.ThrowIfNull(singleAttachments);

            var ma = new Dictionary<string, DictionaryTypeAttachmentSchema>();

            foreach(var (type, schema) in multipleAttachments)
            {
                ma.Add(type, schema);
            }

            MultipleAttachments = ma;

            var sa = new Dictionary<string, DictionaryTypeAttachmentSchema>();

            foreach (var (type, schema) in singleAttachments)
            {
                sa.Add(type, schema);
            }

            SingleAttachments = sa;
        }
    }
}
