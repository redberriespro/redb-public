﻿using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Models.Internals
{
    public class DictionaryTypeAttachmentSchema
    {
        public IReadOnlyCollection<string> ContentTypes { get; }

        public JObject Data { get; }

        public DictionaryTypeAttachmentSchema(IEnumerable<string> contentTypes, JObject data)
        {
            ArgumentNullException.ThrowIfNull(contentTypes);
            ArgumentNullException.ThrowIfNull(data);

            ContentTypes = contentTypes.Select(x => x.ToLowerInvariant()).ToArray();
            Data = data;
        }
    }
}
