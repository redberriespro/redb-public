﻿using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Models
{
    public record DictionaryAttachmentMiniModel
    {
        public int Id { get; init; }

        public DateTime Created { get; init; }

        public DateTime LastModified { get; init; }

        public string ContentType { get; init; } = null!;

        public string? FileName { get; init; } 

        public string Type { get; init; } = null!;

        public JObject Data { get; init; } = null!;
    }

    public record DictionaryAttachmentModel : DictionaryAttachmentMiniModel
    {
        public byte[] Content { get; init; } = null!;
    }
}