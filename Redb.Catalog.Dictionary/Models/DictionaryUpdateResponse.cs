﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Models
{
    public class DictionaryUpdateResponse
    {
        [JsonProperty("updateReport")]
        public DictionaryUpdateReport UpdateReport { get; set; }
    }

    public class DictionaryUpdateReport
    {
        [JsonProperty("items")]
        public List<DictionaryUpdateReportItem> Items { get; set; }=new List<DictionaryUpdateReportItem>();
    }

    public class DictionaryUpdateReportItem
    {
        [JsonProperty("dictionaryId")]
        public int DictionaryId { get; set; }

        [JsonProperty("dictionaryName")]
        public string DictionaryName { get; set; }

        [JsonProperty("dictionaryType")]
        public string DictionaryType { get; set; }

        [JsonProperty("originalValue")]
        public JToken? OriginalValue { get; set; }

        [JsonProperty("newValue")]
        public JToken? NewValue { get; set; }

        [JsonProperty("status")]
        public string? Status { get; set; }
    }
}
