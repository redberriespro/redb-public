﻿using Redb.Catalog.Common.Models;

namespace Redb.Catalog.Dictionary.Models;

public class DictionaryAttachmentListRequest : IPaginatable
{
    public string Type { get; init; } = null!;

    public int DictionaryItemId { get; init; }

    public bool ExcludeContent { get; init; }

    public int Offset { get; set; }

    public int Limit { get; set; }
}