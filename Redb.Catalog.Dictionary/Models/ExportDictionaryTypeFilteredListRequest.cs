﻿namespace Redb.Catalog.Dictionary.Models
{
    public class ExportDictionaryTypeFilteredListRequest : DictionaryTypeFilteredListRequest
    {
        public string[]? Columns { get; set; }
    }
}
