﻿using Redb.Catalog.Common.Models;

namespace Redb.Catalog.Dictionary.Models
{
    public class DictionaryVersionListRequest : IPaginatable, ISearchable
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public string? SearchQuery { get; set; }
    }
}
