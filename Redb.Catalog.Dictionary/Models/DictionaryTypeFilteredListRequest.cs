﻿using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Models;
using Redb.Catalog.Dictionary.Enums;

namespace Redb.Catalog.Dictionary.Models
{
    public class DictionaryTypeFilteredListRequest : IPaginatable, ISearchable
    {
        public string? SearchQuery { get; set; }
        public string? Name { get; set; }
        public Range<int>? ItemCount { get; set; }
        public Sorter<DictionaryTypeSortColumn>? Sorter { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}
