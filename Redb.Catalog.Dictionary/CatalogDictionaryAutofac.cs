using Autofac;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Repositories;
using Redb.Catalog.Dictionary.Services;
using Redb.Catalog.Dictionary.Services.Internals;
using Redb.Catalog.Dictionary.Storages;

namespace Redb.Catalog;

public class CatalogDictionaryAutofac : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        RegisterRepositories(builder);
        RegisterServices(builder);
        RegisterStorages(builder);
    }

    private static void RegisterRepositories(ContainerBuilder builder)
    {
        builder.RegisterType<DictionaryRepository>().AsSelf();
        builder.RegisterType<DictionaryAttachmentRepository>().AsSelf();
    }

    private static void RegisterServices(ContainerBuilder builder)
    {
        builder.RegisterType<DictionaryService>();
        builder.RegisterType<DictionaryAttachmentService>();
        builder.RegisterType<DictionaryTypeService>();
        builder.RegisterType<DictionaryExporterService>();
        builder.RegisterType<DictionaryTypeExporterService>();
        builder.RegisterType<DictionaryTypeAttachmentSchemaService>();
    }

    private static void RegisterStorages(ContainerBuilder builder)
    {
        builder.RegisterType<DictionaryStorage>().As<IDictionaryStorage>().SingleInstance();
    }
}