﻿using Redb.Catalog.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace Redb.Catalog.Dictionary.Entities
{
    /// <summary>
    /// Вложения справочника
    /// </summary>
    [Table($"{Constants.CATALOG_TABLE_NAME_PREFIX}dictionary_attachments")]
    public class DictionaryAttachmentEntity : IDisposable
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("key")]
        public int Id { get; set; }

        /// <summary>
        /// Тип вложения
        /// </summary>
        [Column("type")]
        [Required(AllowEmptyStrings = false)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Тип данных
        /// </summary>
        [Column("contentType")]
        [Required(AllowEmptyStrings = false)]
        public string ContentType { get; set; } = null!;

        /// <summary>
        /// Наименование файла
        /// </summary>
        [Column("fileName")]
        public string? FileName { get; set; }

        /// <summary>
        /// Данные в формате json
        /// </summary>
        [Column("data", TypeName = "jsonb")]
        public JsonDocument Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data?.Dispose();
                _data = value;
            }
        }
        /// <summary>
        /// Данные
        /// </summary>
        private JsonDocument _data = null!;

        /// <summary>
        /// Дата создания
        /// </summary>
        [Column("created")]
        public DateTime Created { get; set; }

        /// <summary>
        /// Дата редактирования
        /// </summary>
        [Column("lastModified")]
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Флаг удаления
        /// </summary>
        [Column("isDeleted")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Идентификатор справочника
        /// </summary>
        [Column("dictionaryEntityKey")]
        public int DictionaryEntityId { get; set; }

        /// <summary>
        /// Содержание вложений справочника
        /// </summary>
        public DictionaryAttachmentContentEntity Content { get; set; } = null!;

        public void Dispose()
        {
            Data?.Dispose();
            Data = null;
        }
    }
}
