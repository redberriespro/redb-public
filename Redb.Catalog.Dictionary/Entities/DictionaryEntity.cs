﻿using Redb.Catalog.Common;
using Redb.Catalog.Common.Data.Filters.Jsonb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace Redb.Catalog.Dictionary.Entities
{
    /// <summary>
    /// Справочник
    /// </summary>
    [Table($"{Constants.CATALOG_TABLE_NAME_PREFIX}dictionaries")]
    public class DictionaryEntity : IDisposable
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("key")]
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Column("name")]
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        [Column("type"), Required(AllowEmptyStrings = false)]
        public string Type { get; set; }

        /// <summary>
        /// Данные в формате json
        /// </summary>
        [JsonbFilterable]
        [Column("data", TypeName = "jsonb")]
        public JsonDocument Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data?.Dispose();
                _data = value;
            }
        }  
        /// <summary>
        /// Данные в формате json
        /// </summary>
        private JsonDocument _data;

        /// <summary>
        /// Дата создания
        /// </summary>
        [Column("created")]
        public DateTime Created { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        [Column("lastModified")]
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Флаг удаления
        /// </summary>
        [Column("isDeleted")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Коллекция вложения к справочнику
        /// </summary>
        public ICollection<DictionaryAttachmentEntity> Attachments = new HashSet<DictionaryAttachmentEntity>();

        public void Dispose()
        {
            Data?.Dispose();
        }
    }
}
