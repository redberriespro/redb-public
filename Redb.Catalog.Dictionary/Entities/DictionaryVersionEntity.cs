﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Redb.Catalog.Common;

namespace Redb.Catalog.Dictionary.Entities
{
    /// <summary>
    /// Версии справочника
    /// </summary>
    [Table($"{Constants.CATALOG_TABLE_NAME_PREFIX}dictionary_versions")]
    public class DictionaryVersionEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Column("key")]
        public int Id { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        [Column("version")]        
        public int Version { get; set; }

        /// <summary>
        /// Дата версии
        /// </summary>
        [Column("versionDate")]
        public DateTime VersionDate { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [Column("UserId")]
        public int UserId { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Column("UserName")]
        public string UserName { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [Column("comment")]
        public string Comment { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Column("name")]
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        [Column("type"), Required(AllowEmptyStrings = false)]
        public string Type { get; set; }

        /// <summary>
        /// Данные в формате json
        /// </summary>
        [Column("data", TypeName = "jsonb")]
        public string Data { get; set; } = "{}";

        /// <summary>
        /// Дата создания
        /// </summary>
        [Column("created")]
        public DateTime Created { get; set; }

        /// <summary>
        /// Дата редактирования
        /// </summary>
        [Column("lastModified")]
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Флаг удаления
        /// </summary>
        [Column("isDeleted")]
        public bool IsDeleted { get; set; }
    }
}
