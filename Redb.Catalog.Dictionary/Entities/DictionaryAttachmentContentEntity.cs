﻿using Redb.Catalog.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Redb.Catalog.Dictionary.Entities
{
    /// <summary>
    /// Содержание вложений справочника
    /// </summary>
    [Table($"{Constants.CATALOG_TABLE_NAME_PREFIX}dictionary_attachment_contents")]
    public class DictionaryAttachmentContentEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Column("dictionaryAttachmentEntityKey")]
        public int DictionaryAttachmentEntityId { get; set; }

        /// <summary>
        /// Тело вложения
        /// </summary>
        [Column("data")]
        public byte[] Data { get; set; } = null!;
    }
}
