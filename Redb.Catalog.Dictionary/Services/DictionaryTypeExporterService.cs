﻿using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Dictionary.Models;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using Telerik.Windows.Documents.Spreadsheet.Model;

namespace Redb.Catalog.Dictionary.Services
{
    public class DictionaryTypeExporterService
    {
        private const string SKALA_HDR_STYLE = "skalahdr";

        private readonly DictionaryTypeService _dictionaryTypeService;

        public DictionaryTypeExporterService(DictionaryTypeService dictionaryTypeService)
        {
            _dictionaryTypeService = dictionaryTypeService;
        }
        public async Task<CatalogFile> ExportFilteredList(IReadOnlyList<DictionaryTypeMiniModel> filteredList, ExportDictionaryTypeFilteredListRequest request)
        {
            var formatProvider = new XlsxFormatProvider();
            var workbook = new Workbook();

            var hdrStyle = workbook.Styles.Add(SKALA_HDR_STYLE);
            hdrStyle.IsBold = true;
            var worksheet = workbook.Worksheets.Add();
            worksheet.Name = "Filtered Dictionary Types";
            var columns = new List<string>();

            for (int i = 0; i < request.Columns?.Length; i++)
            {
                var columnName = "";
                var tempSplit = request.Columns[i].Split(".");
                var tempColumn = tempSplit[tempSplit.Length - 1];
                columns.Add(tempColumn);
                switch (tempColumn)
                {
                    case "typeName":
                        columnName = "Название";
                        break;
                    case "itemCount":
                        columnName = "Количество";
                        break;
                }
                worksheet.Cells[0, i].SetValue(columnName);
                worksheet.Cells[0, i].SetStyleName(SKALA_HDR_STYLE);
            }

            var counts = await _dictionaryTypeService.GetSummary();

            int rowId = 1;
            foreach (var type in filteredList)
            {
                for (int i = 0; i < columns.Count; i++)
                {
                    switch (columns[i])
                    {
                        case "typeName":
                            worksheet.Cells[rowId, i].SetValueAsText(type.TypeName);
                            break;
                        case "itemCount":
                            var count = counts.DictionaryTypes.ContainsKey(type.Id) ? counts.DictionaryTypes[type.Id].Count : 0;
                            worksheet.Cells[rowId, i].SetValue(count);
                            break;
                    }

                }
                rowId++;
            }

            for (int i = 0; i < request.Columns?.Length; i++)
            {
                worksheet.Columns[i].AutoFitWidth();
            }

            var content = formatProvider.Export(workbook);
            return new CatalogFile("Conf_DictionaryTypes.xlsx", content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }
    }
}
