﻿using Redb.Catalog.Common.Exceptions;
using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Common.Models;
using Redb.Catalog.Dictionary.Data.Requests;
using Redb.Catalog.Dictionary.Entities;
using Redb.Catalog.Dictionary.Extensions;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Models;
using Redb.Catalog.Dictionary.Models.Internals;
using Redb.Catalog.Dictionary.Repositories;
using Redb.Catalog.Dictionary.Services.Internals;
using Telerik.Windows.Documents.Spreadsheet.Expressions.Functions;

namespace Redb.Catalog.Dictionary.Services
{
    public class DictionaryAttachmentService
    {
        private const string AnyContentType = "*/*";

        private readonly DictionaryAttachmentRepository _dictionaryAttachmentRepository;
        private readonly DictionaryRepository _dictionaryRepository;
        private readonly IDictionaryTypeStorage _dictionaryTypeStorage;
        private readonly DictionaryTypeAttachmentSchemaService _dictionaryTypeAttachmentSchemaService;

        public DictionaryAttachmentService(
            DictionaryAttachmentRepository dictionaryAttachmentRepository,
            DictionaryRepository dictionaryRepository,
            IDictionaryTypeStorage dictionaryTypeStorage,
            DictionaryTypeAttachmentSchemaService dictionaryTypeAttachmentSchemaService)
        {
            _dictionaryAttachmentRepository = dictionaryAttachmentRepository;
            _dictionaryRepository = dictionaryRepository;
            _dictionaryTypeStorage = dictionaryTypeStorage;
            _dictionaryTypeAttachmentSchemaService = dictionaryTypeAttachmentSchemaService;
        }

        public async Task<ListResult<DictionaryAttachmentMiniModel>> GetList(DictionaryAttachmentListRequest request, CancellationToken cancellationToken)
        {
            var (dictionaryEntity, attachmentsSchema) = await GetDictionaryTypeAttachmentsSchema(request.DictionaryItemId, cancellationToken);

            if (!attachmentsSchema.MultipleAttachments.Any(x => x.Key.ToLowerInvariant() == request.Type.ToLowerInvariant()))
            {
                throw new CatalogBadRequestException($"Dictionary type '{dictionaryEntity.Type}' does not have attachments type '{request.Type}'.")
                        .Data(nameof(request.DictionaryItemId), request.DictionaryItemId)
                        .Data(nameof(request.Type), request.Type);
            }

            var dataResponse = await _dictionaryAttachmentRepository.Get(request.ToDataRequest(), cancellationToken).ConfigureAwait(false);

            return new ListResult<DictionaryAttachmentMiniModel>(dataResponse.Data.Select(x => x.ToMiniModel()).ToArray(), dataResponse.Total);
        }

        public async Task<DictionaryAttachmentMiniModel?> GetSingle(DictionaryAttachmentSingleRequest request, CancellationToken cancellationToken)
        {
            var (dictionaryEntity, attachmentsSchema) = await GetDictionaryTypeAttachmentsSchema(request.DictionaryItemId, cancellationToken);

            if (!attachmentsSchema.SingleAttachments.Any(x => x.Key.ToLowerInvariant() == request.Type.ToLowerInvariant()))
            {
                throw new CatalogBadRequestException($"Dictionary type '{dictionaryEntity.Type}' does not have attachment type '{request.Type}'.")
                        .Data(nameof(request.DictionaryItemId), request.DictionaryItemId)
                        .Data(nameof(request.Type), request.Type);
            }

            var dataResponse = await _dictionaryAttachmentRepository.Get(request.ToDataRequest(), cancellationToken).ConfigureAwait(false);

            return dataResponse.Data.Select(x => x.ToMiniModel()).SingleOrDefault();
        }

        public async Task<DictionaryAttachmentModel> Create(int dictionaryItemId, string type, DictionaryAttachmentCreateRequest request, CancellationToken cancellationToken)
        {
            var (dictionaryEntity, attachmentsSchema) = await GetDictionaryTypeAttachmentsSchema(dictionaryItemId, cancellationToken);


            var schema = attachmentsSchema.MultipleAttachments.SingleOrDefault(x => x.Key.ToLowerInvariant() == type.ToLowerInvariant());
            if (schema is { Key: null, Value: null })
            {
                throw new CatalogBadRequestException($"Dictionary type '{dictionaryEntity.Type}' does not have attachments type '{type}'.")
                        .Data(nameof(dictionaryItemId), dictionaryItemId)
                        .Data(nameof(type), type);
            }

            if (!schema.Value.ContentTypes.Contains(AnyContentType)
                    && !schema.Value.ContentTypes.Contains(request.ContentType.ToLowerInvariant()))
            {
                throw new CatalogBadRequestException($"Dictionary attachment type '{type}' accept only '{string.Join(';', schema.Value.ContentTypes)}' content types.")
                        .Data(nameof(request.ContentType), request.ContentType);
            }

            var existingAttachments = await _dictionaryAttachmentRepository.GetAttachments(dictionaryItemId, type, excludeContent: true, cancellationToken);
            var existing = existingAttachments.FirstOrDefault(e => string.Equals(e.FileName, request.FileName, StringComparison.CurrentCultureIgnoreCase));
            if (existing is not null)
            {
                throw new CatalogAlreadyExistsException($"Dictionary attachment for item '{dictionaryItemId}' with Type = '{type}' and FileName = '{request.FileName}' already exists in the database.")
                    .Data(nameof(dictionaryItemId), dictionaryItemId)
                    .Data(nameof(request.FileName), request.FileName)
                    .Data(nameof(type), type);

            }

            var dictionaryAttachment = await _dictionaryAttachmentRepository.Create(new DictionaryAttachmentEntity()
            {
                DictionaryEntityId = dictionaryItemId,
                Content = new DictionaryAttachmentContentEntity
                {
                    Data = request.Content
                },
                ContentType = request.ContentType,
                FileName = request.FileName,
                Type = type,
                Data = request.Data.ToJsonDocument(),
                Created = DateTime.UtcNow,
                LastModified = DateTime.UtcNow
            },
            cancellationToken);

            return dictionaryAttachment.ToModel();
        }

        public async Task<DictionaryAttachmentModel> Set(int dictionaryItemId, string type, DictionaryAttachmentSetRequest request, CancellationToken cancellationToken)
        {
            var (dictionaryEntity, attachmentsSchema) = await GetDictionaryTypeAttachmentsSchema(dictionaryItemId, cancellationToken);

            if (!attachmentsSchema.SingleAttachments.Any(x => x.Key.ToLowerInvariant() == type.ToLowerInvariant()))
            {
                throw new CatalogBadRequestException($"Dictionary type '{dictionaryEntity.Type}' does not have attachments type '{type}'.")
                        .Data(nameof(dictionaryItemId), dictionaryItemId)
                        .Data(nameof(type), type);
            }

            var schema = attachmentsSchema.SingleAttachments.Single(x => x.Key.ToLowerInvariant() == type.ToLowerInvariant());

            if (!schema.Value.ContentTypes.Contains(AnyContentType)
                    && !schema.Value.ContentTypes.Contains(request.ContentType.ToLowerInvariant()))
            {
                throw new CatalogBadRequestException($"Dictionary attachment type '{type}' accept only '{string.Join(';', schema.Value.ContentTypes)}' content types.")
                        .Data(nameof(request.ContentType), request.ContentType);
            }

            var model = await GetSingle(new DictionaryAttachmentSingleRequest
            {
                DictionaryItemId = dictionaryItemId,
                Type = type,
                ExcludeContent = true,
            }, cancellationToken);

            if (model is null)
            {
                var dictionaryAttachment = await _dictionaryAttachmentRepository.Create(new DictionaryAttachmentEntity()
                {
                    DictionaryEntityId = dictionaryItemId,
                    Content = new DictionaryAttachmentContentEntity
                    {
                        Data = request.Content
                    },
                    ContentType = request.ContentType,
                    FileName = request.FileName,
                    Type = type,
                    Data = request.Data.ToJsonDocument(),
                    Created = DateTime.UtcNow,
                    LastModified = DateTime.UtcNow
                },
                cancellationToken);

                return dictionaryAttachment.ToModel();
            }
            else
            {
                var dictionaryAttachment = await _dictionaryAttachmentRepository.Update(
                    model.Id,
                    request.Data,
                    request.Content,
                    request.ContentType,
                    request.FileName,
                    cancellationToken);

                return dictionaryAttachment.ToModel();
            }
        }

        public async Task<DictionaryAttachmentModel> Get(int id, bool excludeContent, CancellationToken cancellationToken)
        {
            var dictionary = await _dictionaryAttachmentRepository.Get(id, excludeContent, cancellationToken).ConfigureAwait(false);

            return dictionary.ToModel();
        }

        public async Task<DictionaryAttachmentModel> Get(int dictionaryItemId, string type, bool excludeContent, CancellationToken cancellationToken)
        {
            var (dictionaryEntity, attachmentsSchema) = await GetDictionaryTypeAttachmentsSchema(dictionaryItemId, cancellationToken);

            if (!attachmentsSchema.SingleAttachments.Any(x => x.Key.ToLowerInvariant() == type.ToLowerInvariant()))
            {
                throw new CatalogBadRequestException($"Dictionary type '{dictionaryEntity.Type}' does not have attachment type '{type}'.")
                        .Data(nameof(dictionaryItemId), dictionaryItemId)
                        .Data(nameof(type), type);
            }

            var dataResponse = await _dictionaryAttachmentRepository.Get(new DictionaryAttachmentListDataRequest(type, dictionaryItemId, excludeContent), cancellationToken).ConfigureAwait(false);

            var model = dataResponse.Data.Select(x => x.ToModel()).SingleOrDefault();

            if (model is null)
            {
                throw new CatalogNotFoundException($"Dictionary single attachment with type {type} not found.");
            }

            return model;
        }

        public async Task Delete(int attachmentId, CancellationToken cancellationToken)
        {
            await _dictionaryAttachmentRepository.Delete(attachmentId, cancellationToken);
        }

        public async Task Delete(int dictionaryItemId, string type, CancellationToken cancellationToken)
        {
            var (dictionaryEntity, attachmentsSchema) = await GetDictionaryTypeAttachmentsSchema(dictionaryItemId, cancellationToken);

            if (!attachmentsSchema.SingleAttachments.Any(x => x.Key.ToLowerInvariant() == type.ToLowerInvariant()))
            {
                throw new CatalogBadRequestException($"Dictionary type '{dictionaryEntity.Type}' does not have single attachment type '{type}'.")
                        .Data(nameof(dictionaryItemId), dictionaryItemId)
                        .Data(nameof(type), type);
            }

            var model = await GetSingle(new DictionaryAttachmentSingleRequest
            {
                DictionaryItemId = dictionaryItemId,
                Type = type,
            }, cancellationToken);

            if (model is null)
            {
                throw new CatalogNotFoundException($"Dictionary single attachment with type {type} not found.");
            }

            await Delete(model.Id, cancellationToken);
        }

        private async Task<(DictionaryEntity, DictionaryTypeAttachmentsSchema)> GetDictionaryTypeAttachmentsSchema(int dictionaryItemId, CancellationToken cancellationToken)
        {
            var dictionaryEntity = await _dictionaryRepository.Get(dictionaryItemId);
            var dictionaryType = await _dictionaryTypeStorage.GetDictionaryTypeAsync(dictionaryEntity.Type, cancellationToken);

            return (dictionaryEntity, _dictionaryTypeAttachmentSchemaService.ExtractAttachmentsSchema(dictionaryType.DictionaryManifest.TypeSchema));
        }
    }
}
