using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Enums;
using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Common.Models;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Extensions;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Models;
using Redb.Catalog.Dictionary.Repositories;
using Redb.Catalog.Models.Dictionary;

namespace Redb.Catalog.Dictionary.Services;

public class DictionaryTypeService
{
    private readonly IDictionaryTypeStorage _dictionaryTypeStorage;
    private readonly DictionaryRepository _dictionaryRepository;

    public DictionaryTypeService(
        DictionaryRepository dictionaryRepository, 
        IDictionaryTypeStorage dictionaryTypeStorage)
    {
        _dictionaryRepository = dictionaryRepository;
        _dictionaryTypeStorage = dictionaryTypeStorage;
    }

    public async Task<ListResult<DictionaryTypeMiniModel>> GetDictionaryTypes(DictionaryTypeListRequest request)
    {
        var dictionaryTypes = await _dictionaryTypeStorage.GetDictionaryTypesAsync().ConfigureAwait(false);
        var query = dictionaryTypes as IEnumerable<IDictionaryTypeData>;
        if (request.SearchQuery != null)
        {
            query = query.Where(x => x.DictionaryManifest.TypeName.Contains(request.SearchQuery, StringComparison.CurrentCultureIgnoreCase));
        }
        var data = query.Skip(request.Offset).Take(request.Limit > 0 ? request.Limit : int.MaxValue).Select(d => d.ToDictionaryTypeMiniModel()).ToList();
        var total = query.Count();

        return new ListResult<DictionaryTypeMiniModel>(data, total);
    }

    public async Task<ListResult<DictionaryTypeMiniModel>> GetFilteredList(DictionaryTypeFilteredListRequest request)
    {
        var sorter = request.Sorter ?? Sorter<DictionaryTypeSortColumn>.CreateSorterByProperty(DictionaryTypeSortColumn.Name, SortDirection.Desc);

        var summary = await _dictionaryRepository.GetDictionaryTypesSummary().ConfigureAwait(false);

        var dictionaryTypes = await _dictionaryTypeStorage.GetDictionaryTypesAsync().ConfigureAwait(false);

        var query = dictionaryTypes.Select(dtd => new SortableDictionaryTypeData(
            dtd,
            summary.DictionaryTypes.ContainsKey(dtd.Id) ? (int?)summary.DictionaryTypes[dtd.Id].Count : null));

        if (request.SearchQuery != null)
        {
            query = query.Where(x => x.DictionaryTypeData.DictionaryManifest.TypeName.Contains(request.SearchQuery, StringComparison.CurrentCultureIgnoreCase));
        }

        if (request.Name != null)
        {
            query = query.Where(x => x.DictionaryTypeData.DictionaryManifest.TypeName.Contains(request.Name, StringComparison.CurrentCultureIgnoreCase));
        }

        if (request.ItemCount != null)
        {
            query = query.Where(x => x.ItemCount >= request.ItemCount.Start && x.ItemCount <= request.ItemCount.End);
        }

        var data = query.Apply(sorter).Skip(request.Offset).Take(request.Limit > 0 ? request.Limit : int.MaxValue).Select(d => d.DictionaryTypeData.ToDictionaryTypeMiniModel()).ToList();
        var total = query.Count();

        return new ListResult<DictionaryTypeMiniModel>(data, total);
    }

    public async Task<DictionaryTypeModel> GetDictionaryType(string id)
    {
        var type = await _dictionaryTypeStorage.GetDictionaryTypeAsync(id);

        return type.ToDictionaryTypeModel();
    }

    public async Task<DictionaryTypesSummaryModel> GetSummary()
    {
        var dictionaryTypesSummaryResponse = await _dictionaryRepository.GetDictionaryTypesSummary().ConfigureAwait(false);

        return new DictionaryTypesSummaryModel()
        { DictionaryTypes = dictionaryTypesSummaryResponse.DictionaryTypes.ToDictionary(x => x.Key, pair => pair.Value.ToModel()) };
    }
}