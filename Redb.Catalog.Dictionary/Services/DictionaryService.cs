﻿using Newtonsoft.Json.Linq;
using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Common.Models;
using Redb.Catalog.Dictionary.Entities;
using Redb.Catalog.Dictionary.Extensions;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Models;
using Redb.Catalog.Dictionary.Repositories;

namespace Redb.Catalog.Dictionary.Services
{
    public class DictionaryService
    {
        private readonly DictionaryRepository _dictionaryRepository;

        public DictionaryService(DictionaryRepository dictionaryRepository)
        {
            _dictionaryRepository = dictionaryRepository;
        }

        public async Task<ListResult<DictionaryMiniModel>> GetList(DictionaryListRequest request)
        {
            var dataResponse = await _dictionaryRepository.Get(request.ToDataRequest()).ConfigureAwait(false);

            return new ListResult<DictionaryMiniModel>(dataResponse.Data.Select(x => x.ToMiniModel()).ToArray(), dataResponse.Total);
        }

        public async Task<ListResult<DictionaryMiniModel>> GetFilteredList(DictionaryFilteredListRequest request)
        {
            var dataResponse = await _dictionaryRepository.GetFilteredList(request.ToDataRequest()).ConfigureAwait(false);
            if (dataResponse.Data.Count == 0)
                return new ListResult<DictionaryMiniModel>(Array.Empty<DictionaryMiniModel>(), 0);

            return new ListResult<DictionaryMiniModel>(dataResponse.Data.Select(x => x.ToMiniModel()).ToArray(), dataResponse.Total);
        }

        public async Task<DictionaryModel> Get(int id)
        {
            var dictionary = await _dictionaryRepository.Get(id).ConfigureAwait(false);

            return dictionary.ToModel();
        }

        public async Task<IReadOnlyCollection<DictionaryModel>> GetLastRecords(string dictType, int numRecords)
        {
            var dictionary = await _dictionaryRepository
                .GetLastRecords(dictType, numRecords)
                .ConfigureAwait(false);

            return dictionary.Select(e => e.ToModel()).ToArray();
        }

        public async Task<DictionaryModel> Create(DictionaryCreateRequest request)
        {
            var dictionary = await _dictionaryRepository.Create(new DictionaryEntity()
            {
                Name = request.Name,
                Type = request.TypeId,
                Data = request.Data.ToJsonDocument(),
                Created = DateTime.UtcNow,
                LastModified = DateTime.UtcNow
            });

            return dictionary.ToModel();
        }

        public Task Update(int id, DictionaryUpdateRequest request)
        {
            return _dictionaryRepository.Update(id, request.Data, request.Name);
        }

        public Task Delete(int id)
        {
            return _dictionaryRepository.Delete(id);
        }

        public async Task<ListResult<DictionaryVersionModel>> GetVersionList(int id, DictionaryVersionListRequest request)
        {
            var dataResponse = await _dictionaryRepository.GetVersions(id, request.ToDataRequest()).ConfigureAwait(false);
            var result = dataResponse.Data.Select(x => x.ToModel()).ToArray();
            foreach (var item in result)
            {
                item.DiffReport = await GetVersionDiff(item.Id, item.Version);
            }
            return new ListResult<DictionaryVersionModel>(result, dataResponse.Total);
        }

        public async Task<DictionaryVersionModel> GetVersion(int id, int version)
        {
            var dictionary = await _dictionaryRepository.GetVersion(id, version).ConfigureAwait(false);

            return dictionary.ToModel();
        }

        public async Task<List<DictionaryVersionDiffReportItem>> GetVersionDiff(int id, int version)
        {
            var report = new List<DictionaryVersionDiffReportItem>();
            var dictionaryVersion = (await _dictionaryRepository.GetVersion(id, version).ConfigureAwait(false)).ToModel();

            DictionaryVersionModel dictionaryPrevVersion = null;
            if (version != 0)
            {
                dictionaryPrevVersion = (await _dictionaryRepository.GetVersion(id, version - 1).ConfigureAwait(false)).ToModel();
            }
            if (dictionaryPrevVersion != null)
            {
                if (dictionaryPrevVersion.Name != dictionaryVersion.Name)
                {
                    report.Add(new DictionaryVersionDiffReportItem
                    {
                        Property = "Название",
                        OriginalValue = dictionaryPrevVersion.Name,
                        NewValue = dictionaryVersion.Name
                    });
                }
                var isModify = !JToken.DeepEquals(dictionaryPrevVersion.Data, dictionaryVersion.Data);
                foreach (var kv in dictionaryVersion.Data)
                {
                    var prevValue = dictionaryPrevVersion.Data[kv.Key];
                    if (kv.Value != null && prevValue != null &&
                            (kv.Value.Type == JTokenType.Integer && prevValue.Type == JTokenType.Float ||
                             kv.Value.Type == JTokenType.Float && prevValue.Type == JTokenType.Integer)
                                ? kv.Value.Value<decimal>().CompareTo(prevValue.Value<decimal>()) != 0
                                : !JToken.DeepEquals(prevValue, kv.Value))
                    {
                        report.Add(new DictionaryVersionDiffReportItem()
                        {
                            Property = kv.Key,
                            OriginalValue = prevValue,
                            NewValue = kv.Value
                        });
                    }
                }
            }
            else
            {
                report.Add(new DictionaryVersionDiffReportItem()
                {
                    Property = "Название",
                    OriginalValue = null,
                    NewValue = dictionaryVersion.Name
                });
                foreach (var kv in dictionaryVersion.Data)
                {
                    report.Add(new DictionaryVersionDiffReportItem()
                    {
                        Property = kv.Key,
                        OriginalValue = null,
                        NewValue = kv.Value,
                    });
                }
            }
            return report;
        }

        public void SetCurrentUser(int userId)
        {
            _dictionaryRepository.SetCurrentUser(userId);
        }

        public async Task<List<DictionaryUpdateResponse>> ImportDictionaries(List<DictionaryInstance> instances, string[]? dictionaryNames = null, bool fullSync = false)
        {
            var result = new List<DictionaryUpdateResponse>();
            if (fullSync)
            {
                if (dictionaryNames != null && dictionaryNames.Length != 0)
                {
                    foreach (var dictionaryName in dictionaryNames)
                    {
                        var records = await _dictionaryRepository.GetRecords(dictionaryName);
                        foreach (var record in records)
                        {
                            if (!instances.Any(x => x.Name == record.Name && x.DictionaryType == dictionaryName))
                            {
                                await DeleteImpl(record, result);
                            }
                        }
                    }
                }
                else
                {
                    var records = await _dictionaryRepository.Get(new DictionaryListRequest()
                    {
                        Limit = -1
                    }.ToDataRequest());
                    foreach (var record in records.Data)
                    {
                        if (!instances.Any(x => x.Name == record.Name && x.DictionaryType == record.Type))
                            await DeleteImpl(record, result);
                    }
                }
            }

            foreach (var instance in instances)
            {
                if (dictionaryNames == null || dictionaryNames.Length == 0 || dictionaryNames.Contains(instance.DictionaryType))
                {
                    var records = await _dictionaryRepository.GetRecords(instance.DictionaryType);
                    var record = records.FirstOrDefault(x => x.Name == instance.Name);
                    if (record != null)
                    {
                        var data = record.Data.ToJObject();
                        if (!JObject.DeepEquals(data, instance.Data))
                        {
                            await _dictionaryRepository.Update(record.Id, instance.Data, instance.Name);
                            var report = new DictionaryUpdateReport();
                            report.Items.Add(new DictionaryUpdateReportItem()
                            {
                                DictionaryId = record.Id,
                                DictionaryType = record.Type,
                                DictionaryName = record.Name,
                                NewValue = instance.Data,
                                Status = "Updated"
                            });
                            result.Add(new DictionaryUpdateResponse() { UpdateReport = report });
                        }
                    }
                    else
                    {
                        var entity = instance.ToEntity();
                        var utcNow = DateTime.UtcNow;
                        entity.LastModified = utcNow;
                        entity.Created = utcNow;
                        await _dictionaryRepository.Create(entity);
                        var report = new DictionaryUpdateReport();

                        report.Items.Add(new DictionaryUpdateReportItem()
                        {
                            DictionaryId = entity.Id,
                            DictionaryType = entity.Type,
                            DictionaryName = entity.Name,
                            NewValue = entity.Data.ToJObject(),
                            Status = "Created"
                        });
                        result.Add(new DictionaryUpdateResponse() { UpdateReport = report });
                    }
                }
            }
            return result;
            async Task DeleteImpl(DictionaryEntity record, List<DictionaryUpdateResponse> result)
            {
                await _dictionaryRepository.Delete(record.Id);
                var report = new DictionaryUpdateReport();
                report.Items.Add(new DictionaryUpdateReportItem()
                {
                    DictionaryId = record.Id,
                    DictionaryType = record.Type,
                    DictionaryName = record.Name,
                    Status = "Deleted",
                });
                result.Add(new DictionaryUpdateResponse() { UpdateReport = report });
            }

        }
    }
}
