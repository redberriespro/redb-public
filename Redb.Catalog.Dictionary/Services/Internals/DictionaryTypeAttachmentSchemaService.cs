﻿using Newtonsoft.Json.Linq;
using Redb.Catalog.Dictionary.Models.Internals;

namespace Redb.Catalog.Dictionary.Services.Internals
{
    public class DictionaryTypeAttachmentSchemaService
    {
        public DictionaryTypeAttachmentsSchema ExtractAttachmentsSchema(JObject typeSchema)
        {
            ArgumentNullException.ThrowIfNull(typeSchema);

            var singleAttachments = ExtractSingleAttachmentSchemas(typeSchema);
            var multipleAttachments = ExtractMultipleAttachmentSchemas(typeSchema);

            return new DictionaryTypeAttachmentsSchema(multipleAttachments, singleAttachments);
        }

        private static List<(string, DictionaryTypeAttachmentSchema)> ExtractSingleAttachmentSchemas(JObject typeSchema)
        {
            var singleAttachments = new List<(string, DictionaryTypeAttachmentSchema)>();

            var properties = (JObject)typeSchema["properties"]!;

            foreach (var property in properties!.Properties())
            {
                var value = (JObject)property.Value;

                if (value.Value<bool>("attachment") == true)
                {
                    var schema = ExtractAttachmentSchema(value);

                    singleAttachments.Add((property.Name, schema));
                }
            }

            return singleAttachments;
        }

        private static List<(string, DictionaryTypeAttachmentSchema)> ExtractMultipleAttachmentSchemas(JObject typeSchema)
        {
            var multipleAttachments = new List<(string, DictionaryTypeAttachmentSchema)>();

            var properties = (JObject)typeSchema["properties"]!;

            foreach (var property in properties!.Properties())
            {
                var value = (JObject)property.Value;

                if (value.Value<bool>("attachments") == true)
                {
                    var schema = ExtractAttachmentSchema((JObject)value["items"]!);

                    multipleAttachments.Add((property.Name, schema));
                }
            }

            return multipleAttachments;
        }

        private static DictionaryTypeAttachmentSchema ExtractAttachmentSchema(JObject value)
        {
            var contentTypes = value["properties"]!["content"]!.Value<string>("contentMediaType").Split(";", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);

            var jProperties = new JObject();

            var jData = new JObject
            {
                ["type"] = "object",
                ["properties"] = jProperties
            };

            foreach (var p in ((JObject)value["properties"]!).Properties())
            {
                if (p.Name != "content")
                {
                    jProperties.Add(p.Name, p.Value);
                }
            }

            var schema = new DictionaryTypeAttachmentSchema(contentTypes, jData);
            return schema;
        }
    }
}
