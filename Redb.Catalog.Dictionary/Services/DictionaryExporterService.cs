﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Dictionary.Models;
using System.IO.Compression;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using Telerik.Windows.Documents.Spreadsheet.Model;

namespace Redb.Catalog.Dictionary.Services
{
    public class DictionaryExporterService
    {
        private const string SKALA_HDR_STYLE = "skalahdr";
        private const string DATE_FORMAT = "dd.mm.yyyy";

        private readonly DictionaryTypeService _dictionaryTypeService;
        private readonly DictionaryService _dictionaryService;

        public DictionaryExporterService(DictionaryTypeService dictionaryTypeService, DictionaryService dictionaryService)
        {
            _dictionaryTypeService = dictionaryTypeService;
            _dictionaryService = dictionaryService;
        }

        public async Task<CatalogFile> ExportToJson()
        {
            var nodeTypes = await _dictionaryTypeService.GetDictionaryTypes(new DictionaryTypeListRequest
            {
                Limit = 100
            });
            using (var resultFile = new MemoryStream())
            {
                using (var zip = new ZipArchive(resultFile, ZipArchiveMode.Create, true))
                {
                    foreach (var nodeType in nodeTypes.Result)
                    {
                        var dictEntry = zip.CreateEntry($"{nodeType.Id}.json");
                        var contents = new JArray();
                        var records = await _dictionaryService.GetList(new DictionaryListRequest
                        {
                            Type = nodeType.Id,
                            Limit = 10000
                        });
                        foreach (var record in records.Result)
                        {
                            if (record.Data["title"] == null)
                                record.Data.Add("title", record.Name);
                            else
                                record.Data["title"] = record.Name;
                            contents.Add(record.Data);
                        }
                        using (var writer = new StreamWriter(dictEntry.Open()))
                        {
                            using (var jsonWriter = new JsonTextWriter(writer))
                            {
                                contents.WriteTo(jsonWriter);
                            }
                        }

                    }
                }
                resultFile.Seek(0, SeekOrigin.Begin);
                return new CatalogFile("Conf_Dictionaries.zip", resultFile.ToArray(), "application/zip");
            }
        }

        public async Task<CatalogFile> ExportFilteredList(IReadOnlyList<DictionaryMiniModel> filteredList, ExportDictionaryFilteredListRequest request)
        {
            var formatProvider = new XlsxFormatProvider();
            var workbook = new Workbook();

            var hdrStyle = workbook.Styles.Add(SKALA_HDR_STYLE);
            hdrStyle.IsBold = true;
            var worksheet = workbook.Worksheets.Add();
            worksheet.Name = "Filtered Dictionary Items";
            var columns = new List<string>();
            var dataSchema = _dictionaryTypeService.GetDictionaryType(request.Type).Result.TypeSchema;

            for (int i = 0; i < request.Columns?.Length; i++)
            {
                var columnName = "";
                var tempSplit = request.Columns[i].Split(".");
                var tempColumn = tempSplit[tempSplit.Length - 1];
                columns.Add(tempColumn);
                switch (tempColumn)
                {
                    case "id":
                        columnName = "Id";
                        break;
                    case "name":
                        columnName = "Название";
                        break;
                    case "type":
                        columnName = "Тип";
                        break;
                    case "lastModified":
                        columnName = "Дата изменения";
                        break;
                    case "created":
                        columnName = "Дата создания";
                        break;
                    default:
                        if (dataSchema["properties"][tempColumn] != null &&
                                dataSchema["properties"][tempColumn].Type != JTokenType.Null)
                        {
                            columnName = dataSchema["properties"][tempColumn]["title"].ToString();
                        }
                        break;
                }
                worksheet.Cells[0, i].SetValue(columnName);
                worksheet.Cells[0, i].SetStyleName(SKALA_HDR_STYLE);
            }

            int rowId = 1;
            foreach (var type in filteredList)
            {
                for (int i = 0; i < columns.Count; i++)
                {
                    switch (columns[i])
                    {
                        case "id":
                            worksheet.Cells[rowId, i].SetValue(type.Id);
                            break;
                        case "name":
                            worksheet.Cells[rowId, i].SetValueAsText(type.Name);
                            break;
                        case "type":
                            worksheet.Cells[rowId, i].SetValueAsText(type.Type);
                            break;
                        case "lastModified":
                            worksheet.Cells[rowId, i].SetValue(type.LastModified);
                            worksheet.Cells[rowId, i].SetFormat(new CellValueFormat(DATE_FORMAT));
                            break;
                        case "created":
                            worksheet.Cells[rowId, i].SetValue(type.Created);
                            worksheet.Cells[rowId, i].SetFormat(new CellValueFormat(DATE_FORMAT));
                            break;
                        default:
                            if (type.Data[columns[i]] != null && type.Data[columns[i]].Type != JTokenType.Null)
                            {
                                worksheet.Cells[rowId, i].SetValueAsText(type.Data[columns[i]].ToString());
                            }
                                
                            break;
                    }

                }
                rowId++;
            }

            for (int i = 0; i < request.Columns?.Length; i++)
            {
                worksheet.Columns[i].AutoFitWidth();
            }

            var content = formatProvider.Export(workbook);
            return new CatalogFile("Conf_DictionaryItems.xlsx", content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }
    }
}
