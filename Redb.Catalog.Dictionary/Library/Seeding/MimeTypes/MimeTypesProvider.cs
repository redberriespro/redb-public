﻿using Newtonsoft.Json;
using System.Reflection;
using System.Text;

namespace Redb.Catalog.Dictionary.Library.Seeding.MimeTypes
{
    public class MimeTypesProvider
    {
        private readonly Dictionary<string, string> _mimeTypes;

        public MimeTypesProvider()
        {
            var mapResourceName = GetType().Namespace + "." + "map.json";

            using var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(mapResourceName)!;
            using var streamReader = new StreamReader(stream, Encoding.UTF8);

            var json = streamReader.ReadToEnd();

            _mimeTypes = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }

        public string GetMimeType(string fileExtension)
        {
            ArgumentNullException.ThrowIfNull(fileExtension);

            return _mimeTypes[fileExtension];
        }

        public void AddMap(string fileExtension, string mimeType, bool replace = false)
        {
            ArgumentNullException.ThrowIfNull(fileExtension);
            ArgumentNullException.ThrowIfNull(mimeType);

            if (replace)
            {
                _mimeTypes[fileExtension] = mimeType;
            }
            else
            {
                _mimeTypes.Add(fileExtension, mimeType);
            }
        }
    }
}
