﻿using Newtonsoft.Json.Linq;

namespace Redb.Catalog.Dictionary.Interfaces
{
    public record DictionaryItemAttachmentSeed
    {
        public byte[] Content { get; init; } = null!;

        public string ContentType { get; init; } = null!;

        public string? FileName { get; init; }

        public JObject Data { get; init; } = null!;
    }
}