﻿using Newtonsoft.Json.Linq;
using Redb.Catalog.Dictionary.Interfaces;

namespace Redb.Catalog.Dictionary.Library.Seeding
{
    public record DictionaryItemSeed
    {
        public string Type { get; init; } = null!;

        public string Name { get; init; } = null!;

        public JObject Data { get; init; } = null!;

        public IReadOnlyDictionary<string, DictionaryItemAttachmentSeed> SingleAttachments { get; init; } = null!;

        public IReadOnlyDictionary<string, IReadOnlyCollection<DictionaryItemAttachmentSeed>> MultipleAttachments { get; init; } = null!;
    }
}
