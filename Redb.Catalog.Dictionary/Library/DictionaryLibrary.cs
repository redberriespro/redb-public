﻿using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Redb.Catalog.Dictionary.Interfaces;
using Redb.Catalog.Dictionary.Library.Seeding;
using Redb.Catalog.Dictionary.Library.Seeding.MimeTypes;

namespace Redb.Catalog.Dictionary.Library
{
    public class DictionaryLibrary
    {
        private const string SeedAttachmentsPropertyName = "@attachments@";
        private ILog _log = LogManager.GetLogger(typeof(DictionaryLibrary));
        private readonly string _folder;
        private readonly MimeTypesProvider _mimeTypesProvider = new MimeTypesProvider();

        public DictionaryLibrary(string folder)
        {
            _folder = Path.IsPathRooted(folder) ? folder : Path.Combine(Directory.GetCurrentDirectory(), folder);
        }

        public IReadOnlyList<DictionaryFolderInfo> EnumerateDictionaries()
        {
            var folderName = "dictionaries";
            var searchPattern = "dictionary.json";

            var result = new List<DictionaryFolderInfo>();

            var path = Path.Combine(_folder, folderName);

            if (!Directory.Exists(path))
                return result;

            var manifestFilePathes = Directory.GetFiles(path,
                searchPattern, SearchOption.AllDirectories);
            foreach (var manifestFilePath in manifestFilePathes)
            {
                var folderInfo = new DictionaryFolderInfo();

                var parentDir = Directory.GetParent(manifestFilePath);
                folderInfo.AbsolutePath = parentDir.FullName;
                folderInfo.Version = int.Parse(parentDir.Name);
                folderInfo.Code = Directory.GetParent(parentDir.FullName).Name;

                result.Add(folderInfo);
            }

            return result;
        }

        public async Task<DictionaryManifest> LoadDictionaryManifest(DictionaryFolderInfo info)
        {
            _log.Debug($"loading dict type from {info.AbsolutePath}");
            var manifestFileName = Path.Combine(info.AbsolutePath, "dictionary.json");
            var manifest = JsonConvert.DeserializeObject<DictionaryManifest>(await File.ReadAllTextAsync(manifestFileName).ConfigureAwait(false));
            return manifest;
        }

        public async IAsyncEnumerable<DictionaryItemSeed> EnumerateSeedDictionaries()
        {
            var path = Path.Combine(_folder, "seed", "dictionaries");
            if (!Directory.Exists(path))
                yield break;

            var files = Directory.GetFiles(path, "*.json", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var dataArray = JsonConvert.DeserializeObject<JObject[]>(await File.ReadAllTextAsync(file).ConfigureAwait(false));
                var type = Path.GetFileNameWithoutExtension(file);
                var attachmentsPath = Path.Combine(Directory.GetParent(file).FullName, "attachments", type);

                foreach (var data in dataArray)
                {
                    var singleAttachments = new Dictionary<string, DictionaryItemAttachmentSeed>();

                    var multipleAttachments = new Dictionary<string, List<DictionaryItemAttachmentSeed>>();

                    if (data.ContainsKey(SeedAttachmentsPropertyName))
                    {
                        (singleAttachments, multipleAttachments) = await ParseAttachmentSetRequestsAsync((JObject)data[SeedAttachmentsPropertyName], attachmentsPath);

                        data.Remove(SeedAttachmentsPropertyName);
                    }

                    var entity = new DictionaryItemSeed
                    {
                        Data = data,
                        Name = data["title"]?.Value<string>(),
                        Type = type,
                        SingleAttachments = singleAttachments,
                        MultipleAttachments = multipleAttachments.ToDictionary(kv => kv.Key, kv => kv.Value as IReadOnlyCollection<DictionaryItemAttachmentSeed>),
                    };

                    yield return entity;
                }
            }
        }

        private async Task<(Dictionary<string, DictionaryItemAttachmentSeed>, Dictionary<string, List<DictionaryItemAttachmentSeed>>)> ParseAttachmentSetRequestsAsync(JObject jObject, string attachmentsPath)
        {
            var singles = new Dictionary<string, DictionaryItemAttachmentSeed>();
            var sets = new Dictionary<string, List<DictionaryItemAttachmentSeed>>();

            foreach (var property in jObject)
            {
                if (property.Value.Type == JTokenType.Array)
                {
                    var attachments = await ParseAttachmentsAsync((JArray)property.Value, attachmentsPath);

                    sets.Add(property.Key, attachments);
                }
                else
                {
                    var attachment = await ParseAttachmentAsync((JObject)property.Value, attachmentsPath);

                    singles.Add(property.Key, attachment);
                }
            }

            return (singles, sets);
        }

        private async Task<DictionaryItemAttachmentSeed> ParseAttachmentAsync(JObject jObject, string attachmentsPath)
        {
            var content = jObject["content"]?.Value<string>();

            if (content is null)
            {
                throw new Exception("Property 'content' must be defined for dictionary attachment seed.");
            }

            var contentType = jObject["contentType"]?.Value<string>();

            if(contentType is null)
            {
                var fileExtension = content.Split(".").Last();

                contentType = _mimeTypesProvider.GetMimeType(fileExtension);
            }
            
            var fileName = jObject["filename"]?.Value<string>();

            fileName ??= content;

            return new DictionaryItemAttachmentSeed
            {
                FileName = fileName,
                ContentType = contentType,
                Content = await ReadAttachmentContentAsync(content, attachmentsPath),
                Data = (jObject["data"] as JObject) ?? new JObject(),
            };
        }

        private async Task<List<DictionaryItemAttachmentSeed>> ParseAttachmentsAsync(JArray jArray, string attachmentsPath)
        {
            var attachments = new List<DictionaryItemAttachmentSeed>();

            foreach (var jObject in jArray)
            {
                var attachment = await ParseAttachmentAsync((JObject)jObject, attachmentsPath);

                attachments.Add(attachment);
            }

            return attachments;
        }

        private async Task<byte[]> ReadAttachmentContentAsync(string fileName, string directory)
        {
            var filePath = Path.Combine(directory, fileName);

            return await File.ReadAllBytesAsync(filePath);
        }
    }
}
