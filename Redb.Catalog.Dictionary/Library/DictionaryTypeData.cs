using Redb.Catalog.Dictionary.Interfaces;

namespace Redb.Catalog.Dictionary.Library;

public class DictionaryTypeData : IDictionaryTypeData
{
    public DictionaryManifest DictionaryManifest { get; }

    public string Id { get; private set; }
    public string Code { get; private set; }
    public int Version { get; private set; }

    public DictionaryTypeData(DictionaryManifest dictionaryManifest)
    {
        DictionaryManifest = dictionaryManifest;
    }

    public void Initialize(DictionaryFolderInfo nodeFolderInfo)
    {
        // https://redberries.atlassian.net/wiki/spaces/SKALA/pages/1854767122
        Code = nodeFolderInfo.Code.ToUpper();
        Version = nodeFolderInfo.Version;
        Id = $"{Code}.{Version}";
    }
}