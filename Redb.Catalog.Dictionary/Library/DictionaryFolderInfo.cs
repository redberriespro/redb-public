namespace Redb.Catalog.Dictionary.Library;

public class DictionaryFolderInfo
{
    public string Code { get; set; }

    public int Version { get; set; }

    public string AbsolutePath { get; set; }
}