﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Redb.Catalog.Dictionary.Enums
{
    [JsonConverter(typeof(StringEnumConverter), typeof(CamelCaseNamingStrategy))]
    public enum DictionarySortColumn
    {
        Id,
        DictionaryTypeName,
        Name,
        LastModifiedDate
    }
}
