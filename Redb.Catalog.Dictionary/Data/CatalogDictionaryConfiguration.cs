﻿using Microsoft.EntityFrameworkCore;
using Redb.Catalog.Dictionary.Entities;

namespace Redb.Catalog.Dictionary.Data
{
    public static class CatalogDictionaryConfiguration
    {
        public static void ConfigureModel(ModelBuilder builder)
        {
            builder.Entity<DictionaryVersionEntity>()
                   .HasKey(a => new { a.Id, a.Version });

            builder.Entity<DictionaryVersionEntity>()
                   .Property(e => e.VersionDate)
                   .HasConversion(
                        v => v,
                        v => new DateTime(v.Ticks, DateTimeKind.Utc));

            builder.Entity<DictionaryVersionEntity>()
                   .Property(e => e.Created)
                   .HasConversion(
                        v => v,
                        v => new DateTime(v.Ticks, DateTimeKind.Utc));

            builder.Entity<DictionaryVersionEntity>()
                   .Property(e => e.LastModified)
                   .HasConversion(
                        v => v,
                        v => new DateTime(v.Ticks, DateTimeKind.Utc));

            builder.Entity<DictionaryEntity>()
                   .HasMany(e => e.Attachments)
                   .WithOne()
                   .HasForeignKey(e => e.DictionaryEntityId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<DictionaryAttachmentEntity>()
                   .HasOne(e => e.Content)
                   .WithOne()
                   .HasForeignKey<DictionaryAttachmentContentEntity>(e => e.DictionaryAttachmentEntityId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<DictionaryAttachmentContentEntity>()
                   .HasKey(e => e.DictionaryAttachmentEntityId);

            builder.Entity<DictionaryEntity>()
                   .HasIndex(e => new { e.Name, e.Type })
                   .IsUnique()
                   .HasFilter("NOT \"isDeleted\"");
        }
    }
}