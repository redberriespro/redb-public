﻿using Redb.Catalog.Common.Data.EntityVersioning;
using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Dictionary.Entities;

namespace Redb.Catalog.Dictionary.Data
{
    public class DictionaryVersionTracker<TDBContext> : EntityVersionTracker<TDBContext, DictionaryEntity>
        where TDBContext: ICatalogDictionaryContext, IChangeTrackeredContext
    {
        public DictionaryVersionTracker(TDBContext dBContext)
            : base(dBContext) { }

        public override void GenerateVersions(int changeAuthorId, string changeAuthorName)
        {
            ArgumentNullException.ThrowIfNull(changeAuthorName);

            foreach (var entry in _addedEntries)
            {
                var latestVersion = _dbContext.DictionaryVersions.Where(x => x.Id == entry.Entity.Id).Max(x => x.Version as int?) ?? -1;
                _dbContext.DictionaryVersions.AddAsync(new DictionaryVersionEntity()
                {
                    Id = entry.Entity.Id,
                    Version = latestVersion + 1,
                    VersionDate = entry.Entity.Created,
                    UserId = changeAuthorId,
                    UserName = changeAuthorName,
                    Comment = $"{(changeAuthorId == 0 ? "Импорт объекта" : "Пользователь создал объект")}",
                    Name = entry.Entity.Name,
                    Type = entry.Entity.Type,
                    Data = entry.Entity.Data.ToJsonString(),
                    Created = entry.Entity.Created,
                    LastModified = entry.Entity.LastModified,
                    IsDeleted = entry.Entity.IsDeleted,

                });
            }
            foreach (var entry in _modifiedEntries)
            {
                var latestVersion = _dbContext.DictionaryVersions.Where(x => x.Id == entry.Entity.Id).Max(x => x.Version as int?) ?? -1;

                _dbContext.DictionaryVersions.AddAsync(new DictionaryVersionEntity()
                {
                    Id = entry.Entity.Id,
                    Version = latestVersion + 1,
                    VersionDate = entry.Entity.LastModified,
                    UserId = changeAuthorId,
                    UserName = changeAuthorName,
                    Comment = entry.Entity.IsDeleted ?
                              $"{(changeAuthorId == 0 ? "Импорт объекта" : "Пользователь удалил объект")}" :
                              $"{(changeAuthorId == 0 ? "Импорт объекта" : "Пользователь изменил объект")}",
                    Name = entry.Entity.Name,
                    Type = entry.Entity.Type,
                    Data = entry.Entity.Data.ToJsonString(),
                    Created = entry.Entity.Created,
                    LastModified = entry.Entity.LastModified,
                    IsDeleted = entry.Entity.IsDeleted,
                });
            }
        }
    }
}
