﻿namespace Redb.Catalog.Dictionary.Data.Responses
{
    public class DictionaryTypesSummaryResponse
    {
        public IReadOnlyDictionary<string, DictionaryTypeSummaryInfo> DictionaryTypes { get; set; }
    }

    public class DictionaryTypeSummaryInfo
    {
        public int Count { get; set; }
    }
}
