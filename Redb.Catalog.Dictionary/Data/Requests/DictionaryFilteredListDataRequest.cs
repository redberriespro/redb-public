﻿using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Data.Filters;
using Redb.Catalog.Common.Data.Requests;
using Redb.Catalog.Dictionary.Enums;

namespace Redb.Catalog.Dictionary.Data.Requests
{
    public class DictionaryFilteredListDataRequest : IPaginatableRequest, ISearchableRequest
    {
        public string? SearchQuery { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public Range<int>? Id { get; set; }
        public Range<DateTime>? Created { get; set; }
        public Range<DateTime>? Modified { get; set; }
        public IFilter? Filter { get; set; }
        public bool ShowDeleted { get; set; }
        public Sorter<DictionarySortColumn>? Sorter { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}
