﻿using Redb.Catalog.Common.Data.Requests;

namespace Redb.Catalog.Dictionary.Data.Requests
{
    public class DictionaryVersionListDataRequest : IPaginatableRequest, ISearchableRequest
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public string? SearchQuery { get; set; }
    }
}
