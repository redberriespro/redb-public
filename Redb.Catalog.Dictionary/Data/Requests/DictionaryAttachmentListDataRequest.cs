﻿using Redb.Catalog.Common.Data.Requests;
using Redb.Catalog.Common.Extensions;

namespace Redb.Catalog.Dictionary.Data.Requests
{
    public class DictionaryAttachmentListDataRequest : IPaginatableRequest
    {
        public string Type { get; }

        public int DictionaryEntityId { get; }

        public bool ExcludeContent { get; }

        public int Offset { get; set; } = 0;

        public int Limit { get; set; } = int.MaxValue;

        public DictionaryAttachmentListDataRequest(
            string type,
            int dictionaryEntityId,
            bool excludeContent)
        {
            ArgumentNullException.ThrowIfNull(type);

            if (dictionaryEntityId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dictionaryEntityId), "Dictionary entity id must be positive.")
                        .Data(nameof(DictionaryEntityId), dictionaryEntityId);
            }

            Type = type;
            DictionaryEntityId = dictionaryEntityId;
            ExcludeContent = excludeContent;
        }
    }
}
