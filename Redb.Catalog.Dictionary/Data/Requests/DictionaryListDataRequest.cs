﻿using Redb.Catalog.Common.Data.Requests;
using Redb.Catalog.Common.Enums;
using Redb.Catalog.Dictionary.Enums;

namespace Redb.Catalog.Dictionary.Data.Requests
{
    public class DictionaryListDataRequest : IPaginatableRequest, ISortableRequest<DictionarySortColumn>, ISearchableRequest
    {
        public string Type { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
        public SortDirection SortDirection { get; set; }
        public DictionarySortColumn SortBy { get; set; }
        public string? SearchQuery { get; set; }
    }
}
