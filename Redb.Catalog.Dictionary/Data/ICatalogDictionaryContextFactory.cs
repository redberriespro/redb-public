namespace Redb.Catalog.Dictionary.Data;

public interface ICatalogDictionaryContextFactory
{
    ICatalogDictionaryContext CreateCatalogDictionaryContext();

    int UserId { get; set; }
}