using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Redb.Catalog.Dictionary.Entities;

namespace Redb.Catalog.Dictionary.Data;

public interface ICatalogDictionaryContext : IAsyncDisposable, IDisposable
{
    /// <summary>
    /// ��������� ������������
    /// </summary>
    DbSet<DictionaryEntity> Dictionaries { get; set; }

    DbSet<DictionaryAttachmentEntity> DictionaryAttachments { get; set; }

    DbSet<DictionaryVersionEntity> DictionaryVersions { get; set; }

    public Task SaveChangesAsync();

    EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

    int UserId { get; set; }
}