﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Redb.Catalog.Common.Data;
using Redb.Catalog.Common.Data.Responses;
using Redb.Catalog.Common.Enums;
using Redb.Catalog.Common.Exceptions;
using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Dictionary.Data;
using Redb.Catalog.Dictionary.Data.Requests;
using Redb.Catalog.Dictionary.Data.Responses;
using Redb.Catalog.Dictionary.Entities;
using Redb.Catalog.Dictionary.Enums;
using Redb.Catalog.Dictionary.Extensions;

namespace Redb.Catalog.Dictionary.Repositories
{
    public class DictionaryRepository
    {
        private readonly ICatalogDictionaryContextFactory _contextFactory;

        public DictionaryRepository(ICatalogDictionaryContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public IReadOnlyList<DictionaryEntity> Get(string dictType, string propName, string propValue)
        {
            using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var res = ctx
                .Dictionaries
                .FromSqlInterpolated($"SELECT * FROM catalog_dictionaries WHERE \"data\" ->> {propName} = {propValue}")
                .Where(x => dictType == x.Type && !x.IsDeleted)
                .ToList();

            return res;
        }

        public async Task<DictionaryEntity> Get(int id)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var result = await ctx.Dictionaries.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted).ConfigureAwait(false);
            if (result == null)
            {
                throw new CatalogNotFoundException($"Dictionary with id {id} not found");
            }

            return result;
        }

        public async Task<IReadOnlyCollection<DictionaryEntity>> GetRecords(string typeId)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var result = await ctx
                .Dictionaries
                .Where(x => x.Type == typeId && !x.IsDeleted)
                .OrderBy(x => x.Name)
                .ToListAsync()
                .ConfigureAwait(false);

            return result;
        }

        public async Task<IReadOnlyCollection<DictionaryEntity>> GetLastRecords(string typeId, int numRecords)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var result = await ctx.Dictionaries.Where(x => x.Type == typeId && !x.IsDeleted)
                .OrderByDescending(x => x.LastModified)
                .Take(numRecords)
                .ToListAsync()
                .ConfigureAwait(false);

            return result;
        }

        [Obsolete("use GetFiltered instead")]
        public async Task<ListDataResponse<DictionaryEntity>> Get(DictionaryListDataRequest dataRequest)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var dictionaries = ctx.Dictionaries.AsQueryable();
            if (dataRequest.Type != null)
            {
                dictionaries = dictionaries.Where(x => x.Type == dataRequest.Type && !x.IsDeleted);
            }
            else
            {
                dictionaries = dictionaries.Where(x => !x.IsDeleted);
            }

            if (dataRequest.SearchQuery != null)
            {
                dictionaries = dictionaries.Where(x => EF.Functions.ILike(x.Name, $"%{dataRequest.SearchQuery}%"));
            }

            switch (dataRequest.SortBy)
            {
                case DictionarySortColumn.Id:
                    dictionaries = dataRequest.SortDirection == SortDirection.Asc
                        ? dictionaries.OrderBy(x => x.Id)
                        : dictionaries.OrderByDescending(x => x.Id);
                    break;
                case DictionarySortColumn.DictionaryTypeName:
                    dictionaries = dataRequest.SortDirection == SortDirection.Asc
                        ? dictionaries.OrderBy(x => x.Type)
                        : dictionaries.OrderByDescending(x => x.Type);
                    break;
                case DictionarySortColumn.Name:
                    dictionaries = dataRequest.SortDirection == SortDirection.Asc
                        ? dictionaries.OrderBy(x => x.Name)
                        : dictionaries.OrderByDescending(x => x.Name);
                    break;
                case DictionarySortColumn.LastModifiedDate:
                    dictionaries = dataRequest.SortDirection == SortDirection.Asc
                        ? dictionaries.OrderBy(x => x.LastModified)
                        : dictionaries.OrderByDescending(x => x.LastModified);
                    break;
            }

            var data = dataRequest.Limit != -1 ? await dictionaries.Skip(dataRequest.Offset).Take(dataRequest.Limit).ToListAsync().ConfigureAwait(false) :
                                                 await dictionaries.Skip(dataRequest.Offset).ToListAsync().ConfigureAwait(false);

            return new ListDataResponse<DictionaryEntity>(data, await dictionaries.CountAsync().ConfigureAwait(false));
        }

        public async Task<ListDataResponse<DictionaryEntity>> GetFilteredList(DictionaryFilteredListDataRequest dataRequest)
        {
            ArgumentNullException.ThrowIfNull(dataRequest, nameof(dataRequest));

            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            IQueryable<DictionaryEntity?> dictionaries = null;

            if (dataRequest.Filter is not null)
            {
                dictionaries = ctx.Dictionaries.Apply(dataRequest.Filter);
            }
            else
            {
                dictionaries = ctx.Dictionaries;
            }

            if (dataRequest.Type != null)
            {
                dictionaries = dictionaries.Where(x => x.Type == dataRequest.Type && (dataRequest.ShowDeleted || !x.IsDeleted));
            }
            else
            {
                dictionaries = dictionaries.Where(x => dataRequest.ShowDeleted || !x.IsDeleted);
            }

            if (dataRequest.SearchQuery != null)
            {
                dictionaries = dictionaries.Where(x => EF.Functions.ILike(x.Name, $"%{dataRequest.SearchQuery}%"));
            }

            if (dataRequest.Name != null)
            {
                dictionaries = dictionaries.Where(x => EF.Functions.ILike(x.Name, $"%{dataRequest.Name}%"));
            }

            if (dataRequest.Created != null)
            {
                dictionaries = dictionaries.Where(x => x.Created >= dataRequest.Created.Start && x.Created <= dataRequest.Created.End);
            }

            if (dataRequest.Modified != null)
            {
                dictionaries = dictionaries.Where(x => x.LastModified >= dataRequest.Modified.Start && x.LastModified <= dataRequest.Modified.End);
            }

            if (dataRequest.Id != null)
            {
                dictionaries = dictionaries.Where(x => x.Id >= dataRequest.Id.Start && x.Id <= dataRequest.Id.End);
            }

            if (dataRequest.Sorter != null)
            {
                dictionaries = dictionaries.Apply(dataRequest.Sorter);
            }

            var data = dataRequest.Limit != -1 ? await dictionaries.Skip(dataRequest.Offset).Take(dataRequest.Limit).ToListAsync().ConfigureAwait(false) :
                                                 await dictionaries.Skip(dataRequest.Offset).ToListAsync().ConfigureAwait(false);

            return new ListDataResponse<DictionaryEntity>(data, await dictionaries.CountAsync().ConfigureAwait(false));
        }

        public async Task<IReadOnlyCollection<DictionaryEntity>> CreateRecords(IEnumerable<DictionaryEntity> dictionaries, bool forceUpdate)
        {
            var list = new List<DictionaryEntity>();

            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            foreach (var entity in dictionaries)
            {
                var existingEntity = await ctx.Dictionaries.SingleOrDefaultAsync(e => e.Name == entity.Name &&
                    e.Type == entity.Type && !e.IsDeleted);

                if (existingEntity == null)
                {
                    await ctx.Dictionaries.AddAsync(entity);
                    list.Add(entity);

                    continue;
                }

                list.Add(existingEntity);

                if (!forceUpdate) continue;
                existingEntity.LastModified = entity.LastModified;
                existingEntity.Data = entity.Data;
            }

            await ctx.SaveChangesAsync().ConfigureAwait(false);

            return list;
        }

        public async Task<DictionaryEntity> Create(DictionaryEntity dictionary)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            await ctx.Dictionaries.AddAsync(dictionary).ConfigureAwait(false);

            try
            {
                await ctx.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateException ex) when (ex.InnerException is Npgsql.PostgresException { SqlState: SqlStates.DuplicateKeyValueViolatesUniqueConstraint } npgsqlException)
            {
                CatalogAlreadyExistsException.Throw<DictionaryEntity>(dictionary.Name, ex);
            }

            return dictionary;
        }

        public async Task Update(int id, JObject data, string name)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var entity = await ctx.Dictionaries.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted).ConfigureAwait(false);
            if (entity == null)
            {
                throw new CatalogNotFoundException($"Dictionary with id {id} not found");
            }

            entity.Data = data.ToJsonDocument();
            entity.Name = name;
            entity.LastModified = DateTime.UtcNow;

            try
            {
                await ctx.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateException ex) when (ex.InnerException is Npgsql.PostgresException { SqlState: SqlStates.DuplicateKeyValueViolatesUniqueConstraint } npgsqlException)
            {
                CatalogAlreadyExistsException.Throw<DictionaryEntity>(name, ex);
            }
        }

        public async Task Delete(int id)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var entity = await ctx.Dictionaries.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted).ConfigureAwait(false);
            if (entity == null)
                throw new CatalogNotFoundException($"Dictionary with id {id} not found");

            entity.IsDeleted = true;
            await ctx.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteByType(string type)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var entity = await ctx.Dictionaries.Where(x => x.Type == type && !x.IsDeleted).ToListAsync().ConfigureAwait(false);
            foreach (var item in entity)
                item.IsDeleted = true;
            await ctx.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<DictionaryTypesSummaryResponse> GetDictionaryTypesSummary()
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var dictionaryTypes = await ctx.Dictionaries.Where(x => !x.IsDeleted).GroupBy(x => x.Type).Select(x => new { DictionaryTypeId = x.Key, Count = x.Count() })
                .ToDictionaryAsync(x => x.DictionaryTypeId, arg => new DictionaryTypeSummaryInfo() { Count = arg.Count }).ConfigureAwait(false);

            return new DictionaryTypesSummaryResponse() { DictionaryTypes = dictionaryTypes };
        }

        public async Task<bool> Any()
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            return await ctx.Dictionaries.AnyAsync().ConfigureAwait(false);
        }

        public async Task<ListDataResponse<DictionaryVersionEntity>> GetVersions(int id, DictionaryVersionListDataRequest dataRequest)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var dictVersions = ctx.DictionaryVersions.AsQueryable();

            dictVersions = dictVersions.Where(x => x.Id == id);

            if (dataRequest.SearchQuery != null)
            {
                dictVersions = dictVersions.Where(x => EF.Functions.ILike(x.Name, $"%{dataRequest.SearchQuery}%"));
            }
            dictVersions = dictVersions.OrderByDescending(x => x.VersionDate);

            var data = await dictVersions.Skip(dataRequest.Offset)
                 .Take(dataRequest.Limit).ToListAsync().ConfigureAwait(false);

            return new ListDataResponse<DictionaryVersionEntity>(data, await dictVersions.CountAsync().ConfigureAwait(false));

        }

        public async Task<DictionaryVersionEntity> GetVersion(int id, int version)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var result = await ctx.DictionaryVersions.FirstOrDefaultAsync(x => x.Id == id && x.Version == version).ConfigureAwait(false);
            if (result == null)
            {
                throw new CatalogNotFoundException($"Dictionary with id {id} and version {version} not found");
            }

            return result;
        }

        public void SetCurrentUser(int userId)
        {
            _contextFactory.UserId = userId;
        }
    }
}
