﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Redb.Catalog.Common.Data.Responses;
using Redb.Catalog.Common.Exceptions;
using Redb.Catalog.Common.Extensions;
using Redb.Catalog.Dictionary.Data;
using Redb.Catalog.Dictionary.Data.Requests;
using Redb.Catalog.Dictionary.Entities;

namespace Redb.Catalog.Dictionary.Repositories
{
    public class DictionaryAttachmentRepository
    {
        private readonly ICatalogDictionaryContextFactory _contextFactory;

        public DictionaryAttachmentRepository(ICatalogDictionaryContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<IReadOnlyCollection<DictionaryAttachmentEntity>> GetAttachments(int dictionaryItemId, string type, bool excludeContent, CancellationToken cancellationToken)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var query = ctx.DictionaryAttachments
                           .Where(x => x.DictionaryEntityId == dictionaryItemId
                                       && x.Type == type
                                       && !x.IsDeleted);

            if (!excludeContent)
            {
                query = query.Include(x => x.Content);
            }

            var dictionaries = await query.OrderByDescending(x => x.Id)
                                          .ToListAsync(cancellationToken)
                                          .ConfigureAwait(false);

            return dictionaries;
        }

        public async Task<ListDataResponse<DictionaryAttachmentEntity>> Get(DictionaryAttachmentListDataRequest dataRequest, CancellationToken cancellationToken)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var query = ctx.DictionaryAttachments
                           .Where(x => x.DictionaryEntityId == dataRequest.DictionaryEntityId
                                       && x.Type == dataRequest.Type
                                       && !x.IsDeleted);

            if (!dataRequest.ExcludeContent)
            {
                query = query.Include(x => x.Content);
            }

            var dictionaries = query.OrderByDescending(x => x.Id);

            var data = dataRequest.Limit != -1
                            ? await dictionaries.Skip(dataRequest.Offset).Take(dataRequest.Limit).ToListAsync(cancellationToken).ConfigureAwait(false)
                            : await dictionaries.Skip(dataRequest.Offset).ToListAsync(cancellationToken).ConfigureAwait(false);

            return new ListDataResponse<DictionaryAttachmentEntity>(data, await dictionaries.CountAsync(cancellationToken: cancellationToken).ConfigureAwait(false));
        }

        public async Task<DictionaryAttachmentEntity> Create(DictionaryAttachmentEntity dictionaryAttachment, CancellationToken cancellationToken)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            await ctx.DictionaryAttachments
                     .AddAsync(dictionaryAttachment, cancellationToken)
                     .ConfigureAwait(false);

            await ctx.SaveChangesAsync()
                     .ConfigureAwait(false);

            return dictionaryAttachment;
        }

        public async Task<DictionaryAttachmentEntity> Update(
            int id,
            JObject data,
            byte[] content,
            string contentType,
            string? fileName,
            CancellationToken cancellationToken)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var entity = await ctx.DictionaryAttachments
                                  .Include(x => x.Content)
                                  .Where(x => x.Id == id && !x.IsDeleted)
                                  .FirstOrDefaultAsync(cancellationToken)
                                  .ConfigureAwait(false)
                         ?? throw new CatalogNotFoundException($"Dictionary attachment with id {id} not found");

            entity.Data = data.ToJsonDocument();
            entity.Content.Data = content;
            entity.ContentType = contentType;
            entity.FileName = fileName;
            entity.LastModified = DateTime.UtcNow;

            await ctx.SaveChangesAsync()
                     .ConfigureAwait(false);

            return entity;
        }

        public async Task Delete(int id, CancellationToken cancellationToken)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var entity = await ctx.DictionaryAttachments
                                  .Where(x => x.Id == id && !x.IsDeleted)
                                  .FirstOrDefaultAsync(cancellationToken)
                                  .ConfigureAwait(false)
                         ?? throw new CatalogNotFoundException($"Dictionary attachment with id {id} not found");

            entity.IsDeleted = true;

            await ctx.SaveChangesAsync()
                     .ConfigureAwait(false);
        }

        public async Task<DictionaryAttachmentEntity> Get(int id, bool excludeContent, CancellationToken cancellationToken)
        {
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            var query = ctx.DictionaryAttachments
                           .Where(x => x.Id == id && !x.IsDeleted);

            if (!excludeContent)
            {
                query = query.Include(x => x.Content);
            }

            var result = await query.FirstOrDefaultAsync(cancellationToken)
                                    .ConfigureAwait(false);

            return result ?? throw new CatalogNotFoundException($"Dictionary attachment with id {id} not found");
        }

        public async Task WriteSingleAttachmentsAsync(IEnumerable<DictionaryAttachmentEntity> entities, bool forceUpdate)
        {
            ArgumentNullException.ThrowIfNull(entities);

            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            foreach (var entity in entities)
            {
                var existingEntity = await ctx.DictionaryAttachments.SingleOrDefaultAsync(e => e.DictionaryEntityId == entity.DictionaryEntityId &&
                                                                                               e.Type == entity.Type &&
                                                                                               !e.IsDeleted);
                if (existingEntity == null)
                {
                    await ctx.DictionaryAttachments.AddAsync(entity);
                }
                else
                {
                    if (!forceUpdate)
                    {
                        continue;
                    }

                    existingEntity.LastModified = entity.LastModified;
                    existingEntity.Data = entity.Data;
                    existingEntity.Content.Data = entity.Content.Data;
                    existingEntity.ContentType = entity.ContentType;
                    existingEntity.FileName = entity.FileName;
                }
            }

            await ctx.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task WriteMultipleAttachmentsAsync(IEnumerable<DictionaryAttachmentEntity> entities, bool forceUpdate)
        {
            ArgumentNullException.ThrowIfNull(entities);
            await using var ctx = _contextFactory.CreateCatalogDictionaryContext();

            foreach (var entity in entities)
            {
                var existingEntity = await ctx.DictionaryAttachments
                    .FirstOrDefaultAsync(e => e.DictionaryEntityId == entity.DictionaryEntityId &&
                                              e.Type == entity.Type && e.FileName == entity.FileName &&
                                              !e.IsDeleted);
                if (existingEntity == null)
                {
                    await ctx.DictionaryAttachments.AddAsync(entity);
                }
                else
                {
                    if (!forceUpdate)
                    {
                        continue;
                    }

                    existingEntity.LastModified = entity.LastModified;
                    existingEntity.Data = entity.Data;
                    existingEntity.Content.Data = entity.Content.Data;
                    existingEntity.ContentType = entity.ContentType;
                    existingEntity.FileName = entity.FileName;
                }
            }

            await ctx.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}

