﻿using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Redb.Catalog.Common.Data.EntityVersioning
{
    public interface IChangeTrackeredContext
    {
        ChangeTracker ChangeTracker { get; }
    }
}
