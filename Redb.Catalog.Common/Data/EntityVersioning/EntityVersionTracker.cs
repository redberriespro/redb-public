﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Redb.Catalog.Common.Data.EntityVersioning
{
    public abstract class EntityVersionTracker<TDBContext, TEntity>
        where TDBContext : IChangeTrackeredContext
        where TEntity : class
    {
        protected readonly TDBContext _dbContext;

        protected EntityVersionTracker(TDBContext dbContext)
        {
            ArgumentNullException.ThrowIfNull(dbContext);
            _dbContext = dbContext;
        }

        protected List<EntityEntry<TEntity>> _addedEntries = null!;
        protected List<EntityEntry<TEntity>> _modifiedEntries = null!;

        public void CollectChanges()
        {
            _dbContext.ChangeTracker.DetectChanges();

            _addedEntries = _dbContext.ChangeTracker
                                      .Entries<TEntity>()
                                      .Where(e => e.State == EntityState.Added)
                                      .ToList();
            _modifiedEntries = _dbContext.ChangeTracker
                                         .Entries<TEntity>()
                                         .Where(e => e.State == EntityState.Modified)
                                         .ToList();
        }

        public abstract void GenerateVersions(int changeAuthorId, string changeAuthorName);
    }
}
