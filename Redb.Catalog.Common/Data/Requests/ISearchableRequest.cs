﻿namespace Redb.Catalog.Common.Data.Requests;

public interface ISearchableRequest
{
    string? SearchQuery { get; set; }
}