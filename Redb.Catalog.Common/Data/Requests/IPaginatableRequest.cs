﻿namespace Redb.Catalog.Common.Data.Requests;

public interface IPaginatableRequest
{
    int Offset { get; set; }
    int Limit { get; set; }
}