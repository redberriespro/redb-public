﻿using Redb.Catalog.Common.Enums;

namespace Redb.Catalog.Common.Data.Requests;

public interface ISortableRequest<T>
{
    SortDirection SortDirection { get; set; }
    T SortBy { get; set; }
}