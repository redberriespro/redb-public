﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters
{
    public class FilterEquals<TEntity, TValue> : Filter
        where TEntity : class
    {
        public FilterEquals(
            string propertyName,
            TValue propertyValue)
        {
            ArgumentNullException.ThrowIfNull(propertyName, nameof(propertyName));
            ArgumentNullException.ThrowIfNull(propertyValue, nameof(propertyValue));

            var propertyValueParameter = GetUniqueParamenterName();

            Parameters = ImmutableArray.Create(new NpgsqlParameter(propertyValueParameter, propertyValue));

            Sql = $"{propertyName} = @{propertyValueParameter}";
        }

        public override string Sql { get; }

        public override ImmutableArray<NpgsqlParameter> Parameters { get; }
    }
}
