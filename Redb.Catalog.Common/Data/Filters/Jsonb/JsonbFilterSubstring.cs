﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters.Jsonb
{
    public sealed class JsonbFilterSubstring<TEntity> : FilterJsonb
        where TEntity : class
    {
        public JsonbFilterSubstring(
            string propertyName,
            string propertyValue)
        {
            ArgumentNullException.ThrowIfNull(propertyName, nameof(propertyName));
            ArgumentNullException.ThrowIfNull(propertyValue, nameof(propertyValue));

            var propertyNameParameter = GetUniqueParamenterName();
            var propertyValueParameter = GetUniqueParamenterName();

            Parameters = ImmutableArray.Create(
                new NpgsqlParameter(propertyNameParameter, propertyName),
                new NpgsqlParameter(propertyValueParameter, $"%{propertyValue}%"));

            Sql = $"\"{GetJsonbProperty<TEntity>()}\" ->> @{propertyNameParameter} ilike @{propertyValueParameter}";
        }

        public override string Sql { get; }

        public override ImmutableArray<NpgsqlParameter> Parameters { get; }
    }
}
