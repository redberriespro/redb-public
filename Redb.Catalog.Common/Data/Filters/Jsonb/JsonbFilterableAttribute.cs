﻿namespace Redb.Catalog.Common.Data.Filters.Jsonb
{
    [AttributeUsage(AttributeTargets.Property)]
    public class JsonbFilterableAttribute : Attribute
    {
    }
}
