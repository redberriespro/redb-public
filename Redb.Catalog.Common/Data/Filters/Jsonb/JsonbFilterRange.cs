﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters.Jsonb
{
    public class JsonbFilterRange<TEntity, TValue> : FilterJsonb
        where TEntity : class
        where TValue : IComparable
    {
        public JsonbFilterRange(
            string propertyName,
            TValue propertyStartValue,
            TValue propertyEndValue)
            : this(
                propertyName,
                propertyStartValue,
                propertyEndValue,
                GetDefaultTypeConvertion)
        {
        }

        public JsonbFilterRange(
            string propertyName,
            TValue propertyStartValue,
            TValue propertyEndValue,
            Func<Type, string> typeConvertion)
        {
            ArgumentNullException.ThrowIfNull(propertyName, nameof(propertyName));
            ArgumentNullException.ThrowIfNull(propertyStartValue, nameof(propertyStartValue));
            ArgumentNullException.ThrowIfNull(propertyEndValue, nameof(propertyEndValue));
            ArgumentNullException.ThrowIfNull(typeConvertion, nameof(typeConvertion));

            var propertyNameParameter = GetUniqueParamenterName();
            var propertyStartValueParameter = GetUniqueParamenterName();
            var propertyEndValueParameter = GetUniqueParamenterName();

            Parameters = ImmutableArray.Create(
                new NpgsqlParameter(propertyNameParameter, propertyName),
                new NpgsqlParameter(propertyStartValueParameter, propertyStartValue),
                new NpgsqlParameter(propertyEndValueParameter, propertyEndValue));

            var tc = typeConvertion(typeof(TValue));

            Sql = $"(\"{GetJsonbProperty<TEntity>()}\" ->> @{propertyNameParameter}){tc} between @{propertyStartValueParameter} and @{propertyEndValueParameter}";
        }

        public override string Sql { get; }

        public override ImmutableArray<NpgsqlParameter> Parameters { get; }
    }
}
