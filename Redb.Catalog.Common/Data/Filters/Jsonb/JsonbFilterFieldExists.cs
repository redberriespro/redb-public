﻿namespace Redb.Catalog.Common.Data.Filters.Jsonb;

using Npgsql;
using System.Collections.Immutable;

public class JsonbFilterFieldExists<TEntity> : FilterJsonb
    where TEntity : class
{
    public JsonbFilterFieldExists(
        string propertyName,
        bool exists)
    {
        ArgumentNullException.ThrowIfNull(propertyName, nameof(propertyName));

        var propertyNameParameter = GetUniqueParamenterName();
        Parameters = ImmutableArray.Create(new NpgsqlParameter(propertyNameParameter, propertyName));

        Sql = exists
            ? $"(\"{GetJsonbProperty<TEntity>()}\" -> @{propertyNameParameter}) IS NOT NULL"
            : $"(\"{GetJsonbProperty<TEntity>()}\" -> @{propertyNameParameter}) IS NULL";
    }
    public override string Sql { get; }
    public override ImmutableArray<NpgsqlParameter> Parameters { get; }
}