﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters.Jsonb
{
    public class JsonbFilterEquals<TEntity, TValue> : FilterJsonb
        where TEntity : class
    {
        public JsonbFilterEquals(
           string propertyName,
           TValue propertyValue)
            : this(
                 propertyName,
                 propertyValue,
                 GetDefaultTypeConvertion)
        {
        }

        public JsonbFilterEquals(
            string propertyName,
            TValue propertyValue,
            Func<Type, string> typeConvertion)
        {
            ArgumentNullException.ThrowIfNull(propertyName, nameof(propertyName));
            ArgumentNullException.ThrowIfNull(propertyValue, nameof(propertyValue));
            ArgumentNullException.ThrowIfNull(typeConvertion, nameof(typeConvertion));

            var propertyNameParameter = GetUniqueParamenterName();
            var propertyValueParameter = GetUniqueParamenterName();

            Parameters = ImmutableArray.Create(
                new NpgsqlParameter(propertyNameParameter, propertyName),
                new NpgsqlParameter(propertyValueParameter, propertyValue));

            var tc = typeConvertion(typeof(TValue));

            Sql = $"(\"{GetJsonbProperty<TEntity>()}\" ->> @{propertyNameParameter}){tc} = @{propertyValueParameter}";
        }

        public override string Sql { get; }

        public override ImmutableArray<NpgsqlParameter> Parameters { get; }
    }
}
