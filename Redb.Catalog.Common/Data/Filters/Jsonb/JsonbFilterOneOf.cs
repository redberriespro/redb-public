﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters.Jsonb
{
    public class JsonbFilterOneOf<TEntity, TValue> : FilterJsonb
        where TEntity : class
    {
        public JsonbFilterOneOf(
            string propertyName,
            params TValue[] propertyValues)
            : this(
                 propertyName,
                 GetDefaultTypeConvertion,
                 propertyValues)
        { }

        public JsonbFilterOneOf(
            string propertyName,
            Func<Type, string> typeConvertion,
            params TValue[] propertyValues)
        {
            ArgumentNullException.ThrowIfNull(propertyName, nameof(propertyName));
            ArgumentNullException.ThrowIfNull(propertyValues, nameof(propertyValues));
            ArgumentNullException.ThrowIfNull(typeConvertion, nameof(typeConvertion));

            var propertyNameParameter = GetUniqueParamenterName();

            var values = propertyValues.Select(v => new NpgsqlParameter(GetUniqueParamenterName(), v)).ToArray();

            var parameters = values.Concat(new[] { new NpgsqlParameter(propertyNameParameter, propertyName) });
            var valuesNames = string.Join(",", values.Select(p => $"@{p.ParameterName}"));

            Parameters = ImmutableArray.CreateRange(parameters);

            var tc = typeConvertion(typeof(TValue));

            Sql = $"(\"{GetJsonbProperty<TEntity>()}\" ->> @{propertyNameParameter}){tc} in ({valuesNames})";
        }

        public override string Sql { get; }

        public override ImmutableArray<NpgsqlParameter> Parameters { get; }
    }
}
