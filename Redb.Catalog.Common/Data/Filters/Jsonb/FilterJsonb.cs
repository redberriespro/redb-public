﻿using Npgsql;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations.Schema;

namespace Redb.Catalog.Common.Data.Filters.Jsonb
{
    public abstract class FilterJsonb : Filter, IFilter
    {
        private static readonly Dictionary<Type, string> DefaultTypeConvertions = new() {
            {typeof(string), ""},
            {typeof(int), "::numeric"},
            {typeof(float), "::numeric"}
        };

        public override string Sql { get; } = null!;

        public override ImmutableArray<NpgsqlParameter> Parameters { get; }

        protected static string GetDefaultTypeConvertion(Type type)
        {
            if (DefaultTypeConvertions.TryGetValue(type, out var convertion))
            {
                return convertion;
            }

            throw new InvalidOperationException($"Undefined type convertion for type '{type}'. Define custom convertion.");
        }

        protected static string GetJsonbProperty<TEntity>()
        {
            var entityType = typeof(TEntity);
            var jsonbFilterableAttributeType = typeof(JsonbFilterableAttribute);
            var columnAttributeType = typeof(ColumnAttribute);
            var properties = entityType.GetProperties();

            foreach (var property in properties)
            {
                if (property.CustomAttributes.Any(a => a.AttributeType == jsonbFilterableAttributeType))
                {
                    var columnAttribute = (ColumnAttribute?)Attribute.GetCustomAttribute(property, columnAttributeType);

                    if (columnAttribute is null)
                    {
                        throw new Exception($"Property '{property}' does not contain an attribute '{columnAttributeType}'.");
                    }

                    if (columnAttribute.TypeName != "jsonb")
                    {
                        throw new Exception($"Property '{property}' does not contain an attribute '{columnAttributeType}' with type name 'jsonb'.");
                    }

                    return columnAttribute.Name;
                }
            }

            throw new Exception($"Type '{entityType}' does not contain a property with attribute '{jsonbFilterableAttributeType}'.");
        }
    }
}
