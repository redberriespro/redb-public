﻿namespace Redb.Catalog.Common.Data.Filters
{
    public interface IFilterModifier
    {
        IFilter Modify(IFilter filter);
    }
}
