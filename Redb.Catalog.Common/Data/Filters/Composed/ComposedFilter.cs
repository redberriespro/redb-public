﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters.Composed
{
    public abstract class ComposedFilter : IFilter
    {
        protected ComposedFilter(params IFilter[] filters)
        {
            ArgumentNullException.ThrowIfNull(filters, nameof(filters));

            (Sql, Parameters) = Compose(filters);
        }

        public string Sql { get; }

        public ImmutableArray<NpgsqlParameter> Parameters { get; }

        protected abstract (string, ImmutableArray<NpgsqlParameter>) Compose(params IFilter[] filters);
    }
}
