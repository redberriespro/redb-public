﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters.Composed
{
    public class ComposedFilterOr : ComposedFilter
    {
        protected override (string, ImmutableArray<NpgsqlParameter>) Compose(params IFilter[] filters)
        {
            var parameters = ImmutableArray.Create(filters.SelectMany(f => f.Parameters).ToArray());
            var sql = string.Join(" or ", filters.Select(f => f.Sql));
            sql = $"({sql})";

            return (sql, parameters);
        }

        public ComposedFilterOr(params IFilter[] filters)
            : base(filters) { }
    }
}
