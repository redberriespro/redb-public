﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters
{
    public abstract class Filter : IFilter
    {
        public abstract string Sql { get; }

        public abstract ImmutableArray<NpgsqlParameter> Parameters { get; }

        protected static string GetUniqueParamenterName()
        {
            return $"p{Guid.NewGuid():N}";
        }
    }
}
