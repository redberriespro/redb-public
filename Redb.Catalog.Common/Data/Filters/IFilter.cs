﻿using Npgsql;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Data.Filters
{
    public interface IFilter
    {
        string Sql { get; }

        ImmutableArray<NpgsqlParameter> Parameters { get; }
    }
}
