﻿namespace Redb.Catalog.Common.Data.Responses;

public class ListDataResponse<TData>
{
    public IReadOnlyList<TData> Data { get; }
    public int Total { get; }

    public ListDataResponse(IReadOnlyList<TData> data, int total)
    {
        Data = data;
        Total = total;
    }
}