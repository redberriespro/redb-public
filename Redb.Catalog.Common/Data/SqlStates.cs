﻿using Npgsql;

namespace Redb.Catalog.Common.Data
{
    public static class SqlStates
    {
        public const string DuplicateKeyValueViolatesUniqueConstraint = PostgresErrorCodes.UniqueViolation;
    }
}
