﻿namespace Redb.Catalog.Common.Models.DataFilters;

using Data.Filters;
using Data.Filters.Jsonb;
using Enums;

public class DataFilterFieldExist<TEntity> : DataFilter
    where TEntity : class
{
    public DataFilterFieldExist(
        string property,
        bool value,
        IFilterModifier? filterModifier = null)
        : base(property, filterModifier)
    {
        Value = value;
    }

    public bool Value { get; }

    public override DataFilterType Type => DataFilterType.Exist;

    protected override IFilter ToPropertyFilter() => new JsonbFilterFieldExists<TEntity>(Property, Value);
}