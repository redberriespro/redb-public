﻿using Redb.Catalog.Common.Data.Filters;
using Redb.Catalog.Common.Data.Filters.Jsonb;
using Redb.Catalog.Common.Enums;

namespace Redb.Catalog.Common.Models.DataFilters
{
    public class DataFilterSearch<TEntity> : DataFilter
         where TEntity : class
    {
        public DataFilterSearch(
            string property,
            string value,
            IFilterModifier? filterModifier = null)
            : base(property, filterModifier)
        {
            Value = value;
        }

        public string Value { get; }

        public override DataFilterType Type => DataFilterType.Search;

        protected override IFilter ToPropertyFilter() => new JsonbFilterSubstring<TEntity>(Property, Value);
    }
}
