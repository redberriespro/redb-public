﻿using Redb.Catalog.Common.Data.Filters;
using Redb.Catalog.Common.Data.Filters.Jsonb;
using Redb.Catalog.Common.Enums;
using System.Collections.Immutable;

namespace Redb.Catalog.Common.Models.DataFilters
{
    public class DataFilterOneOf<TEntity, T> : DataFilter
         where TEntity : class
    {
        public DataFilterOneOf(
            string property,
            IEnumerable<T> values,
            IFilterModifier? filterModifier = null)
            : base(property, filterModifier)
        {
            Values = values.ToImmutableArray();
        }

        public ImmutableArray<T> Values { get; }

        public override DataFilterType Type => DataFilterType.OneOf;

        protected override IFilter ToPropertyFilter() => new JsonbFilterOneOf<TEntity, T>(Property, Values.ToArray());
    }
}
