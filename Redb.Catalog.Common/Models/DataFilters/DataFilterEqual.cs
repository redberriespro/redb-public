﻿namespace Redb.Catalog.Common.Models.DataFilters;

using Data.Filters;
using Data.Filters.Jsonb;
using Enums;

public class DataFilterEqual<TEntity, TValue> : DataFilter
    where TEntity : class
{
    public DataFilterEqual(
        string property,
        TValue value,
        IFilterModifier? filterModifier = null)
        : base(property, filterModifier)
    {
        Value = value;
    }

    public TValue Value { get; }

    public override DataFilterType Type => DataFilterType.Equal;

    protected override IFilter ToPropertyFilter() => new JsonbFilterEquals<TEntity, TValue>(Property, Value);
}