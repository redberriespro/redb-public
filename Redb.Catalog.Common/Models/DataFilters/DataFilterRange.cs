﻿using Redb.Catalog.Common.Data.Filters;
using Redb.Catalog.Common.Data.Filters.Jsonb;
using Redb.Catalog.Common.Enums;

namespace Redb.Catalog.Common.Models.DataFilters
{
    public class DataFilterRange<TEntity, T> : DataFilter
        where TEntity : class
        where T : IComparable
    {
        public DataFilterRange(
            string property,
            T start,
            T end,
            IFilterModifier? filterModifier = null)
            : base(property, filterModifier)
        {
            Start = start;
            End = end;
        }

        public T Start { get; }

        public T End { get; }
        public override DataFilterType Type => DataFilterType.Range;

        protected override IFilter ToPropertyFilter() => new JsonbFilterRange<TEntity, T>(Property, Start, End);
    }
}
