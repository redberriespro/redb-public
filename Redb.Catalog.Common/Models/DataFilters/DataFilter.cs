﻿using Newtonsoft.Json.Linq;
using Redb.Catalog.Common.Data.Filters;
using Redb.Catalog.Common.Enums;
using System.Globalization;

namespace Redb.Catalog.Common.Models.DataFilters
{
    public abstract class DataFilter
    {
        public abstract DataFilterType Type { get; }

        public string Property { get; }

        protected IFilterModifier? FilterModifier { get; }

        protected DataFilter(string property, IFilterModifier? filterModifier = null)
        {
            ArgumentNullException.ThrowIfNull(property, nameof(property));

            Property = property;
            FilterModifier = filterModifier;
        }

        public IFilter ToFilter()
        {
            var propertyFilter = ToPropertyFilter();

            return FilterModifier?.Modify(propertyFilter) ?? propertyFilter;
        }

        protected abstract IFilter ToPropertyFilter();

        public static DataFilter CreateSearch<TEntity>(string property, string value, IFilterModifier? filterModifier = null)
             where TEntity : class
        {
            return new DataFilterSearch<TEntity>(property, value, filterModifier);
        }

        public static DataFilter CreateExist<TEntity>(string property, bool exist, IFilterModifier? filterModifier = null)
            where TEntity : class =>
            new DataFilterFieldExist<TEntity>(property, exist, filterModifier);

        public static DataFilter CreateEqual<TEntity>(string property, string propertyType, string value, IFilterModifier? filterModifier = null)
            where TEntity : class =>
            propertyType switch
            {
                "string"  => new DataFilterEqual<TEntity, string>(property, value, filterModifier),
                "integer" => new DataFilterEqual<TEntity, int>(property, int.Parse(value, CultureInfo.InvariantCulture),
                    filterModifier),
                "number"  => new DataFilterEqual<TEntity, float>(property,
                    float.Parse(value, CultureInfo.InvariantCulture), filterModifier),
                _         => throw new InvalidOperationException($"Unknown property type '{propertyType}'")
            };

        public static DataFilter CreateOneOf<TEntity>(string property, string propertyType, IEnumerable<string> values, IFilterModifier? filterModifier = null)
             where TEntity : class
        {
            switch (propertyType)
            {
                case "string":
                    {
                        return new DataFilterOneOf<TEntity, string>(property, values, filterModifier);
                    }
                case "integer":
                    {
                        return new DataFilterOneOf<TEntity, int>(property, values.Select(s => int.Parse(s, CultureInfo.InvariantCulture)), filterModifier);
                    }
                case "number":
                    {
                        return new DataFilterOneOf<TEntity, float>(property, values.Select(s => float.Parse(s, CultureInfo.InvariantCulture)), filterModifier);
                    }
                default:
                    {
                        throw new InvalidOperationException($"Unknown property type '{propertyType}'");
                    }
            }
        }

        public static DataFilter CreateRange<TEntity>(string property, string propertyType, string start, string end, IFilterModifier? filterModifier = null)
            where TEntity : class
        {
            switch (propertyType)
            {
                case "string":
                    {
                        return new DataFilterRange<TEntity, string>(property, start, end, filterModifier);
                    }
                case "integer":
                    {
                        return new DataFilterRange<TEntity, int>(property, int.Parse(start, CultureInfo.InvariantCulture), int.Parse(end, CultureInfo.InvariantCulture), filterModifier);
                    }
                case "number":
                    {
                        return new DataFilterRange<TEntity, float>(property, float.Parse(start, CultureInfo.InvariantCulture), float.Parse(end, CultureInfo.InvariantCulture), filterModifier);
                    }
                default:
                    {
                        throw new InvalidOperationException($"Unknown property type '{propertyType}'");
                    }
            }
        }
    }
}
