﻿namespace Redb.Catalog.Common.Models;

public class ListResult<TResult>
{
    public IReadOnlyList<TResult> Result { get; }
    public int Total { get; }

    public ListResult(IReadOnlyList<TResult> result, int total)
    {
        Result = result;
        Total = total;
    }
}

