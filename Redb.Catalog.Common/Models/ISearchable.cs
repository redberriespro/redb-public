﻿namespace Redb.Catalog.Common.Models;

public interface ISearchable
{
    string? SearchQuery { get; set; }
}