﻿namespace Redb.Catalog.Common.Models;

public interface IPaginatable
{
    int Offset { get; set; }
    int Limit { get; set; }
}