﻿using Redb.Catalog.Common.Enums;

namespace Redb.Catalog.Common.Models;

public interface ISortable<T>
{
    SortDirection? SortDirection { get; set; }
    T SortBy { get; set; }
}