﻿namespace Redb.Catalog.Common.Extensions
{
    public static class ExceptionExtensions
    {
        public static TException Data<TException>(this TException ex, object key, object? value)
            where TException : Exception
        {
            ArgumentNullException.ThrowIfNull(key);

            ex.Data.Add(key, value);
            return ex;
        }
    }
}
