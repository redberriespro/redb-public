﻿using Newtonsoft.Json.Linq;
using System.Text.Json;

namespace Redb.Catalog.Common.Extensions;

public static class JObjectExtentions
{
    public static JsonDocument ToJsonDocument(this JObject jObject) => JsonDocument.Parse(jObject.ToString());
}
