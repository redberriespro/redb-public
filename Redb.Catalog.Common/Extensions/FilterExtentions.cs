﻿using Microsoft.EntityFrameworkCore;
using Npgsql;
using Redb.Catalog.Common.Data.Filters;
using Redb.Catalog.Common.Data.Filters.Composed;
using System.ComponentModel.DataAnnotations.Schema;

namespace Redb.Catalog.Common.Extensions
{
    public static class FilterExtentions
    {
        public static IQueryable<TEntity> Apply<TEntity>(this DbSet<TEntity> source, IFilter filter)
             where TEntity : class
        {
            ArgumentNullException.ThrowIfNull(source, nameof(source));
            ArgumentNullException.ThrowIfNull(filter, nameof(filter));

            var table = GetTableName<TEntity>();

            if (table == null)
            {
                throw new InvalidOperationException($"Entity '{typeof(TEntity)}' does not have attribute 'Table'");
            }

            var (sqlWithSimplifiedParameters, simplifiedParameters) = SimplifyParametersName(filter.Sql, filter.Parameters);

            var sql = $"SELECT * FROM {table} WHERE {sqlWithSimplifiedParameters}";
            var parameters = simplifiedParameters.Select(p => (object)p).ToArray();

            return source.FromSqlRaw(sql, parameters);
        }

        public static IFilter ComposeAnd(this IEnumerable<IFilter> filters)
        {
            return new ComposedFilterAnd(filters.ToArray());
        }

        public static IFilter ComposeOr(this IEnumerable<IFilter> filters)
        {
            return new ComposedFilterOr(filters.ToArray());
        }

        private static string? GetTableName<TEntity>() where TEntity : class
        {
            return (Attribute.GetCustomAttribute(typeof(TEntity), typeof(TableAttribute)) as TableAttribute)?.Name;
        }

        private static (string, List<NpgsqlParameter>) SimplifyParametersName(string sql, IEnumerable<NpgsqlParameter> parameters)
        {
            var simplifiedParameters = new List<NpgsqlParameter>();
            var sqlWithSimplifiedParameters = sql;

            var ind = 0;
            foreach (var parameter in parameters)
            {
                var p = parameter.Clone();
                p.ParameterName = $"p{ind++}";

                simplifiedParameters.Add(p);
                sqlWithSimplifiedParameters = sqlWithSimplifiedParameters.Replace(parameter.ParameterName, p.ParameterName);
            }

            return (sqlWithSimplifiedParameters, simplifiedParameters);
        }
    }
}
