﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.Json;

namespace Redb.Catalog.Common.Extensions;

public static class JsonDocumentExtentions
{
    public static string ToJsonString(this JsonDocument document) => document.RootElement.GetRawText();

    public static JObject ToJObject(this JsonDocument document) => JsonConvert.DeserializeObject<JObject>(document.ToJsonString());
}

