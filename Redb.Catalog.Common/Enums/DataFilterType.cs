﻿namespace Redb.Catalog.Common.Enums
{
    public enum DataFilterType
    {
        Search,
        Equal,
        Exist,
        Range,
        OneOf
    }
}
