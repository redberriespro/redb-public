﻿namespace Redb.Catalog.Common.Enums
{
    public enum SortType
    {
        Property = 0,
        JsonProperty = 1
    }
}
