﻿using Redb.Catalog.Common.Enums;

namespace Redb.Catalog.Common.BasicTypes;

public class Sorter<T>
{
    public SortType SortType { get; }

    public SortDirection SortDirection { get; }

    public T Property { get; }

    public string? JsonProperty { get; }

    public Sorter() { }

    protected Sorter(string jsonProperty, SortDirection sortDirection)
    {
        ArgumentNullException.ThrowIfNull(jsonProperty);

        SortType = SortType.JsonProperty;
        JsonProperty = jsonProperty;
        SortDirection = sortDirection;
    }

    protected Sorter(T property, SortDirection sortDirection)
    {
        ArgumentNullException.ThrowIfNull(property);

        SortType = SortType.Property;
        Property = property;
        SortDirection = sortDirection;
    }

    public static Sorter<T> CreateSorterByProperty(T property, SortDirection sortDirection) => new(property, sortDirection);

    public static Sorter<T> CreateSorterByJsonProperty(string jsonProperty, SortDirection sortDirection) => new(jsonProperty, sortDirection);
}