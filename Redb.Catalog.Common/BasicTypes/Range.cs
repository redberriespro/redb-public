﻿namespace Redb.Catalog.Common.BasicTypes
{
    public class Range<T>
    {
        public T Start { get; set; }

        public T End { get; set; }
    }
}
