﻿namespace Redb.Catalog.Common.BasicTypes
{
    public class CatalogFile
    {
        public byte[] Content { get; }
        public string ContentType { get; }
        public string FileName { get; }

        public CatalogFile(string fileName, byte[] content, string contentType)
        {
            Content = content;
            ContentType = contentType;
            FileName = fileName;
        }
    }
}
