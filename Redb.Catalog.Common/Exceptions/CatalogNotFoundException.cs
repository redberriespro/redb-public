﻿using System.Runtime.Serialization;

namespace Redb.Catalog.Common.Exceptions
{
    public class CatalogNotFoundException : CatalogException
    {
        public CatalogNotFoundException()
        {
        }

        protected CatalogNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public CatalogNotFoundException(string message) : base(message)
        {
        }

        public CatalogNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
