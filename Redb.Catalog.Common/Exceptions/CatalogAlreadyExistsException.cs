﻿using System.Runtime.Serialization;

namespace Redb.Catalog.Common.Exceptions
{
    public class CatalogAlreadyExistsException : CatalogException
    {
        public CatalogAlreadyExistsException()
        {
        }

        protected CatalogAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public CatalogAlreadyExistsException(string message) : base(message)
        {
        }

        public CatalogAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public static void Throw<T>(string identifier) { 
            var ex = new CatalogAlreadyExistsException($"{typeof(T).Name} with identifier '{identifier}' is already exists.");

            throw ex;
        }

        public static void Throw<T>(string identifier, Exception innerException)
        {
            var ex = new CatalogAlreadyExistsException($"{typeof(T).Name} with identifier '{identifier}' is already exists.", innerException);

            throw ex;
        }
    }
}
