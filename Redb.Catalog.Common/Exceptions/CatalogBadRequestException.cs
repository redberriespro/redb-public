﻿using System.Runtime.Serialization;

namespace Redb.Catalog.Common.Exceptions
{
    public class CatalogBadRequestException : CatalogException
    {
        public CatalogBadRequestException()
        {
        }

        protected CatalogBadRequestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public CatalogBadRequestException(string message) : base(message)
        {
        }

        public CatalogBadRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
