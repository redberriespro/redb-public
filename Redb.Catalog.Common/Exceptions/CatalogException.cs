using System.Runtime.Serialization;

namespace Redb.Catalog.Common.Exceptions;

public class CatalogException: Exception
{
    public CatalogException()
    {
    }

    protected CatalogException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public CatalogException(string message) : base(message)
    {
    }

    public CatalogException(string message, Exception innerException) : base(message, innerException)
    {
    }
}