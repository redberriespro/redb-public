using Autofac;
using Redb.Catalog.Common.Web.Binders.Parsers;

namespace Redb.Catalog;

public class CatalogCommonWebAutofac : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<ExportedColumnsParser>().AsSelf().SingleInstance();
    }
}