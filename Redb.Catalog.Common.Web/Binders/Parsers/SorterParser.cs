﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json.Linq;
using Redb.Catalog.Common.BasicTypes;
using Redb.Catalog.Common.Enums;
using Redb.Catalog.Common.Exceptions;

namespace Redb.Catalog.Common.Web.Binders.Parsers;

public class SorterParser<TSortableProperties> where TSortableProperties : struct, Enum
{
    private const string JsonPropertySorterPrefix = "d.";

    public async Task<Sorter<TSortableProperties>?> ParseAsync(ModelBindingContext bindingContext, JObject[]? dataSchemas = null)
    {
        var sortByValue = bindingContext.ValueProvider.GetValue("sortBy").FirstOrDefault();
        var sortDirectionValue = bindingContext.ValueProvider.GetValue("sortDirection").FirstOrDefault();

        if (sortByValue != null)
        {
            SortDirection sortDirection;

            if (sortDirectionValue != null)
            {
                if (!Enum.TryParse(sortDirectionValue, true, out sortDirection))
                {
                    throw new CatalogBadRequestException($"Invalid sorting definition. Unknown sorting direction '{sortDirectionValue}'.");
                }
            }
            else
            {
                sortDirection = SortDirection.Desc;
            }

            if (sortByValue.StartsWith(JsonPropertySorterPrefix))
            {
                var jsonProperty = sortByValue[JsonPropertySorterPrefix.Length..];

                if (CheckJsonProperty(jsonProperty, dataSchemas))
                {
                    return Sorter<TSortableProperties>.CreateSorterByJsonProperty(jsonProperty, sortDirection);
                }

                throw new CatalogBadRequestException($"Invalid sorting definition. Unknown json property '{sortByValue}'.");
            }

            if (Enum.TryParse<TSortableProperties>(sortByValue, true, out var property))
            {
                return Sorter<TSortableProperties>.CreateSorterByProperty(property, sortDirection);
            }

            throw new CatalogBadRequestException($"Invalid sorting definition. Unknown property '{sortByValue}'.");
        }

        return null;
    }

    private bool CheckJsonProperty(string jsonProperty, JObject[]? dataSchemas)
    {
        return dataSchemas?.Any(ds => ds["properties"]![jsonProperty] != null) ?? false;
    }
}
