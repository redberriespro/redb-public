﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Redb.Catalog.Common.Exceptions;

namespace Redb.Catalog.Common.Web.Binders.Parsers;

public class ExportedColumnsParser
{
    public string[]? Parse(ModelBindingContext bindingContext)
    {
        var columnsRaw = bindingContext.ValueProvider.GetValue("columns").FirstOrDefault();
        var columns = columnsRaw is not null ? columnsRaw.Split(";", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries) : Array.Empty<string>();

        if (columnsRaw is not null && columns.Length == 0)
        {
            throw new CatalogBadRequestException($"Invalid export column definition '{columnsRaw}'");
        }

        if (columns.Length > 0)
        {
            return columns;
        }

        return null;
    }
}
