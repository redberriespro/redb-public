﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Redb.Catalog.Common.Web.Binders.Factories;

namespace Redb.Catalog.Common.Web.Binders.Providers;

public class BindersProvider : IModelBinderProvider
{
    private readonly Dictionary<Type, IBinderFactory> _binderFactories = new();

    public BindersProvider(params IBinderFactory[] factories)
    {
        foreach(var factory in factories)
        {
            _binderFactories.Add(factory.ModelType, factory);
        }
    }

    public IModelBinder? GetBinder(ModelBinderProviderContext context)
    {
        ArgumentNullException.ThrowIfNull(context);

        if (_binderFactories.TryGetValue(context.Metadata.ModelType, out var factory))
        {
            return factory.Create(context);
        }

        return null;
    }
}
