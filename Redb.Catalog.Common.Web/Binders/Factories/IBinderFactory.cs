﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Redb.Catalog.Common.Web.Binders.Factories;

public interface IBinderFactory
{
    Type ModelType { get; }

    IModelBinder Create(ModelBinderProviderContext context); 
}
