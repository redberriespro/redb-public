using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Redb.Notifications.Core.Channels;
using Redb.Notifications.Core.Services;

namespace Redb.Notifications.Tests.Templates
{
    [TestFixture]
    public class SimpleTemplating
    {
        [Test]
        public async Task Test1()
        {
            var stub = new NotificationServiceCollectingStub();
            Assert.False(stub.Messages.Any());
            await stub.ScheduleDelivery(ChannelDefinitions.CHANNEL_SMS,
                "12345",
                null,
                "t1",
                null);
            Assert.AreEqual(1, stub.Messages.Count);

        }
    }
}