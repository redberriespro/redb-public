using Autofac;
using NUnit.Framework;
using Redb.Notifications.Core;
using Redb.Notifications.Core.Channels.Telegram;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Repositories;
using Redb.Notifications.Core.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Redb.Notifications.Tests.Channels
{
    [TestFixture]
    public class TelegramChannelTests
    {
        private static IContainer _container;
        private INotificationService _notificationService;

        [OneTimeSetUp]
        public async Task SetupContext()
        {
            var builder = new ContainerBuilder();

            builder.Register(c => new NotificationConfig
            {
                Channels = new[]
                {
                    new ChannelConfig
                    {
                        Name = "tele.volzhskaya",
                        Type = "telegram",
                        Telegram = new TelegramConfig
                        {
                            Token = "5257622905:AAEjupAnyGKlohIqSy6VBvtPnEa-FbtGL5A",
                            ChatId = -749720433 // оповещения волжская
                        }
                    },
                    new ChannelConfig
                    {
                        Name = "tele",
                        Type = "telegram",
                        Telegram = new TelegramConfig
                        {
                            Token = "5257622905:AAEjupAnyGKlohIqSy6VBvtPnEa-FbtGL5A",
                            ChatId = -781715955 // оповещения nav-dev
                        }
                    }
                }
            }).As<INotificationConfig>().SingleInstance();

            var na = new NotificationsAutofac { NoDataRepositories = true };
            builder.RegisterModule(na);
            na.RegisterStubDataRepositories(builder);

            _container = builder.Build();
            _notificationService = _container.Resolve<INotificationService>();

            var trs = _container.Resolve<INotificationTemplateRepository>() as NotificationTemplateRepositoryStub;
            await trs.EnsureNotificationTemplate("t1", "-", "hello hello", false);
            await trs.EnsureNotificationTemplate("t2", "-", $"Произошел тестовый инцидент.\r\nОбъект: Вулкан\r\nДата и время: {DateTime.Now}\r\nКомментарий: Извержение вулкана\r\nhttps://www.imdb.com/title/tt26349238/\r\nКоординаты: 56.649835, 161.358402", false);

        }

        [Test, Explicit("uses real channels")]
        public async Task TelegramBasicChannelSingleLineMessage()
        {
            var telest = _notificationService.CheckChannelStatus("tele");
            await _notificationService.ScheduleDelivery("tele", "none", "none", "t1");
        }

        [Test, Explicit("uses real channels")]
        public async Task TelegramBasicChannelMultiLineMessage()
        {
            var telest = _notificationService.CheckChannelStatus("tele");
            await _notificationService.ScheduleDelivery("tele", "none", "none", "t2");
        }

        [Test, Explicit("uses real channels")]
        public async Task TelegramBasicChannelMultiLineMessageWithImage()
        {
            var telest = _notificationService.CheckChannelStatus("tele");

            var imageFilename = "boom.jpg";

            var imageBytes = await File.ReadAllBytesAsync(Path.Combine("Data", imageFilename));

            await _notificationService.ScheduleDelivery("tele", "none", "none", "t2", attachments: new Dictionary<string, byte[]>
            {
                { "0-" + imageFilename, imageBytes },
                { "1-" + imageFilename, imageBytes },
                { "2-" + imageFilename, imageBytes },
                { "3-" + imageFilename, imageBytes },
            });
        }

        [Test, Explicit("uses real channels")]
        public async Task TelegramSpecialChannel()
        {
            var telest = _notificationService.CheckChannelStatus("tele.volzhskaya");
            await _notificationService.ScheduleDelivery("tele.volzhskaya", "none", "none", "t1");
        }
    }
}
