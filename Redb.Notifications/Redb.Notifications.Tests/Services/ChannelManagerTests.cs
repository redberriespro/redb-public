using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core;
using Redb.Notifications.Core.Channels;
using Redb.Notifications.Core.Models;
using Redb.Notifications.Core.Services;
using NUnit.Framework;
using Autofac;
using Redb.Notifications.Core.Config;

namespace Redb.Notifications.Tests
{
    [TestFixture]
    public class ChannelManagerTests
    {
        private static IContainer _container;

        [OneTimeSetUp]
        public async Task SetupContext()
        {
            var builder = new ContainerBuilder();

            builder.Register(c => new NotificationConfig
            {
                Channels = Array.Empty<ChannelConfig>()
            }).As<INotificationConfig>().SingleInstance();

            var na = new NotificationsAutofac { NoDataRepositories = true };
            builder.RegisterModule(na);
            na.RegisterStubDataRepositories(builder);

            _container = builder.Build();
        }

        [Test]
        public async Task Send_ByteArray_ShouldReturnSendResult()
        {
            // Arrange
            // var mockNotificationConfig = new Mock<INotificationConfig>();
            // var mockDumbChannel = new Mock<DumbChannel>();
            // var mockRmqChannelFactory = new Mock<Func<IChannelConfig, INotificationRmqChannel>>();
            // var mockNotificationChannel = new Mock<INotificationChannel>();
            // var mockNotificationPackage = new Mock<INotificationPackage>();

            // var deliveryReport = new DeliveryReport
            // {
            //     Success = true,
            //     Message = "Delivered"
            // };

            // mockNotificationChannel.Setup(c => c.Send(
            //     It.IsAny<string>(),
            //     It.IsAny<string>(),
            //     It.IsAny<string>(),
            //     It.IsAny<string>(),
            //     It.IsAny<string>(),
            //     It.IsAny<bool>(),
            //     It.IsAny<IEnumerable<Attachment>>(),
            //     It.IsAny<IDictionary<string, string>>(),
            //     It.IsAny<CancellationToken>()
            // )).ReturnsAsync(deliveryReport);

            var target = _container.Resolve<IChannelManager>();


            string base64String = 
                "Chp0ZWxlZ3JhbS5uYXZpZ2F0b3IudHJhY2tlZBIBLRoBLSoc0JTQvtC/LiDQuNC90YTQvtGA0LzQsNGG0LjRjzJOPGI+0J7Qv9C40YHQsNC90LjQtTo8L2I+\n" +
                "INCe0YTQvtGA0LzQu9C10L3QviDQk9CQ0Jgg0LHQtdC3INCy0YvQt9C+0LLQsCDQodCe0JMuOAFCCwjMzPe0lqjfPRAFSjUKDUNvcnJlbGF0aW9uSWQSJGM2\n" +
                "NDRlYjg5LWYxYmQtNDc5MC1hNTMwLTZlODcyYzM0MGE1ZEqBAgoUdGVsZWdyYW1GZWVkYmFja0luZm8S6AF7CiAgImluY2lkZW50SWQiOiAiMjAyNTAxMjcw\n" +
                "NzI1NTZfMTEyXzM3Nzk3NDUzIiwKICAicnNQYWNrYWdlVHlwZSI6ICJJbmNpZGVudENvbW1lbnRlZFJlc3BvbnNlIiwKICAicGFja2FnZUlkIjogImM2NDRl\n" +
                "Yjg5LWYxYmQtNDc5MC1hNTMwLTZlODcyYzM0MGE1ZCIsCiAgInNlbmRJbmNpZGVudEF0dGFjaG1lbnRzIjogIlRydWUiLAogICJjaGFubmVsIjogInRlbGVn\n" +
                "cmFtLm5hdmlnYXRvci50cmFja2VkIgp9ShYKDnJlcGx5VG9FbmFibGVkEgRUcnVlShoKD3NlbmRpbmdTdHJhdGVneRIHZGVmYXVsdEoxChByZXBseVRvTWVz\n" +
                "c2FnZUlkEh17CiAgIi0xMDAxODAxMzg4NDM5IjogNjIyMzAKfQ==";

            byte[] byteArray = Convert.FromBase64String(base64String);

            // Act
            var result = await target.Send(byteArray);

            // Assert
            Assert.NotNull(result);
            // Assert.True(result.DeliveryReport.Success);
            // Assert.Equal("Delivered", result.DeliveryReport.Message);
            // Assert.Equal(data.Length, result.Size);
        }
    }
}