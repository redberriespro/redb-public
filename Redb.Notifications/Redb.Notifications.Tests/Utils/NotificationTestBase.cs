using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Navigation;
using Newtonsoft.Json;
using NUnit.Framework;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Services;

namespace Redb.Notifications.Tests.Utils
{
    public class NotificationTestBase
    {
        protected static IContainer Container;
        protected NotificationConfig Config => Container.Resolve<NotificationConfig>();
        protected INavigationSession NotificationService;

        // [OneTimeSetUp]
        // public async Task RunBeforeAnyTests()
        // {
        //     var builder = await PrepareAutofacBuilder();
        //     
        //     try
        //     {
        //         var c = builder.Build();
        //         Container = c;
        //
        //         await InitTestFramework();
        //         
        //         NotificationService = Container.Resolve<INotificationService>();
        //     }
        //     catch (Exception ex)
        //     {
        //         // todo logging
        //         throw;
        //     }
        // }
        //
        // private async Task InitTestFramework()
        // {
        //     DeleteTestSqlBase();
        //     await CreateAndSeedSqlDatabase();
        // }
        //
        // public void DeleteTestSqlBase()
        // {
        //     GetDbCleaner().CleanDb();
        // }
        //
        // public async Task CreateAndSeedSqlDatabase()
        // {
        //     var ctx = Container.Resolve<GarDbContextFactory>().CreateContext();
        //     await ctx.Database.EnsureCreatedAsync();
        //     // await _securityService.InitSecurityObjects();
        //     //await _sqlSeedManager.Seed(_sqlServerConfig.UniverseFile);
        // }
        //
        // private async Task<ContainerBuilder> PrepareAutofacBuilder()
        // {
        //     var builder = new ContainerBuilder();
        //
        //     var appBinPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        //     var configFile = Path.Combine(appBinPath, "appsettings.json");
        //     //         ConfigBase<TestConfig>.Initialize(configFile);
        //     var configText = await File.ReadAllTextAsync(configFile);
        //     var config = JsonConvert.DeserializeObject<TestConfig>(configText);
        //     
        //     builder.Register(c => config).As<TestConfig>();
        //     builder.Register(c => config.Gar).As<GarConfig>();
        //
        //     builder.RegisterModule(new TestsAutofac());
        //     AddCustomModules(builder);
        //     return builder;
        // }
        //
        // protected virtual void AddCustomModules(ContainerBuilder builder)
        // {
        //     builder.RegisterModule(new GarBackendAutofac());
        // }
        //
        // protected PgSqlDbCleaner GetDbCleaner() => new PgSqlDbCleaner(Container.Resolve<GarConfig>().ConnectionString);
    }
}