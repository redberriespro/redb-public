﻿using Autofac;
using Autofac.Core;
using Microsoft.Extensions.Caching.Memory;
using Redb.Notifications.Core.Channels;
using Redb.Notifications.Core.Channels.Dumb;
using Redb.Notifications.Core.Providers;
using Redb.Notifications.Core.Repositories;
using Redb.Notifications.Core.Services;
using Redb.Notifications.Core.Templates;

namespace Redb.Notifications.Core
{
    public class NotificationsAutofac: Module
    {
        public bool NoDataRepositories = false;
        
        protected override void Load(ContainerBuilder builder)
        {
            LoadBasicComponents(builder);
            if (!NoDataRepositories)
            {
                LoadDataRepositories(builder);
            }
            
            builder.RegisterType<NotificationService>()
                .As<INotificationService>()
                .SingleInstance();

            builder.RegisterType<TemplateRenderer>()
                .As<ITemplateRenderer>()
                .SingleInstance()
                .PreserveExistingDefaults();

            builder.RegisterType<ChannelManager>().As<IChannelManager>();

            builder.RegisterType<SmsRmqChannel>().Named<INotificationRmqChannel>(ChannelDefinitions.CHANNEL_SMS);
            builder.RegisterType<EmailRmqChannel>().Named<INotificationRmqChannel>(ChannelDefinitions.CHANNEL_EMAIL);
            builder.RegisterType<TelegramRmqChannel>().Named<INotificationRmqChannel>(ChannelDefinitions.CHANNEL_TELEGRAM);
            builder.Register(
                (c, p) =>
                {
                    var config = p.TypedAs<Config.IChannelConfig>();
                    var code = string.IsNullOrEmpty(config.Type) 
                        ? ChannelDefinitions.CHANNEL_DUMB 
                        : config.Type;
                    return c.ResolveNamed<INotificationRmqChannel>(
                        code,
                        new Parameter[] 
                        { 
                            new NamedParameter("channelName", config.Name), 
                            new NamedParameter("config", config) 
                        }
                    );
            });
        }

        public void LoadDataRepositories(ContainerBuilder builder)
        {
            builder
                .RegisterType<NotificationTemplateRepository>()
                .As<INotificationTemplateRepository>();
        }

        private static void LoadBasicComponents(ContainerBuilder builder)
        {
            builder.RegisterType<CachingNotificationTemplateRepository>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<DumbChannel>()
                .SingleInstance();
        }
        
        public void RegisterStubService(ContainerBuilder builder)
        {
            builder.RegisterType<NotificationServiceCollectingStub>()
                .As<INotificationService>()
                .SingleInstance();
        }
        
        public void RegisterStubDataRepositories(ContainerBuilder builder)
        {
            builder.RegisterType<NotificationTemplateRepositoryStub>()
                .As<INotificationTemplateRepository>()
                .SingleInstance();

            builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>().SingleInstance();
        }

    }

    public class DefaultProviderAutofac : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DefaultNotificationsQueueingProvider>().As<INotificationsQueueingProvider>();
        }
    }
}
