#nullable enable
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Redb.Notifications.Core.Services
{
    internal class QueueTechSupport<T>
    {
        private static readonly ConcurrentQueue<T> _messageQueue = new ConcurrentQueue<T>();
        private readonly int _rateLimitDelay;
        private readonly int _maxQueueSize;
        private readonly Func<T, CancellationToken, Task>? _notifySupportChannel;
        private static Timer? _timer;
        private static readonly object _lock = new object();

        public QueueTechSupport(bool enabled, int rateLimitDelay, int maxQueueSize, Func<T, CancellationToken, Task> notifySupportChannel)
        {
            if (!enabled)
            {
                return;
            }
            _timer = new Timer(_ => Task.Run(() => ProcessQueue()), null, 0, rateLimitDelay);
            _maxQueueSize = maxQueueSize;
            _notifySupportChannel = notifySupportChannel;
        }

        public void AddMessage(T message)
        {
            if (_timer == null) return;

            lock (_lock)
            {
                // Проверка размера очереди и удаление самой старой записи, если необходимо
                while (_messageQueue.Count >= _maxQueueSize)
                {
                    _messageQueue.TryDequeue(out _);
                }

                // Добавление новой записи
                _messageQueue.Enqueue(message);
            }
        }

        private async Task ProcessQueue()
        {
            if (_notifySupportChannel == null) return;

            if (_messageQueue.TryDequeue(out var content))
            {
                await _notifySupportChannel(content, CancellationToken.None);
            }
        }
    }

}