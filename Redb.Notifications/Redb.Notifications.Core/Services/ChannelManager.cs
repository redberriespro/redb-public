using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Redb.Notifications.Core;
using Redb.Notifications.Core.Channels;
using Redb.Notifications.Core.Channels.Dumb;
using Redb.Notifications.Core.Channels.Email;
using Redb.Notifications.Core.Channels.Sms;
using Redb.Notifications.Core.Channels.Telegram;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Models;
using Redb.Notifications.Core.Providers;

namespace Redb.Notifications.Core.Services
{
    public interface IChannelManager
    {
        ChannelStatus CheckChannelStatus(string channelName);
        void NotifySupportChannel(ContentSupportChannel content);
        Task<DeliveryReport> Send(INotificationPackage package, CancellationToken ct = default);
        Task<ISendResult> Send(byte[] package, CancellationToken ct = default);

    }

    internal class ChannelManager : IChannelManager
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ChannelManager));
        private readonly INotificationConfig _notificationConfig;
        private readonly INotificationChannel _dumbChannelSingleton;
        private readonly Func<IChannelConfig, INotificationRmqChannel> _createRmqChannel;
        private readonly Dictionary<string, INotificationChannel> _channels = new Dictionary<string, INotificationChannel>();

        public ChannelManager(
            INotificationConfig notificationConfig,
            DumbChannel dumbChannelSingleton,
            Func<IChannelConfig, INotificationRmqChannel> createRmqChannel
            )
        {
            _notificationConfig = notificationConfig;
            _dumbChannelSingleton = dumbChannelSingleton;
            _createRmqChannel = createRmqChannel;
            CreateChannels();
        }

        public async Task<DeliveryReport> Send(INotificationPackage package, CancellationToken ct = default)
        {
            var (c, sc) = GetChannel(package.ChannelName);
            return await c.Send(
                sc,
                package.Address,
                package.Name,
                package.Subject,
                package.Body,
                package.IsHtml,
                package.Attachments,
                package.Properties,
                ct:ct);
        }

        private void CreateChannels()
        {
            foreach (var chcfg in _notificationConfig.Channels)
            {
                INotificationChannel channel;
                if (!chcfg.QueueMode)
                {
                    channel = chcfg.Type switch
                    {
                        ChannelDefinitions.CHANNEL_SMS => new SmsChannel(chcfg.Sms),
                        ChannelDefinitions.CHANNEL_EMAIL => new EmailChannel(chcfg.Email),
                        ChannelDefinitions.CHANNEL_TELEGRAM => new TelegramChannel(chcfg.Telegram, chcfg.SupportChannel),
                        ChannelDefinitions.CHANNEL_DUMB => _dumbChannelSingleton,
                        _ => throw new NotException("channel not found, type " + chcfg.Type)
                    };
                }
                else
                {
                    channel = chcfg.Type == ChannelDefinitions.CHANNEL_DUMB
                        ? _dumbChannelSingleton
                        : _createRmqChannel(chcfg);
                }

                if (channel == null)
                    continue;

                if (_channels.ContainsKey(chcfg.Name))
                    throw new NotException($"multiple definitions for notification channel '{chcfg.Name}'");
                _channels[chcfg.Name] = channel;
            }
        }

        private (INotificationChannel, string) GetChannel(string channelName)
        {
            var s = channelName.Split("#");
            if (!s.Any() || !_channels.ContainsKey(s[0]))
                throw new ApplicationException($"notification channel not found: '{channelName}'");
            return s.Length switch
            {
                0 => (null, null),
                1 => (_channels[s[0]], null),
                _ => (_channels[s[0]], s[1])
            };
        }

        public ChannelStatus CheckChannelStatus(string channelName)
        {
            try
            {
                var (c, sc) = GetChannel(channelName);
                var cs = c.GetStatus(sc);
                cs.IsConfigured = true;
                return cs;
            }
            catch (ApplicationException _)
            {
                return new ChannelStatus
                {
                    IsConfigured = false
                };
            }
        }

        public async Task<ISendResult> Send(byte[] data, CancellationToken ct = default)
        {
            var package = NotificationPackage.Create(data);
            try
            {
                var report = await Send(
                    package,
                    ct);
    
                return new SendResult
                    {
                        TimeRegister = package.TimeRegister,
                        DeliveryReport = report,
                        Size = data?.Length ?? 0
                    };
            }
            catch (System.Exception ex)
            {
                if (package != null)
                {
                    package.Attachments = null; // чтобы лог не засарять если спамить ошибка будет
                    _log.Error($"Send error info: \n{JsonConvert.SerializeObject(package, Formatting.Indented)}", ex);
                }
                throw ex;
            }
        }

        public void NotifySupportChannel(ContentSupportChannel content)
        {
            foreach (var ch in _channels.Values.Where(ch => ch.IsSupportChannel))
            {
                ch.NotifyTechSupport(content);
            }
        }
    }
}
