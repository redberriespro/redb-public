using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Redb.Catalog.Common.Models.Notifications;
using Redb.Notifications.Core.Channels;
using Redb.Notifications.Core.Entities;
using Redb.Notifications.Core.Providers;

namespace Redb.Notifications.Core.Services
{
    public interface INotificationService
    {
        Task ScheduleDelivery(string channelName,
            string toAddress,  string toName,
            string templateId, 
            IReadOnlyDictionary<string, object> parameters = null,
            List<string> extraRecipients = null,
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string> properties = null,
            CancellationToken ct = default
        );

        Task<INotificationPackage> CreatePackage(
            string channelName,
            string toAddress,
            string toName,
            string templateId,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, byte[]> attachments = null,
            IReadOnlyDictionary<string, string> properties = null);

        ChannelStatus CheckChannelStatus(string channelName);

        Task<List<NotificationTemplateModel>> GetAllTemplates();

        Task<NotificationTemplateModel> GetTemplateById(string id);
        Task Update(string notificationId, NotificationTemplateModel updatedEntity);
    }
}
