using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Redb.Catalog.Common.Models.Notifications;
using Redb.Notifications.Core.Channels;
using Redb.Notifications.Core.Entities;
using Redb.Notifications.Core.Providers;

namespace Redb.Notifications.Core.Services
{
    public class NotificationServiceCollectingStub: INotificationService
    {
        public class MessageItem
        {
            public DateTime ScheduledTime;

            public string Channel;
            public string RecipientAddress;
            public string RecipientName;
            public string TemplateId;
            public IReadOnlyDictionary<string, object> Options;
            public IReadOnlyDictionary<string, byte[]> Attachments { get; set; }
            public List<string> ExtraEmails { get; set; }
        };
        
        public List<MessageItem> Messages = new List<MessageItem>();
        
        public Task ScheduleDelivery(string channelName,
            string toAddress,
            string toName,
            string templateId,
            IReadOnlyDictionary<string, object> parameteres,
            List<string> extraRecipients = null,
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string> properties = null,
            CancellationToken ct = default
        )
        {
            Messages.Add(new MessageItem
            {
                Channel = channelName,
                RecipientAddress = toAddress,
                TemplateId = templateId, 
                Options = parameteres, 
                RecipientName = toName,
                Attachments = attachments,
                ExtraEmails = extraRecipients
            });

            return Task.CompletedTask;
        }

        public ChannelStatus CheckChannelStatus(string channelName)
            => new ChannelStatus
            {
                LastMessage = Messages.Count switch
                {
                    0 => null,
                    _ => Messages.Last().ScheduledTime
                }
            };

        public Task<List<NotificationTemplateModel>> GetAllTemplates()
        {
            throw new NotImplementedException();
        }

        public Task<NotificationTemplateModel> GetTemplateById(string id)
        {
            throw new NotImplementedException();
        }

        public Task Update(string notificationId, NotificationTemplateModel updatedEntity)
        {
            throw new NotImplementedException();
        }

        public Task<INotificationPackage> CreatePackage(string channelName, string toAddress, string toName, string templateId, IReadOnlyDictionary<string, object> parameters, IReadOnlyDictionary<string, byte[]> attachments = null, IReadOnlyDictionary<string, string> properties = null)
        {
            throw new NotImplementedException();
        }
    }
}
