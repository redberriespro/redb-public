#nullable enable

namespace Redb.Notifications.Core.Services
{
    public class ContentSupportChannel
    {
        public ContentSupportChannel(string message, TypeNotifySupCh type = TypeNotifySupCh.Default)
        {
            Message = message;
            Type = type;
        }

        public string Message { get; }
        public TypeNotifySupCh Type { get; set; }
    }
    

    public enum TypeNotifySupCh
    {
        Default,

        /// <summary>
        /// сообщение сохраняется в dropout при ошибке отправки
        /// </summary>
        MessageSaveToDropout,

        /// <summary>
        /// сообщение не было отправлено
        /// </summary>
        FailedProcessMessage,

        /// <summary>
        /// сообщение не было отправлено, но будет попытка повторной отправки
        /// </summary>
        FailedProcessMessageAndWillRetry
    }
}
