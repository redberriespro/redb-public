﻿using log4net;
using Redb.Catalog.Common.Models.Notifications;
using Redb.Notifications.Core.Channels;
using Redb.Notifications.Core.Entities;
using Redb.Notifications.Core.Providers;
using Redb.Notifications.Core.Repositories;
using Redb.Notifications.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Redb.Notifications.Core.Services
{
    public class NotificationService : INotificationService
    {
        private ILog _log = LogManager.GetLogger(typeof(NotificationService));
        private readonly INotificationTemplateRepository _notificationTemplateRepository;
        private readonly IChannelManager _channelManager;
        private readonly ITemplateRenderer _templateRender;

        //Удалить
        // private readonly NotificationConfig _notificationConfig;
        // private readonly DumbChannel _dumbChannelSingleton;

        // private readonly Dictionary<string, INotificationChannel> _channels = new Dictionary<string, INotificationChannel>();

        // private readonly EventLogRepository _eventLogRepository;

        public NotificationService(
            CachingNotificationTemplateRepository notificationTemplateRepository,
            IChannelManager channelManager,
            ITemplateRenderer templateRender
            )
        {
            _notificationTemplateRepository = notificationTemplateRepository;
            _channelManager = channelManager;
            _templateRender = templateRender;
        }

        public async Task ScheduleDelivery(string channelName, string toAddress, string toName, string templateId,
            IReadOnlyDictionary<string, object> parameters, List<string> extraRecipients = null, 
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string> properties = null,
            CancellationToken ct = default)
        {
            
            var t = await GetTemplate(templateId);

            var subj = "";
            
            if (!String.IsNullOrWhiteSpace(t.Subject))
            {
                subj = System.Net.WebUtility.HtmlDecode(_templateRender.RenderTemplate(t.Subject, parameters));
            }
            
            NotificationTemplateEntity masterTemplate = null;
            if (!String.IsNullOrEmpty(t.MasterTemplateId))
                masterTemplate = await GetTemplate(t.MasterTemplateId);
            
            var body = MakeBody(t, masterTemplate, parameters);
            await _channelManager.Send(new NotificationPackage()
            {
                ChannelName = channelName,
                Address     = toAddress,
                Name        = toName,
                Attachments = attachments,
                Subject     = subj,
                Body        = body,
                IsHtml      = t.IsHtml,
                Properties  = properties
            }
            ,ct:ct);
        }

        public async Task<INotificationPackage> CreatePackage(
            string channelName,
            string toAddress,
            string toName,
            string templateId,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, byte[]> attachments = null,
            IReadOnlyDictionary<string, string> properties = null)
        {
            var t = await GetTemplate(templateId);

            var subj = "";
            
            if (!String.IsNullOrWhiteSpace(t.Subject))
            {
                subj = System.Net.WebUtility.HtmlDecode(_templateRender.RenderTemplate(t.Subject, parameters));
            }
            
            NotificationTemplateEntity masterTemplate = null;
            if (!String.IsNullOrEmpty(t.MasterTemplateId))
                masterTemplate = await GetTemplate(t.MasterTemplateId);
            
            var body = MakeBody(t, masterTemplate, parameters);
            return new NotificationPackage()
            {
                ChannelName = channelName,
                Address     = toAddress,
                Name        = toName,
                Attachments = attachments,
                Subject     = subj,
                Body        = body,
                IsHtml      = t.IsHtml,
                Properties  = properties
            };
        }

        private async Task<NotificationTemplateEntity> GetTemplate(string code)
        {
            var et = await _notificationTemplateRepository.GetCompiledById(code);
            if (et != null) return et;
            _log.Error("fatal: missing notification template " + code);
            throw new ArgumentException("notification template missing: " + code, nameof(code));
        }

        private string MakeBody(NotificationTemplateEntity emailTemplate, NotificationTemplateEntity masterTemplate, IReadOnlyDictionary<string, object> parameteres)
        {
            string finalBody;
            if (masterTemplate != null && emailTemplate.MasterTemplateId == masterTemplate.NotificationTemplateId)
            {
                finalBody = _templateRender.RenderTemplateWithMaster(masterTemplate.Body, emailTemplate.Body,
                    parameteres);
            }
            else
            {
                finalBody = _templateRender.RenderTemplate(emailTemplate.Body, parameteres);
            }

            return finalBody;
        }

        public ChannelStatus CheckChannelStatus(string channelName)
        {
            return _channelManager.CheckChannelStatus(channelName);
        }

        public async Task<List<NotificationTemplateModel>> GetAllTemplates()
        {
            return (await _notificationTemplateRepository.GetAll())
                .Select(x=> NotificationTemplateModel.FromEntity(x))
                .ToList();
        }

        public async Task<NotificationTemplateModel> GetTemplateById(string id)
        {
            var entity = await _notificationTemplateRepository.GetById(id);
            return NotificationTemplateModel.FromEntity(entity);
        }

        public async Task Update(string notificationId, NotificationTemplateModel updatedEntity)
        {
            var existedEntity = await _notificationTemplateRepository.GetById(notificationId);
            if(existedEntity != null)
            {
                //можем обновить только 3 поля
                existedEntity.Subject = updatedEntity.AdditionalData.Subject;
                existedEntity.Body = updatedEntity.AdditionalData.Body;
                existedEntity.IsHtml = updatedEntity.AdditionalData.IsHtml; 
                await _notificationTemplateRepository.UpdateNotificationTemplate(existedEntity);
            }
        }
    }
}
