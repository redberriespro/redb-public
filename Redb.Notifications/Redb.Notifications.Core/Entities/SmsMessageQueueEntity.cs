using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Redb.Notifications.Core.Entities
{
    /// <summary>
    /// ������� ��������� SMS
    /// </summary>
    [Table("notifications_sms_q")]
    public class SmsMessageQueueEntity
    {
        /// <summary>
        /// �������������
        /// </summary>
        [Key]
        [Column("id")]
        public Guid MessageId { get; set; }

        /// <summary>
        /// ��������������� ���� ��������
        /// </summary>
        [Column("scheduled")]
        public DateTime ScheduledTime { get; set; }

        /// <summary>
        /// ���� �������� �����������
        /// </summary>
        [Column("sent")]
        public DateTime? SentTime { get; set; }

        /// <summary>
        /// ���� ���������
        /// </summary>
        [Column("text")]
        public string Text { get; set; }
    }
}