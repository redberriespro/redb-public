using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Redb.Notifications.Core.Entities
{
    public static class NotificationDbContextConfiguration
    {
        public static void ConfigureModel(ModelBuilder builder)
        {
            builder.Entity<NotificationTemplateEntity>()
                .HasKey(k => k.NotificationTemplateId);

            builder.Entity<EmailMessageQueueEntity>()
                .HasIndex(k => new {k.ScheduledTime, k.SentTime});
            
            builder.Entity<SmsMessageQueueEntity>()
                .HasIndex(k => new {k.ScheduledTime, k.SentTime});

        }
    }
}