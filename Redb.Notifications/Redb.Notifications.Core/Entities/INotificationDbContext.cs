using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Redb.Notifications.Core.Entities
{
    public interface INotificationDbContext : IAsyncDisposable
    {
        DbSet<EmailMessageQueueEntity> EmailMessageQueue { get; set; }
        DbSet<SmsMessageQueueEntity> SmsMessageQueue { get; set; }
        DbSet<NotificationTemplateEntity> NotificationTemplates { get; set; }
        
        public Task SaveChangesAsync();
        EntityEntry<TEntity> Entry<TEntity>([NotNull] TEntity entity)
            where TEntity : class;

        public DatabaseFacade Database { get; }
    }
    
    public interface INotificationDbContextFactory
    {
        INotificationDbContext CreateContext();
    }
}