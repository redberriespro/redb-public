using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Redb.Notifications.Core.Entities
{
    /// <summary>
    /// ������� ��������� �� email
    /// </summary>
    [Table("notifications_email_q")]
    public class EmailMessageQueueEntity
    {
        /// <summary>
        /// �������������
        /// </summary>
        [Key]
        [Column("id")]
        public Guid MessageId { get; set; }

        /// <summary>
        /// ��������������� ���� ��������
        /// </summary>
        [Column("scheduled")]
        public DateTime ScheduledTime { get; set; }

        /// <summary>
        /// ���� �������� �����������
        /// </summary>
        [Column("sent")]
        public DateTime? SentTime { get; set; }
        
        /// <summary>
        /// ���� ������
        /// </summary>
        [Column("subject")]
        public string Subject { get; set; }

        /// <summary>
        /// ���� ������
        /// </summary>
        [Column("body")]
        [MaxLength(30*1024*1024)]
        public byte[] Body { get; set; }
    }
    
    
}