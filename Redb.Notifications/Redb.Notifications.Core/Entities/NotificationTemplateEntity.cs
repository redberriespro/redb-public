using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Redb.Notifications.Core.Entities
{
    /// <summary>
    /// ������ �����������
    /// </summary>
    [Table("notifications_templates")]
    public class NotificationTemplateEntity
    {
        /// <summary>
        /// �������������
        /// </summary>
        [Column("id")]
        public string NotificationTemplateId { get; set; }

        /// <summary>
        /// ����
        /// </summary>
        [Required]
        [Column("subject")]
        public string Subject { get; set; }

        /// <summary>
        /// ����
        /// </summary>
        [Required]
        [DataType(DataType.Text)]
        [Column("body")]
        public string Body { get; set; }
        
        /// <summary>
        /// ������������ �������� �������
        /// </summary>
        [Column("master_id")]
        public string MasterTemplateId { get; set; }
        
        /// <summary>
        /// ���� �������� �� ���� ������� HTML
        /// </summary>
        [Column("is_html")]
        public bool IsHtml { get; set; }
    }
}