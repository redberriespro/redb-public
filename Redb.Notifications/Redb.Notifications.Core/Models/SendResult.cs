using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ProtoBuf;
using Redb.Notifications.Core.Channels;
using Redb.Notifications.Core.Services;

namespace Redb.Notifications.Core.Models
{
    public interface ISendResult
    {
        DateTime TimeRegister { get; }
        DeliveryReport DeliveryReport { get; }
        int Size { get; }
    }

    // ВАЖНО!!! Объекту namespace не менять т.к. востановление обекта в происходит автоматически по Redb.Notifications.Core.Providers.Unpack  
    [ProtoContract]
    public class SendResult : ISendResult
    {
        [ProtoMember(1)] 
        public DateTime TimeRegister { get; set; }

        [ProtoMember(2)] 
        public DeliveryReport DeliveryReport { get; set; }

        [ProtoMember(3)] 
        public int Size { get; set; }
    }
}
