﻿using Redb.Notifications.Core.Entities;

namespace Redb.Catalog.Common.Models.Notifications
{
    public class NotificationTemplateModel
    {
        public AdditionalDataModel AdditionalData {  get; set; }

        public static NotificationTemplateModel FromEntity(NotificationTemplateEntity entity)
        {
            if (entity == null)
                return null;
            return new NotificationTemplateModel
            {
                AdditionalData= new AdditionalDataModel
                {
                    Body = entity.Body,
                    Subject = entity.Subject,
                    IsHtml = entity.IsHtml,
                    NotificationTemplateId = entity.NotificationTemplateId
                },
            };
        }
    }
    public class AdditionalDataModel
    {
        public string NotificationTemplateId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }

    }
}
