using System.Collections.Generic;
using Newtonsoft.Json;
using Redb.Notifications.Core.Channels;

namespace Redb.Notifications.Core.Config
{
    public class ChannelConfig : IChannelConfig
    {
        [JsonProperty("name")] public string Name { get; set; } = "";

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("email")]
        public EmailConfig Email { get; set; }

        [JsonProperty("telegram")]
        public TelegramConfig Telegram { get; set; }

        [JsonProperty("sms")]
        public SmsConfig Sms { get; set; }

        [JsonProperty("queue")]
        public bool QueueMode { get; set; }

        [JsonProperty("queueName")]
        public string QueueName { get; set; }

        /// <summary>
        /// Имя exchange для отправки информации об отправленных сообщений 
        /// </summary>
        [JsonProperty("feedbackExName")]
        public string FeedbackExName { get; set; }
                
        /// <summary>
        /// Канал для уведомлений об ошибках
        /// </summary>
        [JsonProperty("supportChannel")]
        public SupportChannelConfig SupportChannel { get; set; } = new SupportChannelConfig();
    }

    public class TelegramConfig
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("chatId")]
        public long ChatId { get; set; }

        [JsonProperty("answer")]
        public TelegramAnswerConfig Answer { get; set; }

        /// <summary>
        /// ����� ����� ��������� ��� ��������� 429 �� ���������� � ��
        /// </summary>
        [JsonProperty("requestDelay")]
        public int RequestDelay { get; set; } = 10000;

        [JsonProperty("timeout")]
        public int Timeout { get; set; } = 100;
    }

    public class SupportChannelConfig
    {
        /// <summary>
        /// Включить уведомления
        /// </summary>
        [JsonProperty("enabled")]
        public bool Enabled { get; set; } = false;

        /// <summary>
        /// Размер буфера уведомлений
        /// </summary>
        [JsonProperty("maxQueueSize")]
        public int MaxQueueSize { get; set; } = 10;

        /// <summary>
        /// Уведомлять если  сообщение сохраняется в dropout при ошибке отправки
        /// </summary>
        [JsonProperty("notifyMessageSaveToDropout")]
        public bool NotifyMessageSaveToDropout { get; set; } = true;

        /// <summary>
        /// Уведомлять если сообщение не было отправлено
        /// </summary>
        [JsonProperty("notifyFailedProcessMessage")]
        public bool NotifyFailedProcessMessage { get; set; } = true;

        /// <summary>
        /// Уведомлять если сообщение не было отправлено и будет попытка повторной отправки
        /// </summary>
        [JsonProperty("notifyFailedProcessMessageAndWillRetry")]
        public bool NotifyFailedProcessMessageAndWillRetry { get; set; } = false;
    }

    public class TelegramAnswerConfig
    {
        [JsonProperty("default")]
        public string[] Default { get; set; }

        [JsonProperty("numButtonsInRow")]
        public int NumButtonsInRow { get; set; } = 3;
    }
    
    public class EmailConfig
    {
        [JsonProperty("smtpHost")]
        public string SmtpHost { get; set; }
        [JsonProperty("smtpPort")]
        public int SmtpPort { get; set; }

        [JsonProperty("useSsl")]
        public bool SmtpSSL { get; set; }
        
        [JsonProperty("smtpUser")]
        public string SmtpUser { get; set; }
        
        [JsonProperty("smtpPassword")]
        public string SmtpPassword { get; set; }
        
        [JsonProperty("debugRecipients")]
        public string[] DebugEmails { get; set; }
    }
    
    public class SmsConfig
    {
        [JsonProperty("provider")]
        public string Provider { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
        
        [JsonProperty("sourceAddress")]
        public string SourceAddress { get; set; }
    }
    
    public interface INotificationConfig 
    {
        IEnumerable<IChannelConfig> Channels { get; }
    }

    public interface IChannelConfig
    {
        string Name { get; }
        string Type { get; }
        EmailConfig Email { get; }
        TelegramConfig Telegram { get; }
        SmsConfig Sms { get; }
        bool QueueMode { get; }
        string QueueName { get; }
        string FeedbackExName { get; }
        SupportChannelConfig SupportChannel { get; }
    }

    public class NotificationConfig : INotificationConfig
    {
        [JsonProperty("channels")] public ChannelConfig[] Channels { get; set; }

        IEnumerable<IChannelConfig> INotificationConfig.Channels => Channels;

        public static NotificationConfig CreateDumbConfig()
            => new NotificationConfig {
            Channels = new[]
            {
                new ChannelConfig
                {
                    Type = ChannelDefinitions.CHANNEL_DUMB
                }
            }
        };
}
}
