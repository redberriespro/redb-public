using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ProtoBuf;

namespace Redb.Notifications.Core.Providers
{
    public interface IQueueingPackage
    {
        string RoutingKey { get; }
        string CorrelationId { get; }
        byte[] ToArrayByte();
    }

    public static class ProtoExtension
    {
        public static byte[] ToArrayByte<T>(this T obj)
        {
            using var ms = new System.IO.MemoryStream();

            ProtoBuf.Serializer.Serialize(ms, obj);
            return ms.ToArray();
        }

        public static T FromArrayByte<T>(this byte[] bytes)
        {
            using var ms = new System.IO.MemoryStream(bytes);
            return ProtoBuf.Serializer.Deserialize<T>(ms);
        }
        

        public static byte[] Pack<T>(this T obj)
        {
            using var ms = new System.IO.MemoryStream();
            ProtoBuf.Serializer.Serialize(ms, obj);
            var package = new QueueingPackageContainer
            {
                // TypeName = obj.GetType().FullName,
                TypeName = obj.GetType().AssemblyQualifiedName,
                Data = ms.ToArray()
            };
            using var msPackage = new System.IO.MemoryStream();
            ProtoBuf.Serializer.Serialize(msPackage, package);
            return msPackage.ToArray();
        }      

        public static object Unpack(this byte[] bytes)
        {
            //dummy comment
            using var ms = new System.IO.MemoryStream(bytes);
            var package = ProtoBuf.Serializer.Deserialize<QueueingPackageContainer>(ms);
            var type = FindTypeByName(package.TypeName);//Type.GetType(package.TypeName);
           
            using var msData = new System.IO.MemoryStream(package.Data);
            return ProtoBuf.Serializer.Deserialize(type, msData);
        }

        private class AssemblyQualifiedName
        {
            public string FullTypeName { get; set; }
            public string AssemblyName { get; set; }
        }

        private static AssemblyQualifiedName ParseAssemblyQualifiedName(string assemblyQualifiedName)
        {
            var parts = assemblyQualifiedName.Split(", ");
            return new AssemblyQualifiedName
            {
                FullTypeName = parts[0],
                AssemblyName = parts[1]
            };
        }

        private static Type FindTypeByName(string typeName)
        {
            var assemblyQualified = ParseAssemblyQualifiedName(typeName);
            var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.GetName().Name == assemblyQualified.AssemblyName);
            if (assembly == null)
            {
                throw new InvalidOperationException($"Assembly '{assemblyQualified.AssemblyName}' for '{typeName}' not found");
            }
            var type = assembly.GetType(assemblyQualified.FullTypeName);
            if (type == null)
            {
                throw new InvalidOperationException($"Type '{typeName}' not found");
            }
            return type;
        }
    }

    [ProtoContract]
    public class QueueingPackageContainer
    {
        [ProtoMember(1)]
        public string TypeName { get; set; }

        [ProtoMember(2)]
        public byte[] Data { get; set; }
    }
}
