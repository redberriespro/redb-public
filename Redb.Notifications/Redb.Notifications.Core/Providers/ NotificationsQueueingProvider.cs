using System;
using System.Collections.Generic;
using Redb.Notifications.Core.Services;
using ProtoBuf;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Redb.Notifications.Core.Providers
{
    public interface INotificationsQueueingProvider
    {
        Task Send(INotificationPackage package, CancellationToken ct = default);
    }
    
    public interface INotificationPackage : IQueueingPackage
    {
        string IQueueingPackage.RoutingKey => ChannelName;
        string ChannelName { get; }
        string Address { get; }
        string Name { get; }
        string Subject { get; }
        string Body { get; }
        bool IsHtml { get; }
        IReadOnlyDictionary<string, byte[]> Attachments { get; }
        IReadOnlyDictionary<string, string> Properties { get; }
    }

    public abstract class NotificationsQueueingProvider : INotificationsQueueingProvider
    {
        public abstract Task Send(INotificationPackage package, CancellationToken ct = default);
    }

    public sealed class DefaultNotificationsQueueingProvider : NotificationsQueueingProvider
    {
        private readonly IChannelManager _channelManager;

        public  DefaultNotificationsQueueingProvider(IChannelManager channelManager)
        {
            _channelManager = channelManager;
        }
        public override async Task Send(INotificationPackage package, CancellationToken ct = default)
        {
            await _channelManager.Send(package, ct);
        }
    }

    [ProtoContract]
    public class NotificationPackage : INotificationPackage
    {
        public static readonly string CORRELATION_ID_KEY = "CorrelationId";
        [ProtoMember(1)]
        public string ChannelName { get; set; }
        [ProtoMember(2)]
        public string Address { get; set; }
        [ProtoMember(3)]
        public string Name { get; set; }
        [ProtoMember(4)]
        public IReadOnlyDictionary<string, byte[]> Attachments { get; set; } = new Dictionary<string, byte[]>();
        [ProtoMember(5)]
        public string Subject { get; set; }
        [ProtoMember(6)]
        public string Body { get; set; }
        [ProtoMember(7)]
        public bool IsHtml { get; set; }
        /// <summary>
        /// Время регистрации уведомления для RMQ
        /// </summary>
        [ProtoMember(8)]
        public DateTime TimeRegister { get; set; }
        /// <summary>
        /// Дополнительные свойства
        /// </summary>
        [ProtoMember(9)]
        public IReadOnlyDictionary<string,string> Properties { get; set; } = new Dictionary<string, string>();

        string IQueueingPackage.CorrelationId => Properties?.GetValueOrDefault(CORRELATION_ID_KEY);

        public byte[] ToArrayByte()
        {
            using MemoryStream ms = new MemoryStream();
            Serializer.Serialize(ms, this);
            return ms.ToArray();
        }

        public static NotificationPackage Create(byte[] data)
        {
            using MemoryStream ms = new MemoryStream();
            ms.Write(data, 0, data.Length);
            ms.Seek(0, SeekOrigin.Begin);
            return Serializer.Deserialize<NotificationPackage>(ms);
        }
    }
}
