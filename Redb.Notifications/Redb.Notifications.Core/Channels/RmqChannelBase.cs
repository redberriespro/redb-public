using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Providers;

namespace Redb.Notifications.Core.Channels
{
    public interface INotificationRmqChannel : INotificationChannel
    {

    }

    internal abstract class RmqChannelBase : ChannelBase, INotificationRmqChannel
    {
        private readonly INotificationsQueueingProvider _notificationsQueueingProvide;

        public RmqChannelBase(INotificationsQueueingProvider notificationsQueueingProvide)
        {
            _notificationsQueueingProvide = notificationsQueueingProvide;
        }

        protected internal abstract string GetChannelName();

        protected override Task<DeliveryReport> InternalSend(
            string toAddress,
            string toName,
            string subject,
            string body,
            bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string> properties = null, 
            CancellationToken ct = default)
        {
            _notificationsQueueingProvide.Send(new NotificationPackage()
            {
                ChannelName = GetChannelName(),
                Address = toAddress,
                Name = toName,
                Attachments = attachments,
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                TimeRegister = System.DateTime.UtcNow,
                Properties = properties == null ? CreateProperties() : AddProperties(new Dictionary<string, string>(properties))
            },
            ct: ct);

            return Task.FromResult(new DeliveryReport());
        }

        private Dictionary<string, string> AddProperties(Dictionary<string, string> properties)
        {
            UploadProperties(properties);
            return properties;
        }

        private Dictionary<string, string> CreateProperties()
        {
            var properties = new Dictionary<string, string>();
            UploadProperties(properties);
            return properties;
        }

    }
}
