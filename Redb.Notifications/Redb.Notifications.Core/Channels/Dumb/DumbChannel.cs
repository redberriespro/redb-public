using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Services;

namespace Redb.Notifications.Core.Channels.Dumb
{
    public class DumbChannel:ChannelBase
    {
        public class MessageItem
        {
            public DateTime ScheduledTime;

            public string Channel;
            public string RecipientAddress;
            public string RecipientName;
            public string Subject;
            public string TemplateId;
            public string Body;
            public bool IsHtml;
            public IReadOnlyDictionary<string, byte[]> Attachments { get; set; }
        };
        
        public List<MessageItem> Messages = new List<MessageItem>();

        protected override Task<DeliveryReport> InternalSend(string toAddress, string toName, string subject, string body, bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string> properties = null,
            CancellationToken ct = default)
        {
            // to avoid memory leakage
            if (Messages.Count > 100) 
                Messages.Clear();
            
            Messages.Add(new MessageItem
            {
                RecipientAddress = toAddress,
                RecipientName = toName,
                ScheduledTime = DateTime.Now,
                Subject = subject, Body = body,
                IsHtml = isHtml, Attachments = attachments
            });

            return Task.FromResult(new DeliveryReport());
        }

        public override void  UploadProperties(Dictionary<string, string> properties) { }
    }
}
