using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Services;

namespace Redb.Notifications.Core.Channels
{
    public interface INotificationChannel
    {
        bool IsSupportChannel { get; }

        Task<bool> IsOnline();

        ChannelStatus GetStatus(string subChannel = null);

        Task<DeliveryReport> Send(
            string subchannel,
            string toAddress,
            string toName,
            string subject,
            string body,
            bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null,
            IReadOnlyDictionary<string, string> properties = null,
            CancellationToken ct = default);
            
        void UploadProperties(Dictionary<string, string> properties);
        void NotifyTechSupport(ContentSupportChannel content);
    }
}
