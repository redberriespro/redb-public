#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ProtoBuf;
using Telegram.Bot.Types;

namespace Redb.Notifications.Core.Channels
{
    [ProtoContract]
    [ProtoInclude(100, typeof(TelegramDeliveryReport))]
    public class DeliveryReport
    {
        [ProtoMember(1)] 
        public string? FeedbackInfo { get; internal set; }
    }

    [ProtoContract]
    public class TelegramDeliveryReport : DeliveryReport
    {
        public TelegramDeliveryReport() { }

        public TelegramDeliveryReport(Message messageInfo) : this(new[] { messageInfo }) { }

        public TelegramDeliveryReport(IEnumerable<Message> messageInfos)
        {
            MessageReports.AddRange(messageInfos.Select(m => new TelegramMessageReport(m)));
        }

        public TelegramDeliveryReport(TelegramDeliveryReport messageInfos)
        {
            Include(messageInfos);
        }

        public TelegramDeliveryReport(IEnumerable<Message> messageInfos, IEnumerable<TelegramDeliveryReport> reports)
            : this(messageInfos)
        {
            foreach (var report in reports)
            {
                Include(report);
            }
        }

        internal void Include(TelegramDeliveryReport? reportMedia)
        {
            if (reportMedia == null)
                return;
            MessageReports.AddRange(reportMedia.MessageReports);
        }

        [ProtoMember(1)] 
        public List<TelegramMessageReport> MessageReports { get; set; } = new List<TelegramMessageReport>();
    }

    [ProtoContract]
    public class TelegramMessageReport
    {
        public TelegramMessageReport()
        {
        }

        public TelegramMessageReport(Message message)
        {
            MessageId = message.MessageId;
            ChatId = message.Chat.Id;
        }

        [ProtoMember(1)] 
        public int MessageId { get; set; }

        [ProtoMember(2)] 
        public long ChatId { get; set; }
    }

}
