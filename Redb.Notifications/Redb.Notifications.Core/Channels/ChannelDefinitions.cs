namespace Redb.Notifications.Core.Channels
{
    public static class ChannelDefinitions
    {
        public const string CHANNEL_EMAIL = "email";
        public const string CHANNEL_TELEGRAM = "telegram";
        public const string CHANNEL_SMS = "sms";
        public const string CHANNEL_DUMB = "dumb";
    }
}