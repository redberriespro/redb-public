using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Providers;

namespace Redb.Notifications.Core.Channels
{
    internal class EmailRmqChannel : RmqChannelBase
    {
        private readonly string _channelName;
        private readonly EmailConfig _config;

        public EmailRmqChannel(
            INotificationsQueueingProvider notificationsQueueingProvide,
            string channelName,
            IChannelConfig config)
            : base(notificationsQueueingProvide)
        {
            _channelName = channelName;
            _config = config.Email;
        }

        protected internal override string GetChannelName()
        {
            return _channelName;
        }

        public override void UploadProperties(Dictionary<string, string> properties) { }
    }
}
