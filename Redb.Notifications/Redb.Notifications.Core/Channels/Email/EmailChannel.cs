using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using NUglify;
using Redb.Notifications.Core.Config;

namespace Redb.Notifications.Core.Channels.Email
{
    public class EmailChannel: ChannelBase
    {
        private readonly EmailConfig _config;

        public EmailChannel(EmailConfig config)
        {
            _config = config;
        }

        public override async Task<bool> IsOnline()
        {
            try
            {
                using var client = new SmtpClient();
                await client.ConnectAsync(_config.SmtpHost, _config.SmtpPort, _config.SmtpSSL);
                await client.AuthenticateAsync(_config.SmtpUser, _config.SmtpPassword);
                await client.DisconnectAsync(true);
                return true;
            }
            catch (Exception _)
            {
                return false;
            }
        }

        protected override async Task<DeliveryReport> InternalSend(string toAddress, string toName,
            string subject, string body, bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string> properties = null, 
            CancellationToken ct = default)
        {
            // try
            // {
                
            var message = new MimeMessage();
            SetRecipients(message, toAddress, toName);
            message.Subject = subject;
            
            var bb = new BodyBuilder();
            
            if (isHtml)
            {
                bb.HtmlBody = body;
                bb.TextBody = Uglify.HtmlToText(body).Code;
            }
            else
            {
                bb.TextBody = body;
            }

            if (attachments != null)
            {
                foreach (var atch in attachments)
                {
                    bb.Attachments.Add(atch.Key, atch.Value);
                }
            }
            
            message.Body = bb.ToMessageBody();
            message.Sender = new MailboxAddress(_config.SmtpUser, _config.SmtpUser);
            
            using var client = new SmtpClient();

            await client.ConnectAsync(_config.SmtpHost, _config.SmtpPort,
                _config.SmtpSSL, cancellationToken:ct);
            await client.AuthenticateAsync(_config.SmtpUser, _config.SmtpPassword, cancellationToken:ct);
            await client.SendAsync(message, cancellationToken: ct);
            await client.DisconnectAsync(true, cancellationToken:ct);
            // }
            // catch (Exception ex)
            // {
            //     // DEBUG STUFF ZHL-581 / ZHL-600
            //     // bypass my local error
            //     if (!ex.Message.Contains("non-local recipient verification failed"))
            //         await _eventLogRepository.RegisterEmailDebugEvent(edd, ex);
            // }
            
            return new DeliveryReport();
        }

        private void SetRecipients(MimeMessage message, string email, string recipientName, IEnumerable<string> extraEmails = null)
        {
            if (_config.DebugEmails?.Length > 0)
            {
                foreach (var m in _config.DebugEmails)
                {
                    message.To.Add(new MailboxAddress("Test Recipient", m.Trim()));
                }
            }
            else
            {
                // production workflow
                message.To.Add(new MailboxAddress (recipientName, email.Trim()));
                
                foreach (var em in extraEmails ?? new string[0])
                {
                    message.To.Add(new MailboxAddress(name: recipientName, em.Trim()));
                }
            }

        }

        public override void  UploadProperties(Dictionary<string, string> properties) { }
    }
}
