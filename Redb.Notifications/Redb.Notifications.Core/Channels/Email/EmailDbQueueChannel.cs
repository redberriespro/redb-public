using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Config;

namespace Redb.Notifications.Core.Channels.Email
{
    public class EmailDbQueueChannel: ChannelBase
    {
        private readonly EmailConfig _config;

        public EmailDbQueueChannel(EmailConfig config)
        {
            _config = config;
        }

        protected override Task<DeliveryReport> InternalSend(string toAddress, string toName, string subject, string body, bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string> properties = null,
            CancellationToken ct = default)
        {
            throw new NotImplementedException();
        }

        public override void  UploadProperties(Dictionary<string, string> properties) { }
    }
}
