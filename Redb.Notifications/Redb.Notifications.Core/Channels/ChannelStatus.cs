using System;

namespace Redb.Notifications.Core.Channels
{
    public class ChannelStatus
    {
        public bool IsConfigured { get; set; }
        public DateTime? LastOnlineCheck { get; set; }

        public DateTime? LastMessage { get; set; }
        public UInt64 MessagesSent { get; set; }
    }
}