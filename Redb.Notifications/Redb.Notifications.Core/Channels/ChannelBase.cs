using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Services;
using Redb.Telemetry.Api;

namespace Redb.Notifications.Core.Channels
{
    public abstract class ChannelBase: INotificationChannel
    {
        public const string REPORT_FEEDBACK_INFO = "telegramFeedbackInfo";

        private ChannelStatus _status { get; set; } = new ChannelStatus();
        
        private ConcurrentDictionary<string, ChannelStatus> _subchannelStatuses = new ConcurrentDictionary<string, ChannelStatus>();
        
        public virtual async Task<bool> IsOnline()
        {
            _status.LastOnlineCheck = DateTime.Now;
            return true;
        }

        public virtual bool IsSupportChannel => false;

        public virtual ChannelStatus GetStatus(string subChannel = null)
        {
            var r = new ChannelStatus
            {
                LastOnlineCheck = _status.LastOnlineCheck
                ,
                LastMessage = _status.LastMessage
            };

            if (subChannel != null && _subchannelStatuses.ContainsKey(subChannel))
            {
                r.LastMessage = _subchannelStatuses[subChannel].LastOnlineCheck;
            }

            return r;
        }

        public async Task<DeliveryReport> Send(string subchannel, string toAddress, string toName, string subject, string body, bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null, IReadOnlyDictionary<string, string> properties = null, 
            CancellationToken ct = default)
        {
            using var activity = Tracing.Activity.Source.StartActivity(this);

            var schName = subchannel;

            var report = await InternalSend(toAddress, toName, subject, body, isHtml, attachments, properties, ct:ct);
            report.FeedbackInfo = properties?.GetValueOrDefault(REPORT_FEEDBACK_INFO);
            
            var now = DateTime.Now;
            
            _status.LastMessage = now;
            _status.MessagesSent += 1;
            
            if (schName != null)
            {
                var schSt = _subchannelStatuses.GetOrAdd(subchannel, new ChannelStatus());
                schSt.LastMessage = now;
                schSt.MessagesSent += 1;
            }

            return report;
        }

        protected abstract Task<DeliveryReport> InternalSend(string toAddress, string toName,
            string subject, string body, bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string> property = null,
            CancellationToken ct = default);

        public abstract void UploadProperties(Dictionary<string, string> properties);

        public virtual void NotifyTechSupport(ContentSupportChannel content)
        {
            return;
        }
    }
}
