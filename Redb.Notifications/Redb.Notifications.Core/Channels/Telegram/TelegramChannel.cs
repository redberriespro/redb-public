#nullable enable
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Services;
using Redb.Telemetry.Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace Redb.Notifications.Core.Channels.Telegram
{
    public class TelegramChannel : ChannelBase
    {
        public const string KEY_ANSWER = "answer";
        public const string REPLY_TO_MESSAGE_ID = "replyToMessageId";
        public const string REPLY_TO_ENABLED = "replyToEnabled";
        public const string REPLY_TO_EXCEPTION_IS_PROCESSED = "replyToExceptionIsProcessed";
        public const string SENDING_STRATEGY = "sendingStrategy";

        public struct SendingStrategy
        {
            public const string DEFAULT = "default";
            public const string SEPARATE_MEDIA = "separateMedia";
        }

        private static readonly ILog _log = LogManager.GetLogger(typeof(TelegramChannel));

        private static readonly IReadOnlyCollection<string> ImageFileExtensions = new[]
        {
            ".jpg",
            ".jpeg",
            ".png",
            ".gif",
        };

        private static readonly IReadOnlyCollection<string> VideoFileExtensions = new[]
        {
            ".mp4",
            ".webm",
            ".avi",
            ".mov",
        };

        private readonly TelegramConfig _config;
        private readonly SupportChannelConfig _supportChannelConfig;
        private readonly TelegramBotClient _telegramBotClient;
        private readonly ChatType _typeChat;
        private readonly QueueTechSupport<string> _queueTechSupport;

        public TelegramChannel(TelegramConfig config, SupportChannelConfig supportChannelConfig)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
            _supportChannelConfig = supportChannelConfig;
            var httpClient = new System.Net.Http.HttpClient
            {
                Timeout = TimeSpan.FromSeconds(_config.Timeout)
            };
            _telegramBotClient = new TelegramBotClient(_config.Token, httpClient);

            try
            {
                var chat = _telegramBotClient.GetChatAsync(_config.ChatId).Result;
                _typeChat = chat.Type;
            }
            catch (System.Exception)
            {
                _typeChat = ChatType.Private;
            }

            _queueTechSupport = new QueueTechSupport<string>(
                _supportChannelConfig.Enabled,
                TELEGRAM_RATE_LIMIT_DELAY,
                _supportChannelConfig.MaxQueueSize, 
                SendMessage);
        }

        public override bool IsSupportChannel => _supportChannelConfig.Enabled;

        public override Task<bool> IsOnline()
        {
            return Task.FromResult(true); // todo implement
        }

        protected override async Task<DeliveryReport> InternalSend(string toAddress, string toName,
            string subject, string body, bool isHtml,
            IReadOnlyDictionary<string, byte[]>? attachments = null,
            IReadOnlyDictionary<string, string>? properties = null,
            CancellationToken ct = default)
        {
            using var activity = Tracing.Activity.Source.StartActivity(this);

            var report = new TelegramDeliveryReport();

            ParseMode? parseMode = isHtml ? (ParseMode?)ParseMode.Html : null;

            var keyboard = CreateKeyboard(properties);
            var prop = properties?.GetValueOrDefault(REPLY_TO_MESSAGE_ID);
            var replyToEnabled = bool.TryParse(properties?.GetValueOrDefault(REPLY_TO_ENABLED) ?? string.Empty, out bool replyTo)
                ? replyTo
                : false;
            int? replayMessageId = null;
            if (prop != null && replyToEnabled)
            {
                var strReplayMessageId = JObject.Parse(prop)[_config.ChatId.ToString()]?.ToString();
                replayMessageId = int.TryParse(strReplayMessageId ?? string.Empty, out int messageId)
                    ? (int?)messageId
                    : null;
            }
            // var strReplayMessageId = JObject.Parse(properties?.GetValueOrDefault(REPLY_TO_MESSAGE_ID)??string.Empty)[_config.ChatId.ToString()]?.ToString();


            var sendingStrategy = properties?.GetValueOrDefault(SENDING_STRATEGY) ?? string.Empty;
            try
            {
                if (attachments?.Any() ?? false)
                {
                    // видео и картинки
                    var mediaAttachments = attachments
                        .Where(a => ImageFileExtensions.Any(ext => a.Key.ToLower().EndsWith(ext)) || (sendingStrategy != SendingStrategy.SEPARATE_MEDIA && VideoFileExtensions.Any(ext => a.Key.ToLower().EndsWith(ext))))
                        .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

                    // всё остальное (документы)
                    var otherAttachments = attachments
                        .Where(a => !mediaAttachments.ContainsKey(a.Key))
                        .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

                    if (mediaAttachments.Any())
                    {
                        var reportMedia = await SendMediaGroupAsync(body, mediaAttachments, replayMessageId, parseMode, ct);
                        replayMessageId = InitMessageId(replayMessageId, replyToEnabled, reportMedia);
                        report.Include(reportMedia);
                    }

                    if (otherAttachments.Any())
                    {
                        var reportDoc = await SendDocumentGroupAsync(body, otherAttachments, keyboard, replayMessageId, parseMode, ct);
                        replayMessageId = InitMessageId(replayMessageId, replyToEnabled, reportDoc);
                        report.Include(reportDoc);
                    }
                }
                else
                {
                    var reportText = await SendTextMessageAsync(body, keyboard, replayMessageId, parseMode, ct);
                    replayMessageId = InitMessageId(replayMessageId, replyToEnabled, reportText);
                    report.Include(reportText);
                }
            }
            catch (ApiRequestException ex) when (
                ex.ErrorCode == (int)HttpStatusCode.BadRequest
                && ex.Message.Contains("message to be replied not found"))
            {
                // Проверить что уже отрабатывали эту ошибку
                if (bool.TryParse(properties?.GetValueOrDefault(REPLY_TO_EXCEPTION_IS_PROCESSED) ?? string.Empty, out bool isProcessed) && isProcessed)
                {
                    throw ex;
                }
                _log.Warn($"Failed to send telegram-notifications. Reason: {ex.Message}");
                var copyProperties = properties?.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                copyProperties?.Remove(REPLY_TO_MESSAGE_ID);
                copyProperties?.Remove(REPLY_TO_ENABLED);
                copyProperties?.Add(REPLY_TO_EXCEPTION_IS_PROCESSED, "true");
                return await InternalSend(toAddress, toName, subject, body, isHtml, attachments, copyProperties, ct);
            }
            // Обработка ошибки обработки изображений: отправляем всё как вложения, без проверок
            catch (ApiRequestException ex) when (
                ex.ErrorCode == (int)HttpStatusCode.BadRequest
                && ex.Message.Contains("IMAGE_PROCESS_FAILED")
                && attachments != null && attachments.Count > 0)
            {
                var base64Body = ConvertAttachmentsToBase64(attachments);
                var msg = $"Failed to send telegram-notifications for next images: {string.Join("; ", base64Body.Select(x => $"{x.Key}: {x.Value}"))})";

                activity?.SetErrorStatus(msg);
                activity?.RecordException(ex);
                var bodyWithError = body + "\nВложение невозможно отправить";
                var reportText = await SendTextMessageAsync(bodyWithError, keyboard, replayMessageId, parseMode, ct);
                replayMessageId = InitMessageId(replayMessageId, replyToEnabled, reportText);
                report.Include(reportText);
            }
            // Обработка ошибки превышения лимитов запросов: ждем и пробуем еще раз
            catch (ApiRequestException ex) when (ex.ErrorCode == (int)HttpStatusCode.TooManyRequests)
            {
                activity?.SetErrorStatus(ex);
                activity?.RecordException(ex);


                await Task.Delay(ex.Parameters?.RetryAfter ?? _config.RequestDelay);
                var reportInternal = await InternalSend(toAddress, toName, subject, body, isHtml, attachments, properties, ct: ct);
                report.Include((TelegramDeliveryReport)reportInternal);
            }
            catch (ApiRequestException ex)
            {
                activity?.SetErrorStatus(ex);
                activity?.RecordException(ex);

                if (ex.Parameters?.MigrateToChatId != null)
                {
                    ex.Data.Add(nameof(ResponseParameters.MigrateToChatId), ex.Parameters.MigrateToChatId);
                }

                if (ex.Parameters?.RetryAfter != null)
                {
                    ex.Data.Add(nameof(ResponseParameters.RetryAfter), ex.Parameters.RetryAfter);
                }
                throw;
            }

            return report;
        }

        private static int? InitMessageId(int? replayMessageId, bool replyToEnabled, TelegramDeliveryReport reportMedia)
        {
            if (replyToEnabled && !replayMessageId.HasValue) replayMessageId = reportMedia.MessageReports.FirstOrDefault()?.MessageId;
            return replayMessageId;
        }

        private const int TELEGRAM_RATE_LIMIT_DELAY = 1000;

        private async Task<TelegramDeliveryReport> SendMediaGroupAsync(string body,
                                                                       IReadOnlyDictionary<string, byte[]> attachments,
                                                                       int? replayMessageId,
                                                                       ParseMode? parseMode,
                                                                       CancellationToken cancellationToken)
        {
            var streams = new List<Stream>();
            var medias = new List<IAlbumInputMedia>();
            List<TelegramDeliveryReport> reports = new List<TelegramDeliveryReport>();

            try
            {
                bool isFirstMedia = true;
                var messages = SplitMessageWords(body);
                var caption = messages.FirstOrDefault();
                foreach (var attachment in attachments)
                {
                    if (attachment.Value == null || attachment.Value.Length == 0)
                    {
                        _log.Warn($"Attachment media '{attachment.Key}' is empty. Skipping.  Body is '{body}'");
                        var telInfo = await SendTextMessageAsync(
                            body,
                            keyboard: null,
                            replayMessageId: replayMessageId,
                            parseMode: parseMode,
                            ct: cancellationToken);
                        await Task.Delay(TELEGRAM_RATE_LIMIT_DELAY, cancellationToken); // delay to avoid telegram rate limit, 
                        reports.Add(telInfo);
                        continue;
                    }

                    var ms = new MemoryStream(attachment.Value);
                    streams.Add(ms);

                    // var caption = TruncatingMessage(body);
                    try
                    {
                        if (!isFirstMedia)
                            caption = null;
                        isFirstMedia = false;

                        var media = CreateMediaFromAttachment(parseMode, attachment, ms, caption);
                        medias.Add(media);
                    }
                    catch (System.Exception ex)
                    {
                        _log.Warn($"Failed to create media for '{attachment.Key}'", ex);
                        var telInfo = await SendTextMessageAsync(
                            body,
                            keyboard: null,
                            replayMessageId: replayMessageId,
                            parseMode: parseMode,
                            ct: cancellationToken);
                        await Task.Delay(TELEGRAM_RATE_LIMIT_DELAY, cancellationToken); // delay to avoid telegram rate limit, 
                        reports.Add(telInfo);
                    }
                }

                if (!medias.Any())
                {
                    return new TelegramDeliveryReport(Array.Empty<Message>(), reports);
                }

                //  Разбиваем медиафайлы на группы по 10 или меньше
                var mediaGroups = medias.Select((media, index) => new { media, index })
                                        .GroupBy(x => x.index / 10)
                                        .Select(g => g.Select(x => x.media).ToList())
                                        .ToList();

                var replayId = replayMessageId;
                List<Message> messageInfo = new List<Message>();
                foreach (var mediaGroup in mediaGroups)
                {
                    var groupMessageInfo = await _telegramBotClient.SendMediaGroupAsync(
                                                _config.ChatId,
                                                mediaGroup,
                                                allowSendingWithoutReply: false,
                                                replyToMessageId: replayId,
                                                cancellationToken: cancellationToken);

                    messageInfo.AddRange(groupMessageInfo);
                    replayId = groupMessageInfo.FirstOrDefault()?.MessageId;
                    await Task.Delay(TELEGRAM_RATE_LIMIT_DELAY, cancellationToken); // delay to avoid telegram rate limit
                }

                foreach (var message in messages.Skip(1))
                {
                    var telInfo = await SendTextMessageAsync(
                        message,
                        keyboard: null,
                        replayMessageId: replayId,
                        parseMode: parseMode,
                        ct: cancellationToken);
                    await Task.Delay(TELEGRAM_RATE_LIMIT_DELAY, cancellationToken); // delay to avoid telegram rate limit, 
                    reports.Add(telInfo);
                    replayId = telInfo.MessageReports.FirstOrDefault()?.MessageId;
                }

                return new TelegramDeliveryReport(messageInfo, reports);
            }
            finally
            {
                streams.ForEach(ms => ms.Dispose());
            }
        }

        private static IAlbumInputMedia CreateMediaFromAttachment(ParseMode? parseMode, KeyValuePair<string, byte[]> attachment, MemoryStream ms, string? caption)
        {
            if (ImageFileExtensions.Any(ext => attachment.Key.ToLower().EndsWith(ext)))
            {
                return new InputMediaPhoto(new InputMedia(ms, attachment.Key))
                {
                    Caption = caption,
                    ParseMode = parseMode
                };
            }
            else if (VideoFileExtensions.Any(ext => attachment.Key.ToLower().EndsWith(ext)))
            {
                return new InputMediaVideo(new InputMedia(ms, attachment.Key))
                {
                    Caption = caption,
                    ParseMode = parseMode,
                    SupportsStreaming = true
                };
            }

            throw new NotSupportedException($"Attachment '{attachment.Key}' is not supported as album media attachment");
        }

        /// <summary>
        /// Максимальная длина сообщения
        /// </summary>
        private const int maxLengthMessage = 1024;

        /// <summary>
        /// Режет сообщение до максимальной длины сообщения
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        private static string TruncatingMessage(string body)
        {
            var caption = body;
            if (caption.Length > maxLengthMessage)
            {
                _log.Warn($"Caption is too long: {caption.Length} characters. Truncating to {maxLengthMessage} characters.");

                var notTruncatedCaption = "... (Сообщение слишком длинное и доставлено не полностью)";
                caption = caption.Substring(0, maxLengthMessage - notTruncatedCaption.Length) + notTruncatedCaption;
            }

            return caption;
        }

        private async Task<TelegramDeliveryReport> SendDocumentGroupAsync(
            string body,
            IReadOnlyDictionary<string, byte[]>? attachments,
            ReplyKeyboardMarkup? keyboard,
            int? replayMessageId,
            ParseMode? parseMode,
            CancellationToken ct)
        {
            List<Message> messages = new List<Message>();
            List<TelegramDeliveryReport> reports = new List<TelegramDeliveryReport>();
            if (attachments != null)
                foreach (var attachment in attachments)
                {
                    if (attachment.Value == null || attachment.Value.Length == 0)
                    {
                        _log.Warn($"Attachment Document '{attachment.Key}' is empty. Skipping.  Body is '{body}'");
                        var telInfo = await SendTextMessageAsync(body, keyboard, replayMessageId, parseMode, ct);
                        await Task.Delay(TELEGRAM_RATE_LIMIT_DELAY, ct); // delay to avoid telegram rate limit
                        reports.Add(telInfo);
                        continue;
                    }

                    using var ms = new MemoryStream(attachment.Value);
                    var file = new InputOnlineFile(ms, attachment.Key);

                    var messageInfo = await _telegramBotClient.SendDocumentAsync(
                        chatId: _config.ChatId,
                        document: file,
                        caption: TruncatingMessage(body),
                        allowSendingWithoutReply: false,
                        replyMarkup: _typeChat == ChatType.Channel ? null : keyboard,
                        replyToMessageId: replayMessageId,
                        parseMode: parseMode,
                        cancellationToken: ct
                    );

                    messages.Add(messageInfo);
                }

            return new TelegramDeliveryReport(messages, reports);
        }

        public class TelegramAnswerConfig
        {
            public string[] Default { get; set; }
            public int NumButtonsInRow { get; set; }
        }

        private async Task<TelegramDeliveryReport> SendTextMessageAsync(
            string body,
            ReplyKeyboardMarkup? keyboard,
            int? replayMessageId,
            ParseMode? parseMode,
            CancellationToken ct)
        {
            var messageInfo = await _telegramBotClient.SendTextMessageAsync(
                _config.ChatId,
                body,
                replyMarkup: keyboard,
                replyToMessageId: replayMessageId,
                parseMode: parseMode,
                cancellationToken: ct);

            return new TelegramDeliveryReport(messageInfo);

        }

        private ReplyKeyboardMarkup? CreateKeyboard(IReadOnlyDictionary<string, string>? properties)
        {
            string[] keyboard = Array.Empty<string>();
            if (properties != null
                && properties.TryGetValue(KEY_ANSWER, out var answer)
                && answer != null
                && JsonConvert.DeserializeObject<TelegramAnswerConfig>(answer) is TelegramAnswerConfig keyboardData
                && keyboardData?.Default != null && keyboardData.Default.Any())
            {
                keyboard = keyboardData.Default;
            }
            else
            {
                if (_config.Answer?.Default == null) return null;

                keyboard = _config.Answer.Default;
            }

            int numButtonsInRow = _config.Answer?.NumButtonsInRow ?? 3; // максимальное количество кнопок в сроке

            return new ReplyKeyboardMarkup(keyboard
                        .Select((b, i) => new { Button = new KeyboardButton(b), Index = i })
                        .GroupBy(x => x.Index / numButtonsInRow)
                        .Select(g => g.Select(x => x.Button).ToArray())
                        .ToArray())
            { ResizeKeyboard = true };
        }

        public async Task SendPhotoWithReplyButtonsAsync(long chatId, InputOnlineFile photo, string? caption, ReplyKeyboardMarkup? keyboard, CancellationToken ct)
        {
            await _telegramBotClient.SendPhotoAsync(
                chatId: chatId,
                photo: photo,
                caption: caption,
                allowSendingWithoutReply: false,
                replyMarkup: _typeChat == ChatType.Channel ? null : keyboard, // replyMarkup is not supported for channels
                cancellationToken: ct
            );
        }

        private async Task SendDocumentWithReplyButtonsAsync(long chatId, InputOnlineFile document, string? caption, ReplyKeyboardMarkup? keyboard, CancellationToken ct)
        {
            await _telegramBotClient.SendDocumentAsync(
                chatId: chatId,
                document: document,
                caption: caption,
                allowSendingWithoutReply: false,
                replyMarkup: _typeChat == ChatType.Channel ? null : keyboard, // replyMarkup is not supported for channels
                cancellationToken: ct
            );
        }

        public async Task RemoveReplyButtonsAsync(string chatId, CancellationToken ct = default)
        {
            var replyMarkup = new ReplyKeyboardRemove();

            await _telegramBotClient.SendTextMessageAsync(chatId, "Buttons removed", replyMarkup: replyMarkup, cancellationToken: ct);
        }

        public override void UploadProperties(Dictionary<string, string> properties)
        {
            AddProperties(properties, _config);
        }

        internal static void AddProperties(Dictionary<string, string> properties, TelegramConfig config)
        {
            if (config?.Answer != null)
            {
                properties[KEY_ANSWER] = JsonConvert.SerializeObject(config.Answer);
            }
        }
        private IReadOnlyDictionary<string, string>? ConvertAttachmentsToBase64(IReadOnlyDictionary<string, byte[]>? attachments)
        {
            if (attachments == null || !attachments.Any())
            {
                return null;
            }

            return attachments.ToDictionary(
                kvp => kvp.Key,
                kvp => Convert.ToBase64String(kvp.Value)
            );
        }

        public static string[] SplitMessageWords(string? message, int chunkSize = 1024)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                return Array.Empty<string>();
            }

            List<string> messageChunks = new List<string>();
            int start = 0;

            while (start < message.Length)
            {
                // Находим индекс, где чанк должен закончиться
                int chunkEnd = Math.Min(start + chunkSize, message.Length);

                // Если чанк заканчивается не на конце слова, нужно откатиться назад
                if (chunkEnd < message.Length && !char.IsWhiteSpace(message[chunkEnd]))
                {
                    // Откатываемся назад, пока не найдем пробел или начало строки
                    while (chunkEnd > start && !char.IsWhiteSpace(message[chunkEnd]))
                    {
                        chunkEnd--;
                    }

                    // Если они ровны, то в чанке нет пробелов, и мы просто его обрежем без пробела
                    if (chunkEnd == start)
                    {
                        chunkEnd = Math.Min(start + chunkSize, message.Length);
                    }
                }

                // Создаем чанк и добавляем его в список
                string chunk = message.Substring(start, chunkEnd - start).Trim();
                messageChunks.Add(chunk);

                // Устанавливаем новый стартовый индекс
                start = chunkEnd;
            }

            return messageChunks.ToArray();
        }

        public override void NotifyTechSupport(ContentSupportChannel content)
        {
            var isSend = content.Type switch
            {
                TypeNotifySupCh.MessageSaveToDropout => _supportChannelConfig.NotifyMessageSaveToDropout,
                TypeNotifySupCh.FailedProcessMessage => _supportChannelConfig.NotifyFailedProcessMessage,
                TypeNotifySupCh.FailedProcessMessageAndWillRetry => _supportChannelConfig.NotifyFailedProcessMessageAndWillRetry,
                TypeNotifySupCh.Default => true,
                _ => true
            };

            if (!isSend)
            {
                return;
            }
            
            _queueTechSupport.AddMessage(content.Message);
            return;
        }
        
        public async Task SendMessage(string message, CancellationToken ct)
        {
            await _telegramBotClient.SendTextMessageAsync(
                _config.ChatId,
                message,
                cancellationToken: ct);
        }
    }
}