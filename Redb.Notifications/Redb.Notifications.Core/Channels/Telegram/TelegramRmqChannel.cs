using System;
using System.Collections.Generic;
using System.Linq;
using Redb.Notifications.Core.Channels.Telegram;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Providers;

namespace Redb.Notifications.Core.Channels
{
    internal class TelegramRmqChannel : RmqChannelBase
    {
        private readonly string _channelName;
        private readonly TelegramConfig _config;

        public TelegramRmqChannel(
            INotificationsQueueingProvider notificationsQueueingProvide,
            string channelName,
            IChannelConfig config)
            : base(notificationsQueueingProvide)
        {
            _channelName = channelName;
            _config = config.Telegram;
        }

        protected internal override string GetChannelName()
        {
            return _channelName;
        }

        public override void UploadProperties(Dictionary<string, string> properties)
        {
            TelegramChannel.AddProperties(properties, _config);
        }
    }
}
