using System;
using System.Threading;
using System.Threading.Tasks;

namespace Redb.Notifications.Core.Channels.Sms.Providers
{
    public interface ISmsProvider
    {
        Task<bool> SendSms(string toAddr, string body, CancellationToken ct = default);
    }
}
