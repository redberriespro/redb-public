using System;
using System.Globalization;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using log4net;

namespace Redb.Notifications.Core.Channels.Sms.Providers
{
    public class TargetSmsProvider: ISmsProvider
    {
        private ILog _log = LogManager.GetLogger(typeof(TargetSmsProvider));
        private readonly string _user;
        private readonly string _password;
        private readonly string _sourceAddr;
        private static readonly HttpClient _httpClient = new HttpClient();

        public TargetSmsProvider(string user, string password, string sourceAddr)
        {
            _user = user;
            _password = password;
            _sourceAddr = sourceAddr;
        }

        public async Task<bool> SendSms(string toAddr, string body, 
            CancellationToken ct = default)
        {
            var url =
                $"https://sms.targetsms.ru/sendsms.php?user={HttpUtility.UrlEncode(_user)}&pwd={HttpUtility.UrlEncode(_password)}&dadr={HttpUtility.UrlEncode(toAddr)}&text={HttpUtility.UrlEncode(body)}&sadr={HttpUtility.UrlEncode(_sourceAddr)}";
 
            var qrResp = await _httpClient.GetAsync(url, cancellationToken:ct);

            var resText = await qrResp.Content.ReadAsStringAsync();
            
            
            if (!qrResp.IsSuccessStatusCode) return false;

            // при успехе, оно возвращет номер
            if (!Int64.TryParse(resText, NumberStyles.Integer, CultureInfo.InvariantCulture, out long _))
                return false;
            
            _log.Debug($"SMS sent to {toAddr}, {body}, res {resText}");
            
            return true;
        }
    }
}
