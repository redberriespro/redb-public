using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Channels.Sms.Providers;
using Redb.Notifications.Core.Config;

namespace Redb.Notifications.Core.Channels.Sms
{
    public class SmsChannel: ChannelBase
    {
        private readonly SmsConfig _config;
        private readonly ISmsProvider _provider;

        public SmsChannel(SmsConfig config)
        {
            _config = config;

            switch (config.Provider?.ToLower())
            {
                case "targetsms": {
                    _provider = new TargetSmsProvider(config.Login, config.Password, config.SourceAddress); 
                } break;

                default: throw new NotException("SMS provider not found: " + config.Provider);
            };
        }

        protected override async Task<DeliveryReport> InternalSend(string toAddress, string toName,
            string subject, string body, bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null, 
            IReadOnlyDictionary<string, string>? properties = null,
            CancellationToken ct = default)
        {
            var res = await _provider.SendSms(toAddress, body, ct:ct);
            if (!res) throw new NotException("Unable to send sms");

            return new DeliveryReport();
        }
        
        public override void  UploadProperties(Dictionary<string, string> properties) { }
    }
}
