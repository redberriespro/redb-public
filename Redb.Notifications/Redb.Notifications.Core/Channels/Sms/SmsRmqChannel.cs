using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Providers;

namespace Redb.Notifications.Core.Channels
{
    internal class SmsRmqChannel : RmqChannelBase
    {
        private readonly string _channelName;
        private readonly SmsConfig _config;

        public SmsRmqChannel(
            INotificationsQueueingProvider notificationsQueueingProvide,
            string channelName,
            IChannelConfig config)
            : base(notificationsQueueingProvide)
        {
            _channelName = channelName;
            _config = config.Sms;
        }

        protected internal override string GetChannelName()
        {
            return _channelName;
        }

        public override void UploadProperties(Dictionary<string, string> properties) { }

    }
}
