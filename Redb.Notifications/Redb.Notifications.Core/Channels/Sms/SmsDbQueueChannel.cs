using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Config;

namespace Redb.Notifications.Core.Channels.Sms
{
    public class SmsDbQueueChannel: ChannelBase
    {
        private readonly SmsConfig _config;

        public SmsDbQueueChannel(SmsConfig config)
        {
            _config = config;
        }

        protected override Task<DeliveryReport> InternalSend(string toAddress, string toName, string subject, string body, bool isHtml,
            IReadOnlyDictionary<string, byte[]> attachments = null,
            IReadOnlyDictionary<string, string>? properties = null,
            CancellationToken ct = default)
        {
            throw new NotImplementedException();
        }

        public override void  UploadProperties(Dictionary<string, string> properties) { }
    }
}
