using System;
using System.Runtime.Serialization;

namespace Redb.Notifications.Core
{
    public class NotException: ApplicationException
    {
        public NotException()
        {
        }

        protected NotException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public NotException(string? message) : base(message)
        {
        }

        public NotException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}