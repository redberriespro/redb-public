using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Redb.Notifications.Core.Entities;

namespace Redb.Notifications.Core.Repositories
{
    public class NotificationTemplateRepository : INotificationTemplateRepository
    {
        private readonly INotificationDbContextFactory _contextFactory;

        public NotificationTemplateRepository(INotificationDbContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<List<NotificationTemplateEntity>> GetAll()
        {
            await using var ctx = _contextFactory.CreateContext();
            return await ctx.NotificationTemplates.ToListAsync();
        }

        public async Task<NotificationTemplateEntity> GetById(string id)
        {
            await using var ctx = _contextFactory.CreateContext();
            return await ctx
                .NotificationTemplates
                .SingleOrDefaultAsync(e => e.NotificationTemplateId == id);
        }
        
        public async Task<NotificationTemplateEntity> GetCompiledById(string id)
        {
            // todo review and possibly get rid of
            var tpl = await GetById(id);

            if (tpl == null) return null;
            
            return new NotificationTemplateEntity
            {
                Body = tpl.Body,
                NotificationTemplateId = tpl.NotificationTemplateId,
                IsHtml = tpl.IsHtml,
                MasterTemplateId = tpl.MasterTemplateId,
                Subject = tpl.Subject
            };
        }

        public async Task UpdateNotificationTemplate(NotificationTemplateEntity entity)
        {
            await using var ctx = _contextFactory.CreateContext();

            var dbe = await ctx.NotificationTemplates.FindAsync(entity.NotificationTemplateId);
            ctx.Entry(dbe).CurrentValues.SetValues(entity);
            await ctx.SaveChangesAsync();
        }

        public async Task  CreateNotificationTemplate(NotificationTemplateEntity et)
        {
            await using var ctx = _contextFactory.CreateContext();

            await ctx.NotificationTemplates.AddAsync(et);
            await ctx.SaveChangesAsync();
        }
        
        public async Task EnsureNotificationTemplate(string id, string subject, string body, bool isHtml, string masterTemplate=null)
        {
            var t = await GetById(id);
            if (t != null)
            {
                // do not update the template in the db even it it's got changed here
                //     t.Body = body;
                //     t.Subject = subject;
                //     t.MasterTemplateId = masterTemplate;
                //     t.IsHtml = isHtml;
                //
                //     await UpdateNotificationTemplate(t);
                return;
            };

            await CreateNotificationTemplate(new NotificationTemplateEntity
            { 
                NotificationTemplateId = id,
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                MasterTemplateId = masterTemplate
            });
        }
    }
}