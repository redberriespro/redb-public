using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Redb.Notifications.Core.Entities;
using Redb.Notifications.Core.Repositories;

namespace Redb.Notifications.Core.Services
{
    public class NotificationTemplateRepositoryStub: INotificationTemplateRepository
    {
        public Dictionary<string, NotificationTemplateEntity> Templates =
            new Dictionary<string, NotificationTemplateEntity>();

        public async Task<List<NotificationTemplateEntity>> GetAll()
        {
            return Templates.Values.ToList();
        }

        public async Task<NotificationTemplateEntity> GetById(string id)
        {
            return Templates[id];
        }

        public async Task<NotificationTemplateEntity> GetCompiledById(string id)
        {
            return Templates[id];
        }

        public async Task UpdateNotificationTemplate(NotificationTemplateEntity entity)
        {
            Templates[entity.NotificationTemplateId] = entity;
        }

        public async Task CreateNotificationTemplate(NotificationTemplateEntity et)
        {
            Templates[et.NotificationTemplateId] = et;
        }

        public async Task EnsureNotificationTemplate(string id, string subject, string body, bool isHtml, string masterTemplate = null)
        {
            if (Templates.ContainsKey(id)) return;

            Templates[id] = new NotificationTemplateEntity
            {
                NotificationTemplateId = id,
                Body = body,
                IsHtml = isHtml,
                MasterTemplateId = masterTemplate
            };
        }
    }
}