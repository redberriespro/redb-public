using System.Collections.Generic;
using System.Threading.Tasks;
using Redb.Notifications.Core.Entities;

namespace Redb.Notifications.Core.Repositories
{
    public interface INotificationTemplateRepository
    {
            Task<List<NotificationTemplateEntity>> GetAll();

            Task<NotificationTemplateEntity> GetById(string id);
            Task<NotificationTemplateEntity> GetCompiledById(string id);

            Task UpdateNotificationTemplate(NotificationTemplateEntity entity);

            Task CreateNotificationTemplate(NotificationTemplateEntity et);

            Task EnsureNotificationTemplate(string id, string subject, string body, bool isHtml, string masterTemplate=null);
    }
}