using Microsoft.Extensions.Caching.Memory;
using Redb.Notifications.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Redb.Notifications.Core.Repositories
{
    public class CachingNotificationTemplateRepository : INotificationTemplateRepository
    {
        private static readonly TimeSpan MemoryCacheExpiration = TimeSpan.FromMinutes(5);

        private readonly INotificationTemplateRepository _repository;
        private readonly IMemoryCache _memoryCache;

        public CachingNotificationTemplateRepository(
            INotificationTemplateRepository repository,
            IMemoryCache memoryCache)
        {
            _repository = repository;
            _memoryCache = memoryCache;
        }

        public async Task<List<NotificationTemplateEntity>> GetAll()
        {
            var cacheKey = "NotificationTemplate_GetAll";
            var emailTemplates = await _memoryCache.GetOrCreateAsync(cacheKey, async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = MemoryCacheExpiration;

                return await _repository.GetAll();
            });

            return emailTemplates;
        }

        public async Task<NotificationTemplateEntity> GetById(string id)
        {
            EnsureArgumentIsNotNull(id, nameof(id));

            var cacheKey = $"NotificationTemplate_{id}";
            var emailTemplate = await _memoryCache.GetOrCreateAsync(cacheKey, async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = MemoryCacheExpiration;

                return await _repository.GetById(id);
            });

            return emailTemplate;
        }

        public async Task<NotificationTemplateEntity> GetCompiledById(string id)
        {
            EnsureArgumentIsNotNull(id, nameof(id));

            var cacheKey = $"NotificationTemplate_{id}_tpl";
            var emailTemplate = await _memoryCache.GetOrCreateAsync(cacheKey, async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = MemoryCacheExpiration;

                return await _repository.GetCompiledById(id);
            });

            return emailTemplate;
        }

        public async Task UpdateNotificationTemplate(NotificationTemplateEntity entity)
        {
            EnsureArgumentIsNotNull(entity, nameof(entity));

            await _repository.UpdateNotificationTemplate(entity);

            InvalidateNotificationTemplate(entity.NotificationTemplateId);

            var cacheKey = "NotificationTemplate_GetAll";
            _memoryCache.Remove(cacheKey);
        }

        private void InvalidateNotificationTemplate(string id)
        {
            EnsureArgumentIsNotNull(id, nameof(id));

            var cacheKey = $"NotificationTemplate_{id}";
            _memoryCache.Remove(cacheKey);

            cacheKey = $"NotificationTemplate_{id}_tpl";
            _memoryCache.Remove(cacheKey);
        }

        public async Task CreateNotificationTemplate(NotificationTemplateEntity entity)
        {
            EnsureArgumentIsNotNull(entity, nameof(entity));

            await _repository.CreateNotificationTemplate(entity);

            var cacheKey = $"NotificationTemplate_GetAll";
            _memoryCache.Remove(cacheKey);
        }

        public async Task EnsureNotificationTemplate(string id, string subject, string body, bool isHtml, string masterTemplate = null)
        {
            await _repository.EnsureNotificationTemplate(id, subject, body, isHtml, masterTemplate);

            var cacheKey = $"NotificationTemplate_GetAll";
            _memoryCache.Remove(cacheKey);

            InvalidateNotificationTemplate(id);
        }

        private void EnsureArgumentIsNotNull<T>(T argument, string argumentName)
        {
            if (argument is null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }
    }
}