﻿using System.Collections.Generic;
using Stubble.Core;
using Stubble.Core.Builders;

namespace Redb.Notifications.Core.Templates
{
    public class TemplateRenderer : ITemplateRenderer
    {
        private readonly StubbleVisitorRenderer Stubble = new StubbleBuilder().Build();
        
        public string ApplyTemplateParams(string tplBody, IReadOnlyDictionary<string,object> parameters)
        {
            var body = tplBody;

            foreach (var p in parameters)
            {
                body = body.Replace($"[{p.Key}]", p.Value.ToString());
            }

            return body;
        }

        public string RenderTemplate(string tplBody, IReadOnlyDictionary<string,object> parameters)
        {
            return Stubble.Render(tplBody, parameters);
        }
        
        public string RenderTemplateWithMaster(string tplMaster, string tpl, IReadOnlyDictionary<string,object> parameters)
        {
            var body = Stubble.Render(tpl, parameters);
            var masterDict = new Dictionary<string, object>(parameters) {["body"] = body};
            return Stubble.Render(tplMaster, masterDict);
        }
    }


    public interface ITemplateRenderer
    {
        string ApplyTemplateParams(string tplBody, IReadOnlyDictionary<string, object> parameters);

        string RenderTemplate(string tplBody, IReadOnlyDictionary<string, object> parameters);

        string RenderTemplateWithMaster(string tplMaster, string tpl, IReadOnlyDictionary<string, object> parameters);
    }
}