﻿namespace Redb.Notifications.RMQHost.Telemetry
{
    public static class Tags
    {
        public static class Channel {
            public const string Name = "redb.notifications.channel.name";
        }
    }
}
