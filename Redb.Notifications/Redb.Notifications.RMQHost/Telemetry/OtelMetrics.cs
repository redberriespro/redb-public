using System.Diagnostics.Metrics;

namespace Redb.Notifications.RMQHost.Telemetry;

public interface IOtelMetrics
{
    public const string METRIC_NAME = "Notification.RmqHost";
    void IncNotify(string channelName, TimeSpan notifyDelay, int size);
}

internal class OtelMetrics : IOtelMetrics
{
    private Counter<int> NotificationCounter { get; }

    private ObservableGauge<long> NotificationDelayGauge { get; }
    private readonly Queue<Measurement<long>> _notificationDelays = new(100);

    private ObservableGauge<int> NotifyBytesPackageGauge { get; }
    private readonly Queue<Measurement<int>> _packageBytes = new(100);

    public string MetricName { get; }

    public OtelMetrics(string meterName = IOtelMetrics.METRIC_NAME)
    {
        var meter = new Meter(meterName);
        MetricName = meterName;

        NotificationDelayGauge = meter.CreateObservableGauge(
            "notification_rmq_delay",
            () => GetNotificationDelay(),
            unit: "ns",
            "временная задержка между регистрацией и фактической отправкой уведомления");
        NotificationCounter = meter.CreateCounter<int>(
            "notification_rmq_count",
            description: "счетчик уведомлений");

        NotifyBytesPackageGauge = meter.CreateObservableGauge(
            "notification_package_bytes", 
            () => GetRecordBytes(), 
            unit: "by", 
            "Размер сообщения отправленного через Notification.RmqHost");
        
    }

    #region notification_delay
    public IEnumerable<Measurement<long>> GetNotificationDelay()
    {
        var result = _notificationDelays.ToArray();
        _notificationDelays.Clear();

        return result;
    }

    public IEnumerable<Measurement<int>> GetRecordBytes()
    {
        var result = _packageBytes.ToArray();
        _packageBytes.Clear();

        return result;
    }

    public void IncNotify(string channelName, TimeSpan notifyDelay, int size)
    {
        NotificationCounter.Add(1, new KeyValuePair<string, object>("channel_name", channelName));
        _notificationDelays.Enqueue(new Measurement<long>(notifyDelay.Ticks * 100, new KeyValuePair<string, object>("channel_name", channelName)));
        _packageBytes.Enqueue(new Measurement<int>(size, new KeyValuePair<string, object>("channel_name", channelName)));
    }
    #endregion
}
