﻿using OpenTelemetry.Metrics;
using Redb.Notifications.RMQHost.Configs;
using Redb.Telemetry.Hosting;
using System.Reflection;

namespace Redb.Notifications.RMQHost.Telemetry
{
    internal static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddTelemetry(this IServiceCollection services, TelemetryConfig config)
        {
            if (!string.IsNullOrWhiteSpace(config?.CollectorEndpoint))
            {
                var telemetryOptions = new TelemetryOptions(Assembly.GetEntryAssembly(), config.CollectorEndpoint)
                {
                    TracingSources = config.TracingSources,
                    Meters = config.Meters,
                    EventSources = config.EventSources,
                    LogLevel = config.LogLevel,
                    Debug = config.Debug
                };

                services
                    .AddTracing(telemetryOptions)
                    .AddMetrics(telemetryOptions);
            }

            return services;
        }
    }
}
