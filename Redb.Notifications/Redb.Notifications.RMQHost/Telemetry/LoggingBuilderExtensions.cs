﻿using Redb.Notifications.RMQHost.Configs;
using Redb.Telemetry.Hosting;
using Redb.Telemetry.Log4Net;
using System.Reflection;

namespace Redb.Notifications.RMQHost.Telemetry
{
    public static class LoggingBuilderExtensions
    {
        public static ILoggingBuilder AddOpenTelemetryAppender(this ILoggingBuilder builder, TelemetryConfig config)
        {
            if(!string.IsNullOrWhiteSpace(config?.CollectorEndpoint))
            {
                var telemetryOptions = new TelemetryOptions(Assembly.GetEntryAssembly(), config.CollectorEndpoint)
                {
                    TracingSources = config.TracingSources,
                    Meters = config.Meters,
                    EventSources = config.EventSources,
                    LogLevel = config.LogLevel,
                    Debug = config.Debug
                };

                builder.AddOpenTelemetryAppender(telemetryOptions);
            }

            return builder;
        }
    }
}
