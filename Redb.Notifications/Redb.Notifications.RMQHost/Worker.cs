using log4net;
using Redb.Notifications.Core.Providers;
using Redb.Notifications.Core.Services;
using Redb.Notifications.RMQ.Config;
using Redb.Notifications.RMQ.NotificationBus;
using Redb.Notifications.RMQHost.Telemetry;
using Redb.Telemetry.Api;

namespace Redb.Notifications.RMQHost;

public class Worker : BackgroundService
{
    private readonly ILog _log = LogManager.GetLogger(typeof(Worker));

    private readonly INotifyRabbitMqServer _rmqServer;
    private readonly IChannelManager _channelManager;
    private readonly IRmqNotifications _notificationsConfig;
    private readonly IOtelMetrics _otelMetrics;

    public Worker(
        INotifyRabbitMqServer rmqServer,
        IChannelManager channelManager,
        IRmqNotifications notificationsConfig,
        IOtelMetrics otelMetrics
        )
    {
        _rmqServer = rmqServer;
        _channelManager = channelManager;
        _notificationsConfig = notificationsConfig;
        _otelMetrics = otelMetrics;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _log.Info("Start service");
        foreach (var channelConfig in _notificationsConfig.Channels)
        {
            _rmqServer.Start(
                new RmqMessageSender(
                    SendNotify,
                    channelConfig.Name,
                    NotifySupportChannel,
                    channelConfig.FeedbackExName));
        } 

        return Task.CompletedTask;
    }

    private async Task<byte[]> SendNotify(byte[] body, string channelName, CancellationToken ct)
    {
        using var activity = Tracing.Activity.Source.StartActivity(this);

        activity?.SetTag(Tags.Channel.Name, channelName);

        var result = await _channelManager.Send(body, ct);
        _otelMetrics.IncNotify(channelName, System.DateTime.UtcNow - result.TimeRegister, result.Size);
        return result.Pack();
    }

    private void NotifySupportChannel(ContentSupportChannel content)
    {
        _channelManager.NotifySupportChannel(content);
    }

    public override void Dispose()
    {
        _rmqServer.Dispose();
        base.Dispose();
    }
}
