using Autofac;
using Autofac.Extensions.DependencyInjection;
using log4net;
using log4net.Config;
using Redb.Notifications.RMQ.Config;
using Redb.Notifications.RMQHost;
using Redb.Notifications.RMQHost.Configs;
using Redb.Notifications.RMQHost.Telemetry;
using System.Reflection;

var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
XmlConfigurator.Configure(logRepository, new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));

var version = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion;
LogManager.GetLogger(typeof(Program)).Info($"RMQHost version: {version}");

var config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .Build()
    .Get<AppConfig>();

foreach (var channel in config.Notifications.Channels)
{
    // защита на случай если скопируют конфигу, QueueMode = true, тогда возможно уведомление зациклится в очереди 
    channel.QueueMode = false;
}

IHost host = Host.CreateDefaultBuilder(args)
    .UseServiceProviderFactory(new AutofacServiceProviderFactory())
    .ConfigureContainer<ContainerBuilder>(builder =>
    {
        builder.Register<AppConfig>(_ => config).SingleInstance().As<INotifyRmqConfig>();
        builder.RegisterModule(new NotificationRmqHostAutofac());
    }).ConfigureLogging(logBuilder =>
    {
        logBuilder
            .ClearProviders()
            .AddLog4Net()
            .AddOpenTelemetryAppender(config.Telemetry);
    })
    .ConfigureServices((services) =>
    {
        services
            .AddTelemetry(config.Telemetry)
            .AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();
