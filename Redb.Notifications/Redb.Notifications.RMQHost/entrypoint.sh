#!/bin/sh
echo Starting Redb.Notification Service RMQ Host ...

cd /redbn-notifications-rmqhost/

if [ -z "$REDBN_RMQHOST_APPSETTINGS" ]
then
      echo "with default appsettings.json file"
else
      echo "with config: $REDBN_RMQHOST_APPSETTINGS"
      cp $REDBN_RMQHOST_APPSETTINGS /redbn-notifications-rmqhost/appsettings.json
fi

if [ -z "$REDBN_RMQHOST_LOG4NET" ]
then
      echo "with default log4net.config file"
else
      echo "with config: $REDBN_RMQHOST_LOG4NET"
      cp $REDBN_RMQHOST_LOG4NET /redbn-notifications-rmqhost/log4net.config
fi

/usr/bin/dotnet Redb.Notifications.RMQHost.dll