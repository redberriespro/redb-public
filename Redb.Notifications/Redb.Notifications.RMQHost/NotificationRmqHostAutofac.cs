using Autofac;
using Redb.Notifications.Core.Config;
using Redb.Notifications.RMQ;
using Redb.Notifications.RMQ.Config;
using Redb.Notifications.RMQHost.Configs;
using Redb.Notifications.RMQHost.Telemetry;
using Redb.LifetimeSynchronizedCache;
using Redb.LifetimeSynchronizedCache.Autofac;

namespace Redb.Notifications.RMQHost;

internal class NotificationRmqHostAutofac : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterModule<NotificationBusAutofacModuleShared>(); 

        builder.Register<AppConfig>(_ =>
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build()
                .Get<AppConfig>();
            foreach (var channel in config.Notifications.Channels)
            {
                // защита на случай если скопируют конфигу, QueueMode = true, тогда возможно уведомление зациклится в очереди 
                channel.QueueMode = false;
            }
            return config;
        }).SingleInstance().As<INotifyRmqConfig>();
        builder
            .Register(c=>c.Resolve<INotifyRmqConfig>().Notifications.RmqProvider.Queues)
            .SingleInstance()
            .As<IEnumerable<IQueueConfigs>>();
        builder
            .Register(c => c.Resolve<INotifyRmqConfig>().Notifications)
            .SingleInstance()
            .As<IRmqNotifications>()
            .As<INotificationConfig>();

        builder.RegisterInstance(new LifetimeSynchronizedMemoryCacheOptions());

        builder.RegisterModule<LifetimeSynchronizedMemoryCacheAutofacModule>();
        builder.RegisterModule<Core.NotificationsAutofac>();
        builder.RegisterModule<Core.DefaultProviderAutofac>();

        builder.RegisterType<OtelMetrics>().As<IOtelMetrics>().SingleInstance();
    }
}
