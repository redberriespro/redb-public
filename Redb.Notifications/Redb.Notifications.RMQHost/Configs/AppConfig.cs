﻿using Newtonsoft.Json;
using Redb.Notifications.RMQ.Config;

namespace Redb.Notifications.RMQHost.Configs;

public class AppConfig : INotifyRmqConfig
{
    [JsonProperty("rabbitmqConnection")]
    public RabbitmqConnection RabbitmqConnection { get; set; } = new();

    [JsonProperty("notifications", Required = Required.Always)]
    public RmqNotificationConfig Notifications { get; set; }

    [JsonProperty("telemetry")]
    public TelemetryConfig? Telemetry { get; set; }

    /// <summary>
    /// Путь к каталогу, в который сохраняются необработанные из-за ошибок сообщения.
    /// </summary>
    [JsonProperty("dropoutPath")]
    public string? DropoutPath { get; set; }

    IRabbitmqConnection INotifyRmqConfig.RabbitmqConnection => RabbitmqConnection;

    IRmqNotifications INotifyRmqConfig.Notifications => Notifications;
}
