using Newtonsoft.Json;

namespace Redb.Notifications.RMQHost.Configs
{
    public class TelemetryConfig
    {
        [JsonProperty("collectorEndpoint")] 
        public string CollectorEndpoint { get; set; }

        [JsonProperty("tracingSources")] 
        public string[] TracingSources { get; set; }

        [JsonProperty("meters")]
        public string[] Meters { get; set; }

        [JsonProperty("eventSources")]
        public string[] EventSources { get; set; }

        [JsonProperty("logLevel")]
        public LogLevel LogLevel { get; set; } = LogLevel.Information;

        [JsonProperty("debug")]
        public bool Debug { get; set; }
    }
}
