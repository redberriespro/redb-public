﻿namespace Redb.Notifications.RMQ
{
    internal static class LogContext
    {
        public const string QueueName = "Queue.Name";
        public const string QueueRetryAttemps = "Queue.Retry.Attemps";
        public const string MessageId = "Message.Id";
    }
}
