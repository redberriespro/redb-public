using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using RabbitMQ.Client;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Providers;
using Redb.Notifications.RMQ.NotificationBus;

namespace Redb.Notifications.RMQ.Providers
{
    public interface IRabbitmqNotificationsQueueingProvider : INotificationsQueueingProvider
    {
    }

    public class RabbitmqNotificationsQueueingProvider
        : NotificationsQueueingProvider
        , IRabbitmqNotificationsQueueingProvider
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(RabbitmqNotificationsQueueingProvider));

        private readonly INotifyRabbitMqServer _rmqServer;

        public RabbitmqNotificationsQueueingProvider(
            INotifyRabbitMqServer rmqServer
        )
        {
            _rmqServer = rmqServer;
        }

        public override Task Send(INotificationPackage package, CancellationToken ct = default)
        {
            _rmqServer.Publish(package);
            return Task.CompletedTask;
        }
    }
}
