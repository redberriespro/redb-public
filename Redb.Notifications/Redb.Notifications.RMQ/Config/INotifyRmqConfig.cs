using Redb.Notifications.Core.Config;
using System;
using System.Collections.Generic;

namespace Redb.Notifications.RMQ.Config
{
    public interface INotifyRmqConfig
    {
        IRabbitmqConnection RabbitmqConnection { get; }

        IRmqNotifications Notifications { get; }

        /// <summary>
        /// Путь к каталогу, в который сохраняются необработанные из-за ошибок сообщения.
        /// </summary>
        string? DropoutPath { get; }
    }

    public interface IQueueDefaultConfig
    {
        string NameQueuePattern { get; }

        bool IsDurable { get; }

        bool IsExclusive { get; }

        bool IsAutoDelete { get; }
        
        /// <summary>
        /// Количество параллельно обрабатываемых сообщений в одной очереди
        /// </summary>
        ushort PrefetchCount { get; }

        /// <summary>
        ///     Необязательные аргументы
        /// </summary>
        IDictionary<string, object> Arguments { get; }

        string CreateNameQueue(string name);

        /// <summary>
        /// Включение/отключения механизма повторной обработки сообщений с задержкой через очередь-отстойник.
        /// </summary>
        /// <remarks>
        /// В отключенном состоянии используется текущий механизм, при котором проблемное сообщение сразу отправляется в очередь, а коньсюмер засыпает.
        /// </remarks>
        bool Retry { get; }

        /// <summary>
        /// Имя очереди-отстойника.
        /// </summary>
        string FailedQueryName { get; }

        /// <summary>
        /// Имя обменника, через который проблемное сообщение из оригинальной очереди отправляется в очередь-отстойник.
        /// </summary>
        string FailedExchangeName { get; }

        /// <summary>
        /// Имя обменника, через который проблемное сообщение из очереди-отстойниа возвращается в оригинальную очередь.
        /// </summary>
        string RestoredExchangeName { get; }

        /// <summary>
        /// Задержка, через которое будут выполнена повторная обработка проблемного сообщения. Определяет длительность нахождения проблемного сообщения в очереди-отстойнике.
        /// </summary>
        TimeSpan RetryDelay { get; }

        /// <summary>
        /// Количество попыток обработки проблемного сообщения, после которого оно будет удалено из очереди и сохранено в каталог <see cref="INotifyRmqConfig.DropoutPath"/>
        /// </summary>
        /// <remarks>
        /// Значение 0 указывает на то, что проблемное сообщение никогда не будет удалено из очереди и повторная обработка будет выполняться бесконечно.
        /// </remarks>
        int AttemptsBeforeDropout { get; }

        IQueueConfigs CreateQueueConfig(string channelName);
    }

    public interface IQueueConfigs
    {
        string Name { get; }

        /// <summary>
        /// Имя очереди, может быть пустым и тогда Имя очереди будет сгенерировано
        /// </summary>
        string NameQueue { get; }

        bool? IsDurable { get; }

        bool? IsExclusive { get; }

        bool? IsAutoDelete { get; }

        /// <summary>
        /// Количество параллельно обрабатываемых сообщений в одной очереди
        /// </summary>
        ushort? PrefetchCount { get; }

        /// <summary>
        /// Включение/отключения механизма повторной обработки сообщений с задержкой через очередь-отстойник.
        /// </summary>
        /// <remarks>
        /// В отключенном состоянии используется текущий механизм, при котором проблемное сообщение сразу отправляется в очередь, а коньсюмер засыпает.
        /// </remarks>
        bool? Retry { get; }

        /// <summary>
        /// Имя очереди-отстойника.
        /// </summary>
        string? FailedQueryName { get; }

        /// <summary>
        /// Имя обменника, через который проблемное сообщение из оригинальной очереди отправляется в очередь-отстойник.
        /// </summary>
        string? FailedExchangeName { get; }

        /// <summary>
        /// Имя обменника, через который проблемное сообщение из очереди-отстойниа возвращается в оригинальную очередь.
        /// </summary>
        string? RestoredExchangeName { get; }

        /// <summary>
        /// Задержка, через которое будут выполнена повторная обработка проблемного сообщения. Определяет длительность нахождения проблемного сообщения в очереди-отстойнике.
        /// </summary>
        TimeSpan? RetryDelay { get; }

        /// <summary>
        /// Количество попыток обработки проблемного сообщения, после которого оно будет удалено из очереди и сохранено в каталог <see cref="INotifyRmqConfig.DropoutPath"/>
        /// </summary>
        /// <remarks>
        /// Значение 0 указывает на то, что проблемное сообщение никогда не будет удалено из очереди и повторная обработка будет выполняться бесконечно.
        /// </remarks>
        int? AttemptsBeforeDropout { get; }
    }

    public interface IExchangeConfigs
    {
        /// <summary>
        ///     Название обменника, который мы хотим создать. 
        ///     Название должно быть уникальным.
        /// </summary>
        string Name { get; }

        /// <summary>
        ///     Тип обменника
        /// </summary>
        string Type { get; }

        /// <summary>
        ///     Если установить true, то exchange будет 
        ///     являться постоянным. 
        ///     Он будет храниться на диске и сможет 
        ///     пережить перезапуск сервера/брокера. 
        ///     Если значение false, то exchange является 
        ///     временным и будет удаляться, 
        ///     когда сервер/брокер будет перезагружен
        /// </summary>
        bool IsDurable { get; }

        /// <summary>
        ///     Автоматическое удаление. Exchange будет удален, 
        ///     когда будут удалены все связанные с ним очереди
        /// </summary>
        bool IsAutoDelete { get; }

        /// <summary>
        ///     Необязательные аргументы
        /// </summary>
        IDictionary<string, object> Arguments { get; }

        /// <summary>
        /// Требуется связать Exchange и очередь
        /// </summary>
        /// <value>
        /// <c>true</c> - требуется
        /// <c>false</c> - не требуется
        /// </value>
        bool AutoBinding { get; }
    }

    public interface IRabbitmqConnection
    {
        string ApplicationName { get; }
        List<string> HostNames { get; }

        /// <summary>
        /// Порт Management подсистемы RabbitMq
        /// </summary>
        int? ManagementPort { get; }
        string VirtualHost { get;  }
        int Port { get; } 
        ICredentialsContainer Credentials { get; }
        bool SharedConnection { get; }
        IReadOnlyDictionary<string, string>? ClientProperties { get; }
    }

    public interface ICredentialsContainer
    {
        string UserName { get; set; }
        string Password { get; set; }
    }

    public interface IRmqNotifications : INotificationConfig
    {
        IRmqProvider RmqProvider { get; }
    }

    public interface IRmqProvider
    {
        IExchangeConfigs? Exchange { get; }
        IEnumerable<IQueueConfigs> Queues { get; }
        IQueueDefaultConfig QueueDefault { get; }
    }
}
