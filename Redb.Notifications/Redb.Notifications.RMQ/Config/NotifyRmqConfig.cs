using Newtonsoft.Json;
using Redb.Notifications.Core.Config;
using System;
using System.Collections.Generic;

namespace Redb.Notifications.RMQ.Config
{
    public class NotifyRmqConfig : INotifyRmqConfig
    {
        [JsonProperty("rabbitmqConnection")]
        public RabbitmqConnection RabbitmqConnection { get; set; } = new RabbitmqConnection();

        [JsonProperty("notifications", Required = Required.Always)]
        public RmqNotificationConfig Notifications { get; set; } = new RmqNotificationConfig();
        
        public string? DropoutPath => null;

        IRabbitmqConnection INotifyRmqConfig.RabbitmqConnection => RabbitmqConnection;

        IRmqNotifications INotifyRmqConfig.Notifications => Notifications;
    }

    public class RabbitmqConnection : IRabbitmqConnection
    {
        [JsonProperty("applicationName")]
        public string ApplicationName { get; set; }

        [JsonProperty("hostNames", Required = Required.Always)]
        public List<string> HostNames { get; set; } = new List<string>();

        [JsonProperty("virtualHost")]
        public string VirtualHost { get; set; } = string.Empty;

        /// <summary>
        /// Порт Management подсистемы RabbitMq
        /// </summary>
        [JsonProperty("managementPort")]
        public int? ManagementPort { get; }

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("credentials", NullValueHandling = NullValueHandling.Ignore)]
        public CredentialsContainer Credentials { get; set; } = new CredentialsContainer();

        [JsonProperty("sharedConnection")]
        public bool SharedConnection { get; set; } = false;

        [JsonProperty("clientProperties", NullValueHandling = NullValueHandling.Ignore)]
        public IReadOnlyDictionary<string, string>? ClientProperties { get; set; } = null;

        ICredentialsContainer IRabbitmqConnection.Credentials => Credentials;

        public class CredentialsContainer : ICredentialsContainer
        {
            [JsonProperty("userName", Required = Required.Always)]
            public string UserName { get; set; } = string.Empty;

            [JsonProperty("password", Required = Required.Always)]
            public string Password { get; set; } = string.Empty;
        }
    }

    public class ExchangeConfigs : IExchangeConfigs
    {
        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;

        [JsonProperty("exchangeType")]
        public string Type { get; set; } = "direct";

        [JsonProperty("durable")]
        public bool IsDurable { get; set; } = true;

        [JsonProperty("autoDelete")]
        public bool IsAutoDelete { get; set; } = false;

        [JsonIgnore] // не сделал, т.к. не надо было
        public Dictionary<string, object> Arguments { get; set; } = new Dictionary<string, object>();

        /// <inheritdoc/>
        [JsonProperty("autoBinding")]
        public bool AutoBinding { get; set; } = true;

        IDictionary<string, object> IExchangeConfigs.Arguments => Arguments;
    }

    public class QueueConfigs : IQueueConfigs
    {
        public QueueConfigs() { }

        public QueueConfigs(IQueueDefaultConfig defaultConfig, string name) 
        {
            Name = name;
            NameQueue = defaultConfig.CreateNameQueue(name);
            IsDurable = defaultConfig.IsDurable;
            IsExclusive = defaultConfig.IsExclusive;
            IsAutoDelete = defaultConfig.IsAutoDelete;
        }

        [JsonProperty("name")] 
        public string Name { get; set; } = string.Empty;
                
        [JsonProperty("nameQueue")] 
        public string NameQueue { get; set; } = string.Empty;

        [JsonProperty("durable")]
        public bool? IsDurable { get; set; }

        [JsonProperty("exclusive")]
        public bool? IsExclusive { get; set; }

        [JsonProperty("autoDelete")]
        public bool? IsAutoDelete { get; set; }

        ///<inheritdoc/>
        [JsonProperty("prefetchCount")]
        public ushort? PrefetchCount { get; set; }

        [JsonProperty("retry")]
        public bool? Retry { get; set; }

        [JsonProperty("failedQueryName")]
        public string? FailedQueryName { get; set; }

        [JsonProperty("failedExchangeName")]
        public string? FailedExchangeName { get; set; }

        [JsonProperty("restoredExchangeName")]
        public string? RestoredExchangeName { get; set; }

        [JsonProperty("retryDelay")]
        public TimeSpan? RetryDelay { get; set; }

        [JsonProperty("attemptsBeforeDropout")]
        public int? AttemptsBeforeDropout { get; set; }
    }

    public class QueueDefault : IQueueDefaultConfig
    {
        [JsonProperty("nameQueuePattern")]
        public string NameQueuePattern { get; set; } = "{0}-notify-q";

        [JsonProperty("durable")]
        public bool IsDurable { get; set; } = true;

        [JsonProperty("exclusive")]
        public bool IsExclusive { get; set; } = false;

        [JsonProperty("autoDelete")]
        public bool IsAutoDelete { get; set; } = false;

        ///<inheritdoc/>
        [JsonProperty("prefetchCount")]
        public ushort PrefetchCount { get; set; } = 1;

        [JsonProperty("retry")]
        public bool Retry { get; set; } = false;

        [JsonProperty("failedQueryName")]
        public string FailedQueryName { get; set; } = "failed-notify-q";

        [JsonProperty("failedExchangeName")]
        public string FailedExchangeName { get; set; } = "redb.notifications.failed";

        [JsonProperty("restoredExchangeName")]
        public string RestoredExchangeName { get; set; } = "redb.notifications.restored";

        [JsonProperty("retryDelay")]
        public TimeSpan RetryDelay { get; set; } = TimeSpan.FromSeconds(3);

        [JsonProperty("attemptsBeforeDropout")]
        public int AttemptsBeforeDropout { get; set; } = 0;

        [JsonIgnore] // не сделал, т.к. не надо было
        public Dictionary<string, object> Arguments { get; set; } = new Dictionary<string, object>() 
        {
            {"x-queue-type", "classic"}
        };

        IDictionary<string, object> IQueueDefaultConfig.Arguments => Arguments;

        public string CreateNameQueue(string name)
        {
            return string.Format(NameQueuePattern, name);
        }

        public IQueueConfigs CreateQueueConfig(string channelName)
        {
            return new QueueConfigs(this, channelName);
        }
    }

    public class RmqNotificationConfig
        : NotificationConfig
        , IRmqNotifications
    {
        public bool UseSharedRmqConnection { get; set; } = false;

        public RmqNotifyProvider RmqProvider { get; set; } = new RmqNotifyProvider();

        IRmqProvider IRmqNotifications.RmqProvider => RmqProvider;
    }

    public class RmqNotifyProvider : IRmqProvider
    {
        public RmqNotifyProvider() { }
        public RmqNotifyProvider(string name) 
        { 
            Exchange = new ExchangeConfigs { Name = name };
        }

        [JsonProperty("exchange")]
        public ExchangeConfigs? Exchange { get; set; }

        [JsonProperty("queue")]
        public List<QueueConfigs> Queues { get; set; } = new List<QueueConfigs>();
        
        [JsonProperty("queueDefault")]
        public QueueDefault QueueDefault { get; set; } = new QueueDefault();

        IExchangeConfigs? IRmqProvider.Exchange => Exchange;

        IEnumerable<IQueueConfigs> IRmqProvider.Queues => Queues;
            
        IQueueDefaultConfig IRmqProvider.QueueDefault => QueueDefault;
    }
}
