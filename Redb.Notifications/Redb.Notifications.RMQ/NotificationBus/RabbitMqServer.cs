using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using RabbitMQ.Client;
using Redb.Notifications.Core.Config;
using Redb.Notifications.Core.Providers;
using Redb.Notifications.RMQ.Config;

namespace Redb.Notifications.RMQ.NotificationBus
{
    public interface IRabbitMqServer : IDisposable
    {
        void Publish(IQueueingPackage package);
        void Start(RmqMessageSender sender);
        void Stop(string nameType);
    }

    internal class RabbitMqServer : IRabbitMqServer
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(RabbitMqProducer));
        private readonly IProducer? _producer;
        private readonly Dictionary<string, IConsumer> _consumers = new Dictionary<string, IConsumer>();
        private readonly IRabbitMqMessageFactory _factory;
        private readonly IRabbitmqConnection _connectionConfig;
        private readonly IRmqProvider _provider;
        private readonly IConnection _connection;
        private bool _disposedValue;

        public RabbitMqServer(
            Func<IRabbitmqConnection, IRmqProvider, IRabbitMqMessageFactory> factoryCreator,
            IRabbitmqConnection connectionConfig,
            IRmqProvider provider,
            IEnumerable<string> routingKeys
            )
        {
            _factory = factoryCreator(connectionConfig, provider);
            _connectionConfig = connectionConfig;
            _provider = provider;
            _connection = _factory.CreateConnection();
            if(_provider.Exchange!= null)
            {
                _producer = _factory.CreateMessageProducer(_connection, _provider.Exchange);
                _producer.BindQueueExchange(_provider, routingKeys);
            }
        }

        public void Start(RmqMessageSender sender)
        {
            if (_consumers.ContainsKey(sender.QueueName))
            {
                _log.Info("Consumer already started.");
                return;
            }

            var queueConfig = 
                _provider.Queues.FirstOrDefault(qc => qc.Name == sender.QueueName) 
                ?? _provider.QueueDefault.CreateQueueConfig(sender.QueueName);
            var consumer = _factory.CreateMessageConsumer(_connection, queueConfig, _connectionConfig);
            _log.Info($"Started read messages for '{sender.QueueName}' with feedback exchange '{sender.FeedbackExName}'");
            consumer.StartLoadMessages(sender);
            _consumers.Add(sender.QueueName, consumer);
        }

        public void Stop(string nameType)
        {
            if (!_consumers.ContainsKey(nameType))
            {
                _log.Debug("Consumer isn't found.");
                return;
            }

            _consumers[nameType].Dispose();
            _consumers.Remove(nameType);
        }
        
        public void Publish(IQueueingPackage package)
        {
            if (_producer == null)
            {
                _log.Warn("Producer didn't initialized. Check config 'producer'");
                return;
            }

            try
            {
                _producer.Publish(package);
                _log.Debug($"Send on RMQ {_provider.Exchange?.Name ?? ""}.{package.RoutingKey}");
            }
            catch (Exception ex) 
            {
                _log.Error($"Message of {package.RoutingKey} didn't sent on RMQ/ Reason: {ex.Message}", ex);
                throw new Exception($"Message of {package.RoutingKey} didn't sent on RMQ/ Reason: {ex.Message}", ex);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    foreach (var item in _consumers.Values)
                    {
                        item.Dispose();
                    }
                    _producer?.Dispose();
                    _connection?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~RabbitMqServer()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }
}
