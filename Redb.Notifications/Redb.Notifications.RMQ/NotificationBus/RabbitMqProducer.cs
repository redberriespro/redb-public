using log4net;
using RabbitMQ.Client;
using Redb.Notifications.Core.Providers;
using Redb.Notifications.RMQ.Config;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Redb.Notifications.RMQ.NotificationBus
{
    public interface IProducer: IDisposable
    {
        /// <summary>
        /// связываем exchange и queue
        /// </summary>
        void BindQueueExchange(IRmqProvider provider, IEnumerable<string> routingKeys);

        /// <summary>
        /// Передать message на RMQ
        /// </summary>
        void Publish(IQueueingPackage package);
    }

    internal class RabbitMqProducer : IProducer
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(RabbitMqProducer));

        private readonly IExchangeConfigs _exchangeConfig;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private bool _disposedValue;

        public RabbitMqProducer(
            IConnection connection,
            IExchangeConfigs exchangeConfig
            )
        {
            _exchangeConfig = exchangeConfig;
            _connection = connection;
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare( 
                exchange: exchangeConfig.Name,
                type: exchangeConfig.Type,
                durable: exchangeConfig.IsDurable,
                autoDelete: exchangeConfig.IsAutoDelete,
                exchangeConfig.Arguments
                );
        }

        /// <inheritdoc/>
        public void BindQueueExchange(IRmqProvider provider, IEnumerable<string> routingKeys)
        {
            if (!_exchangeConfig.AutoBinding) return;

            foreach (var routingKey in routingKeys) // берем канал для рассылки
            {
                var queueConfig = provider.Queues.FirstOrDefault(qc => qc.Name == routingKey); // ищем для канала конфигурацию очереди
                if (queueConfig==null) // если её нет создаем имя очереди на основе параметров по умолчанию и биндимся на неё
                {
                    _channel.QueueBind(
                        queue: provider.QueueDefault.CreateNameQueue(routingKey),
                        exchange: _exchangeConfig.Name,
                        routingKey: routingKey);
                }
                else // если есть конфигурация, берем имя очереди, если имя очереди не описано создаем его по шаблону
                {
                    _channel.QueueBind(
                        queue: string.IsNullOrEmpty(queueConfig.NameQueue)
                            ? provider.QueueDefault.CreateNameQueue(queueConfig.Name)
                            : queueConfig.NameQueue,
                        exchange: _exchangeConfig.Name,
                        routingKey: queueConfig.Name);
                }
            }
        }

        public void Publish(IQueueingPackage package)
        {
            try
            {
                IBasicProperties? props = null;
                if (package.CorrelationId != null)
                {
                    props = _channel.CreateBasicProperties();
                    props.CorrelationId = package.CorrelationId;
                }

                _channel.BasicPublish(exchange: _exchangeConfig.Name,
                                    routingKey: package.RoutingKey,
                                    basicProperties: props,
                                    body: package.ToArrayByte());
                _log.Debug($"Send on RMQ {_exchangeConfig.Name}.{package.RoutingKey}");
            }
            catch (Exception ex) 
            {
                _log.Error($"Message of {package.RoutingKey} didn't sent on RMQ/ Reason: {ex.Message}", ex);
                throw new Exception($"Message of {package.RoutingKey} didn't sent on RMQ/ Reason: {ex.Message}", ex);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _channel.Dispose();
                    _connection.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
