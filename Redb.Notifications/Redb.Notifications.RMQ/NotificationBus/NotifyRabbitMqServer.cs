using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using RabbitMQ.Client;
using Redb.Notifications.Core.Providers;
using Redb.Notifications.RMQ.Config;

namespace Redb.Notifications.RMQ.NotificationBus
{
    public interface INotifyRabbitMqServer : IRabbitMqServer, IDisposable
    {
        void Publish(INotificationPackage package);

    }

    internal class NotifyRabbitMqServer : RabbitMqServer, INotifyRabbitMqServer
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(NotifyRabbitMqServer));

        public NotifyRabbitMqServer(
            Func<IRabbitmqConnection, IRmqProvider, IRabbitMqMessageFactory> factoryCreator,
            INotifyRmqConfig config
            )
            : base(
                  factoryCreator,
                  config.RabbitmqConnection,
                  config.Notifications.RmqProvider,
                  config.Notifications.Channels.Select(c => c.Name))
        { }

        public void Publish(INotificationPackage package)
        {
           base.Publish(package);
        }

    }
}

