﻿using RabbitMQ.Client;
using Redb.Notifications.RMQ.Config;
using Redb.Notifications.RMQ.Telemetry;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net.Core;
using log4net;
using RabbitMQ.Client.Framing.Impl;
using RabbitMQ.Client.Events;

namespace Redb.Notifications.RMQ.NotificationBus
{
    public interface IRabbitMqMessageFactory
    {
        IConnection CreateConnection();
        IProducer CreateMessageProducer(IConnection connection, IExchangeConfigs exchangeConfig);
        IConsumer CreateMessageConsumer(IConnection connection, IQueueConfigs queueConfig, IRabbitmqConnection _connectionConfig);
    }

    internal class RabbitMqMessageFactory : IRabbitMqMessageFactory
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(RabbitMqMessageFactory));

        private readonly IRabbitmqConnection _connection;
        private readonly IRmqProvider _provider;
        private readonly ConnectionFactory _connectionFactory;
        private readonly Lazy<IConnection> _sharedConnection;
        private readonly IDropoutBox _dropoutBox;
        private readonly IMetrics _metrics;

        public RabbitMqMessageFactory(
                    IRabbitmqConnection connection,
                    IRmqProvider provider,
                    IDropoutBox dropoutBox,
                    IMetrics metrics)
        {
            _connectionFactory = new ConnectionFactory
            {
                Port = connection.Port,
                VirtualHost = connection.VirtualHost,
                UserName = connection.Credentials.UserName,
                Password = connection.Credentials.Password,
                DispatchConsumersAsync = true // use async-oriented consumer dispatcher. Only compatible with IAsyncBasicConsumer implementations
            };

            if (connection.ClientProperties?.Any() is true)
            {
                foreach (var prop in connection.ClientProperties)
                {
                    if (!string.IsNullOrEmpty(prop.Value))
                    {
                        _connectionFactory.ClientProperties.TryAdd(prop.Key, Encoding.UTF8.GetBytes(prop.Value));
                    }
                }
            }

            _connection = connection;
            _provider = provider;

            _sharedConnection = new Lazy<IConnection>(() =>
            {
                return _connectionFactory.CreateConnection(_connection.HostNames);
            }, LazyThreadSafetyMode.ExecutionAndPublication);
            _dropoutBox = dropoutBox;
            _metrics = metrics;
        }

        public IConnection CreateConnection()
        {
            try
            {
                _log.Info("RabbitMq connection creating at redb-public.");
                var connection = _connection.SharedConnection
                                    ? _sharedConnection.Value
                                    : _connectionFactory.CreateConnection(_connection.HostNames, _connection.ApplicationName + " (notifications)");
                _log.Info("RabbitMq connection created at redb-public.");

                if (connection is IAutorecoveringConnection aconnection)
                {
                    aconnection.CallbackException += delegate (object sender, CallbackExceptionEventArgs args)
                    {
                        _log.Error("RabbitMQConnection.CallbackException", args.Exception);
                    };


                    aconnection.ConnectionRecoveryError += delegate (object sender, ConnectionRecoveryErrorEventArgs args)
                    {
                        _log.Error("RabbitMQConnection.ConnectionRecoveryError", args.Exception);
                    };

                    aconnection.ConnectionShutdown += delegate (object sender, ShutdownEventArgs args)
                    {
                        _log.Info($"RabbitMQConnection.ConnectionShutdown. Reason: {args.ReplyText}");
                    };
                }

                return connection;
            }
            catch (Exception ex)
            {
                _log.Error("Error occured on rabbitmq connection creating.", ex);

                throw ex;
            }
        }

        public IProducer CreateMessageProducer(IConnection connection, IExchangeConfigs exchangeConfig)
        {
            return new RabbitMqProducer(
                connection,
                exchangeConfig);
        }

        public IConsumer CreateMessageConsumer(IConnection connection, IQueueConfigs queueConfig, IRabbitmqConnection _connectionConfig)
        {
            return new RabbitMqConsumer(
                connection,
                queueConfig,
                _provider.QueueDefault,
                _dropoutBox,
                _metrics,
                _connectionConfig);
        }
    }
}
