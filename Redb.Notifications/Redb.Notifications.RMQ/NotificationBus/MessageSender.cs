#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Redb.Notifications.Core.Services;

namespace Redb.Notifications.RMQ.NotificationBus
{
    public class MessageSender
    {
        public MessageSender(Func<byte[], string, CancellationToken, Task<byte[]>> act,
            string? feedbackExName,
            Action<ContentSupportChannel> notifySupportChannel)
        {
            Act = act;
            FeedbackExName = feedbackExName;
            NotifyTechSupport = notifySupportChannel;

        }

        public Func<byte[], string, CancellationToken, Task<byte[]>> Act { get; }
        public string? FeedbackExName { get; }
        public Action<ContentSupportChannel> NotifyTechSupport { get; }
    }

    public class RmqMessageSender : MessageSender
    {
        public RmqMessageSender(
            Func<byte[], string, CancellationToken, Task<byte[]>> act,
            string queueName,
            Action<ContentSupportChannel> notifySupportChannel,
            string? feedbackExName)
            : base(act, feedbackExName, notifySupportChannel)
        {
            QueueName = queueName;
        }

        public string QueueName { get; }
    }
}
