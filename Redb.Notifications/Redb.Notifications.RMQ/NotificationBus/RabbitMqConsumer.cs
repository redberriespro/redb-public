using log4net;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Redb.Notifications.RMQ.Config;
using Redb.Notifications.RMQ.NotificationBus.Retrying;
using Redb.Notifications.RMQ.Telemetry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Redb.Notifications.RMQ.NotificationBus
{
    public interface IConsumer : IDisposable
    {
        void StartLoadMessages(MessageSender sender);
    }

    internal class RabbitMqConsumer : IConsumer
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(RabbitMqConsumer));

        private readonly string _name;
        private readonly IQueueConfigs _queueConfig;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _nameQueue;
        private CancellationTokenSource? _cancelTokenSource;
        private AsyncEventingBasicConsumer? _consumer;
        private bool _disposedValue;
        private AsyncEventHandler<BasicDeliverEventArgs>? _receivedAction;
        private string _consumerTag = string.Empty;
        private readonly IDropoutBox _dropoutBox;
        private readonly IMetrics _metrics;
        private int _attemptsBeforeDropout;
        private bool _useRetry;

        public RabbitMqConsumer(
            IConnection connection,
            IQueueConfigs queueConfig,
            IQueueDefaultConfig queueDefaultConfig,
            IDropoutBox dropoutBox,
            IMetrics metrics,
            IRabbitmqConnection _connectionConfig)
        {
            _queueConfig = queueConfig;
            _name = queueConfig.Name;

            _connection = connection;
            _channel = _connection.CreateModel();
            _nameQueue = string.IsNullOrEmpty(_queueConfig.NameQueue)
                ? string.Format(queueDefaultConfig.NameQueuePattern, _name)
                : _queueConfig.NameQueue;
            _useRetry = _queueConfig.Retry ?? queueDefaultConfig.Retry;
            _attemptsBeforeDropout = _queueConfig.AttemptsBeforeDropout ?? queueDefaultConfig.AttemptsBeforeDropout;

            var failedExchange = _queueConfig.FailedExchangeName ?? queueDefaultConfig.FailedExchangeName;

            _channel.ExchangeDeclare(
                    failedExchange,
                    "direct",
                    _queueConfig.IsDurable ?? queueDefaultConfig.IsDurable,
                    _queueConfig.IsAutoDelete ?? queueDefaultConfig.IsAutoDelete);

            var restoredExchange = _queueConfig.RestoredExchangeName ?? queueDefaultConfig.RestoredExchangeName;

            _channel.ExchangeDeclare(
                        restoredExchange,
                        "direct",
                        _queueConfig.IsDurable ?? queueDefaultConfig.IsDurable,
                        _queueConfig.IsAutoDelete ?? queueDefaultConfig.IsAutoDelete,
                        new Dictionary<string, object>());

            var failedQuery = _queueConfig.FailedQueryName ?? queueDefaultConfig.FailedQueryName;

            var retryDelay = _queueConfig.RetryDelay ?? queueDefaultConfig.RetryDelay;

            _channel.QueueDeclare(queue: failedQuery,
                                  durable: _queueConfig.IsDurable ?? queueDefaultConfig.IsDurable,
                                  exclusive: _queueConfig.IsExclusive ?? queueDefaultConfig.IsExclusive,
                                  autoDelete: _queueConfig.IsAutoDelete ?? queueDefaultConfig.IsAutoDelete,
                                  arguments: new Dictionary<string, object>
                                  {
                                        { "x-message-ttl", (int)retryDelay.TotalMilliseconds },
                                        { "x-dead-letter-exchange", restoredExchange }
                                  });

            try
            {
                _channel.QueueDeclare(queue: _nameQueue,
                              durable: _queueConfig.IsDurable ?? queueDefaultConfig.IsDurable,
                              exclusive: _queueConfig.IsExclusive ?? queueDefaultConfig.IsExclusive,
                              autoDelete: _queueConfig.IsAutoDelete ?? queueDefaultConfig.IsAutoDelete,
                              arguments: new Dictionary<string, object>
                              {
                                      { "x-dead-letter-exchange", failedExchange }
                              });
            }
            catch (Exception ex)
            {
                // Обрабатываем ошибку PRECONDITION_FAILED,- когда очередь пытаются создать неподходящую. 
                // Это нужно для пересоздание очереди без ошибки автоматически с теми же bindings
                if (ex.Message.Contains("PRECONDITION_FAILED"))
                {
                    string rabbitMqUrl = $"http://{_connectionConfig.HostNames.First()}:{_connectionConfig.ManagementPort ?? 15672}"; // Адрес RabbitMQ Management API
                    string username = _connectionConfig.Credentials.UserName;
                    string password = _connectionConfig.Credentials.Password;

                    using (HttpClient client = new HttpClient())
                    {
                        // Настройка аутентификации
                        var byteArray = Encoding.ASCII.GetBytes($"{username}:{password}");
                        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                        // URL для получения всех bindings для конкретной очереди
                        string requestUrl = $"{rabbitMqUrl}/api/queues/{WebUtility.UrlEncode(_connectionConfig.VirtualHost)}/{_nameQueue}/bindings";

                        try
                        {
                            HttpResponseMessage response = client.GetAsync(requestUrl).GetAwaiter().GetResult();
                            response.EnsureSuccessStatusCode();

                            string responseBody = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                            var bindings = JArray.Parse(responseBody);

                            Console.WriteLine("Bindings for queue:");
                            Console.WriteLine(bindings.ToString());

                            _channel = _connection.CreateModel();
                            // удаляем очередь явно, в ней непоходящие параметры
                            
                            _channel.QueueDelete(_nameQueue, true, true);

                            // заново создаём очередь
                            _channel.QueueDeclare(queue: _nameQueue,
                                                  durable: _queueConfig.IsDurable ?? queueDefaultConfig.IsDurable,
                                                  exclusive: _queueConfig.IsExclusive ?? queueDefaultConfig.IsExclusive,
                                                  autoDelete: _queueConfig.IsAutoDelete ?? queueDefaultConfig.IsAutoDelete,
                                                  arguments: new Dictionary<string, object>
                                                  {
                                                           { "x-dead-letter-exchange", failedExchange }
                                                  });
                            
                            // переносим все биндинги со старой в новую
                            foreach (var item in bindings)
                            {
                                if (item is JObject jObject)
                                {
                                    var sourceExchange = jObject.Properties().Single(o => o.Name == "source").Value.ToString();
                                    var routingKey = jObject.Properties().Single(o => o.Name == "routing_key").Value.ToString();
                                    if (!string.IsNullOrEmpty(sourceExchange))
                                    {
                                        _channel.QueueBind(_nameQueue, sourceExchange, routingKey);
                                    }
                                }
                            }
                        }
                        catch (Exception ex2)
                        {
                            Console.WriteLine($"Error: {ex2.Message}");
                            throw ex2;
                        }
                    }
                }
            }
            
            _channel.QueueBind(failedQuery, failedExchange, _name);
            _channel.QueueBind(_nameQueue, restoredExchange, _name);

            if (_useRetry)
            {
                var msg = $"Consuming from queue '{_nameQueue}' with retrying via query '{failedQuery}' with delay '{retryDelay:c}'";

                if (_attemptsBeforeDropout > 0)
                {
                    msg += $" and '{_attemptsBeforeDropout}' max attemps";
                }

                _log.Info(msg + ".");
            }
            else
            {
                _log.Info($"Consuming from queue '{_nameQueue}' without retrying.");
            }

            // количество message обрабатываемые одномоментно. 
            // Если отключить то все сообщения в очереди уйдут в обработку одномоментно
            _channel.BasicQos(
                0,
                prefetchCount: _queueConfig.PrefetchCount ?? queueDefaultConfig.PrefetchCount,
                false);
            _dropoutBox = dropoutBox;
            _metrics = metrics;
        }

        public void StartLoadMessages(MessageSender sender)
        {
            if (_cancelTokenSource != null)
            {
                _log.Warn($"'{_name}' consumer is already running.");
                return;
            }
            _cancelTokenSource = new CancellationTokenSource();
            _consumer = new AsyncEventingBasicConsumer(_channel);
            _receivedAction = async (ch, ea) =>
            {
                try
                {
                    var body = ea.Body.ToArray();
                    var props = ea.BasicProperties;

                    // Отправляем сообщение, например в телеграмм
                    byte[] result = await sender.Act(ea.Body.ToArray(), ea.RoutingKey, _cancelTokenSource.Token); // TODO SPAN MEMORY WARNING

                    // отправляем результат если пришло сообщение на отправку с CorrelationId
                    if (props?.CorrelationId != null && sender.FeedbackExName != null)
                    {
                        var replyProps = _channel.CreateBasicProperties();
                        replyProps.CorrelationId = props.CorrelationId;
                        _channel.BasicPublish(exchange: sender.FeedbackExName,
                            routingKey: "",
                            basicProperties: replyProps,
                            body: result);
                    }

                    _channel.BasicAck(ea.DeliveryTag, false);
                    _log.Debug($"Message processed: {ea.DeliveryTag}, props: {props?.CorrelationId}");
                }
                catch (RequireDropoutException ex) when (_dropoutBox.Enabled)
                {
                    await _dropoutBox.SaveMessageAsync(ea.Body.ToArray(), ea.BasicProperties.Headers, ex, CancellationToken.None);

                    _channel.BasicAck(ea.DeliveryTag, false);

                    using var c0 = LogicalThreadContext.Stacks[LogContext.MessageId].Push(ea.BasicProperties.MessageId);
                    using var c1 = LogicalThreadContext.Stacks[LogContext.QueueName].Push(_nameQueue);

                    var exMessage = "Failed to process message. Terminated error catched. Message saved to dropout box";
                    _log.Warn(exMessage, ex.InnerException ?? ex);
                    sender.NotifyTechSupport(
                        new Core.Services.ContentSupportChannel(
                            exMessage,
                            Core.Services.TypeNotifySupCh.MessageSaveToDropout));

                    _metrics.IncDropoutCount(_nameQueue, ea.BasicProperties.MessageId);
                }
                catch (Exception ex)
                {
                    using var c0 = LogicalThreadContext.Stacks[LogContext.MessageId].Push(ea.BasicProperties.MessageId);
                    using var c1 = LogicalThreadContext.Stacks[LogContext.QueueName].Push(_nameQueue);

                    _metrics.IncRetryCount(_nameQueue, ea.BasicProperties.MessageId);

                    if (_useRetry)
                    {
                        long retryCount = 0;

                        _log.Debug($"Message headers{Environment.NewLine}{JsonSerializer.Serialize(ea.BasicProperties.Headers)}");

                        if (ea.BasicProperties.Headers?.TryGetValue("x-death", out var tmp) is true)
                        {
                            var queue = Encoding.UTF8.GetString((byte[])ea.BasicProperties.Headers["x-first-death-queue"]);
                            var deaths = MessageDiscarding.Parse((List<object>)tmp);

                            retryCount = deaths.First(x => x.Queue == queue).Count;

                            if (_attemptsBeforeDropout > 0
                                && retryCount >= _attemptsBeforeDropout
                                && _dropoutBox.Enabled)
                            {
                                await _dropoutBox.SaveMessageAsync(ea.Body.ToArray(), ea.BasicProperties.Headers, ex, CancellationToken.None);

                                _channel.BasicAck(ea.DeliveryTag, false);

                                using var c2 = LogicalThreadContext.Stacks[LogContext.QueueRetryAttemps].Push(retryCount.ToString());

                                var exRetryMessage = $"Failed to process message, maximum number of retries reached. Message saved to dropout box. Queue: {_nameQueue}, CorrelationId: {ea.BasicProperties?.CorrelationId}"; 
                                _log.Warn(exRetryMessage, ex);
                                sender.NotifyTechSupport(
                                    new Core.Services.ContentSupportChannel(
                                        exRetryMessage,
                                        Core.Services.TypeNotifySupCh.MessageSaveToDropout));

                                _metrics.IncDropoutCount(_nameQueue, ea.BasicProperties.MessageId);

                                return;
                            }
                        }

                        using var c3 = LogicalThreadContext.Stacks[LogContext.QueueRetryAttemps].Push(retryCount.ToString());

                        var exMessage = $"Failed to process message, it will be retry. Queue: {_nameQueue}, CorrelationId: {ea.BasicProperties?.CorrelationId}, RetryCount: {retryCount}";
                        _log.Warn(exMessage, ex);
                        sender.NotifyTechSupport(
                            new Core.Services.ContentSupportChannel(
                                exMessage,
                                Core.Services.TypeNotifySupCh.FailedProcessMessageAndWillRetry));

                        _channel.BasicNack(ea.DeliveryTag, false, false);
                    }
                    else
                    {
                        var exMessage = $"Failed to process message, nack-ing it. Queue: {_nameQueue}, CorrelationId: {ea.BasicProperties?.CorrelationId}";
                        _log.Warn(exMessage, ex);
                        sender.NotifyTechSupport(
                            new Core.Services.ContentSupportChannel(
                                exMessage,
                                Core.Services.TypeNotifySupCh.FailedProcessMessage));

                        _channel.BasicNack(ea.DeliveryTag, false, true);
                        await Task.Delay(1000);
                    }
                }
            };
            _consumer.Received += _receivedAction;
            // this consumer tag identifies the subscription when it has to be cancelled
            _consumerTag = _channel.BasicConsume(
                queue: _nameQueue,
                autoAck: false,
                consumer: _consumer);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    // отписываемся от события
                    if (_receivedAction != null && _consumer != null)
                    {
                        _consumer.Received -= _receivedAction;
                    }
                    // прерываем работу внутри таски. Например таймаут во время отправки сообщения
                    if (_cancelTokenSource != null)
                    {
                        _cancelTokenSource.Cancel();
                        _cancelTokenSource.Dispose();
                    }
                    // отменяем обработку сообщения находящимся в обработке в RMQ
                    if (!string.IsNullOrEmpty(_consumerTag))
                    {
                        _channel.BasicCancel(_consumerTag);
                    }

                    // закрываем уничтожаем канал
                    _channel.Close();
                    _channel.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~RabbitMqConsumer()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
