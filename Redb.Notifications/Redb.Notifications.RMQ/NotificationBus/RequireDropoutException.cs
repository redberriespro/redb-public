﻿using System;

namespace Redb.Notifications.RMQ.NotificationBus
{
    /// <summary>
    /// Ошибка, укзывающая на невозможность обработки сообщения и необходимости его удаления из очереди и сохранения в <see cref="IDropoutBox"/>.
    /// </summary>
    public class RequireDropoutException: Exception
    {
        public RequireDropoutException()
        {

        }

        public RequireDropoutException(string message, Exception innerException = null)
            : base(message, innerException)
        {

        }
    }
}
