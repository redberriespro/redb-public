﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Redb.Notifications.RMQ.NotificationBus
{
    /// <summary>
    /// Хранилище, в которое отправляются проблемные сообщения, которые не удалось решить.
    /// </summary>
    public interface IDropoutBox
    {
        /// <summary>
        /// Указывает на доступность хранилища.
        /// </summary>
        bool Enabled { get; }

        /// <summary>
        /// Сохраняет сообщение в хранилище.
        /// </summary>
        /// <param name="body">Тело сообщения.</param>
        /// <param name="headers">Заголовки сообщения.</param>
        /// <param name="exception"></param>
        /// <returns></returns>
        Task SaveMessageAsync(
                            byte[] body,
                            IDictionary<string, object> headers,
                            Exception exception,
                            CancellationToken cancellationToken);
    }

    /// <summary>
    /// Реализация <see cref="IDropoutBox"/>, основаная на сохранении сообщений в каталог файловой системы.
    /// </summary>
    internal class DropoutBox : IDropoutBox
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(DropoutBox));
        private readonly string? _directory;

        public bool Enabled { get; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="directory">Каталог для хранения сообщений.</param>
        public DropoutBox(string? directory)
        {
            if (directory != null)
            {
                if (Directory.Exists(directory))
                {
                    _directory = directory;
                    Enabled = true;
                }
                else
                {
                    _log.Info($"Dropout directory is defined but not exist. Dropout box is disabled. Defined directory - '{directory}'");
                }
            }
            else
            {
                _log.Info("Dropout directory is not defined. Dropout box is disabled.");
            }
        }

        public async Task SaveMessageAsync(
                            byte[] body,
                            IDictionary<string, object> headers,
                            Exception exception,
                            CancellationToken cancellationToken)
        {
            ThrowIfNull(body, nameof(body));
            ThrowIfNull(headers, nameof(headers));
            ThrowIfNull(exception, nameof(exception));

            var id = Guid.NewGuid().ToString();

            await File.WriteAllBytesAsync(
                        $"{_directory}{Path.DirectorySeparatorChar}{id}.bin",
                        body);

            var json = JsonConvert.SerializeObject(headers, Formatting.Indented);

            await File.WriteAllTextAsync(
                        $"{_directory}{Path.DirectorySeparatorChar}{id}.props",
                        json);

            await File.WriteAllTextAsync(
                        $"{_directory}{Path.DirectorySeparatorChar}{id}.exception",
                        exception.ToString());
        }

        private static void ThrowIfNull(object? argument, string argumentName)
        {
            if (argument is null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }
    }
}
