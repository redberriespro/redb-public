﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Redb.Notifications.RMQ.NotificationBus.Retrying
{
    /// <summary>
    /// Информация об исключении сообщений из указанной <see cref="Queue">очереди</see> по указанной <see cref="Reason">причине</see>
    /// </summary>
    internal class MessageDiscarding
    {
        private static readonly IReadOnlyDictionary<string, string> PropertyMap = new Dictionary<string, string>
        {
            { nameof(Queue), "queue" },
            { nameof(Exchange), "exchange" },
            { nameof(RoutingKeys), "routing-keys" },
            { nameof(Reason), "reason" },
            { nameof(Count), "count" },
            { nameof(Timestamp), "time" },
        };

        /// <summary>
        /// Название очереди, из которой было исключено сообщение.
        /// </summary>
        public string Queue { get; } = null!;

        /// <summary>
        /// Обменник, в который было опубликовано сообщение до того, как оно впервые было исключено из указанной <see cref="Queue">очереди</see> по указанной <see cref="Reason">причине</see>.
        /// </summary>
        public string Exchange { get; } = null!;

        /// <summary>
        /// Ключи маршрутизации (включая CC, но исключая BCC) сообщения до того, как оно впервые было исключено из указанной <see cref="Queue">очереди</see> по указанной <see cref="Reason">причине</see>.
        /// </summary>
        public IReadOnlyCollection<string> RoutingKeys { get; } = null!;

        /// <summary>
        /// Причина, по которой исключено сообщение.
        /// </summary>
        public string Reason { get; } = null!;

        /// <summary>
        /// Сколько раз это сообщение было исключено из указанной <see cref="Queue">очереди</see> по указанной <see cref="Reason">причине</see>.
        /// </summary>
        public long Count { get; }

        /// <summary>
        /// Когда это сообщение было исключено в первый раз из указанной <see cref="Queue">очереди</see> по указанной <see cref="Reason">причине</see>.
        /// </summary>
        public AmqpTimestamp Timestamp { get; }

        private MessageDiscarding(
                    string query,
                    string exchange,
                    IReadOnlyCollection<string> routingKeys,
                    string reason,
                    long count,
                    AmqpTimestamp timestamp)
        {
            Queue = query;
            Exchange = exchange;
            RoutingKeys = routingKeys;
            Reason = reason;
            Count = count;
            Timestamp = timestamp;
        }

        public static MessageDiscarding Parse(Dictionary<string, object> data)
        {
            ThrowIfNull(data, nameof(data));

            return new MessageDiscarding(
                    data[PropertyMap[nameof(Queue)]].AsString(),
                    data[PropertyMap[nameof(Exchange)]].AsString(),
                    data[PropertyMap[nameof(RoutingKeys)]].AsStrings(),
                    data[PropertyMap[nameof(Reason)]].AsString(),
                    data[PropertyMap[nameof(Count)]].AsLong(),
                    data[PropertyMap[nameof(Timestamp)]].AsAmqpTimestamp()
                );
        }

        public static IReadOnlyCollection<MessageDiscarding> Parse(List<object> data)
        {
            ThrowIfNull(data, nameof(data));

            return data.Cast<Dictionary<string, object>>().Select(x => Parse(x)).ToList();
        }

        private static void ThrowIfNull(object? argument, string argumentName)
        {
            if (argument is null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }
    }

    static class ValueExtensions
    {
        public static string AsString(this object val) => Encoding.UTF8.GetString((byte[])val);
        
        public static long AsLong(this object val) =>  (long)val;

        public static AmqpTimestamp AsAmqpTimestamp(this object val) => (AmqpTimestamp)val;

        public static string[] AsStrings(this object val) => ((List<object>)val).Select(x => x.AsString()).ToArray();
    }
}
