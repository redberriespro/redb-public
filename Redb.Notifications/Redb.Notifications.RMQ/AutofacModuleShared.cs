﻿using Autofac;
using Autofac.Core;
using Redb.Notifications.Core.Providers;
using Redb.Notifications.RMQ.Config;
using Redb.Notifications.RMQ.NotificationBus;
using Redb.Notifications.RMQ.Providers;
using Redb.Notifications.RMQ.Telemetry;
using System.Collections.Generic;

namespace Redb.Notifications.RMQ
{
    public class NotificationsQueueingProviderAutofacModuleShared : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<NotificationBusAutofacModuleShared>();

            builder.RegisterType<RabbitmqNotificationsQueueingProvider>().AsSelf().As<INotificationsQueueingProvider>();
        }
    }

    public class NotificationBusAutofacModuleShared : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RabbitMqMessageFactory>();
            builder.Register((c, p) => c.Resolve<RabbitMqMessageFactory>(
                new Parameter[]
                {
                    new NamedParameter("connection", p.TypedAs<IRabbitmqConnection>()),
                    new NamedParameter("provider", p.TypedAs<IRmqProvider>()),
                })).As<IRabbitMqMessageFactory>()
                   .SingleInstance();

            builder.RegisterType<RabbitMqServer>();
            builder.Register((c, p) => c.Resolve<RabbitMqServer>(
                new Parameter[]
                {
                    new NamedParameter("connectionConfig", p.TypedAs<IRabbitmqConnection>()),
                    new NamedParameter("provider", p.TypedAs<IRmqProvider>()),
                    new NamedParameter("routingKeys", p.TypedAs<IEnumerable<string>>()),
                })).As<IRabbitMqServer>().SingleInstance();

            builder.RegisterType<RabbitMqProducer>().As<IProducer>();
            builder.RegisterType<NotifyRabbitMqServer>().As<INotifyRabbitMqServer>();
            builder.Register((c, p) =>
            {
                var config = c.Resolve<INotifyRmqConfig>();

                return new DropoutBox(config.DropoutPath);
            }).As<IDropoutBox>()
              .SingleInstance();

            builder.RegisterType<Metrics>().As<IMetrics>().SingleInstance();
        }
    }
}
