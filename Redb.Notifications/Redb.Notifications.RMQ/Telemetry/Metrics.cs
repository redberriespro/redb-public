using System.Collections.Generic;
using System.Diagnostics.Metrics;

namespace Redb.Notifications.RMQ.Telemetry
{
    public interface IMetrics
    {
        public const string METRIC_NAME = "Notification.Rmq";

        void IncRetryCount(string queueName, string messageId);

        void IncDropoutCount(string queueName, string messageId);
    }

    internal class Metrics : IMetrics
    {
        private Counter<int> RetryCounter { get; }

        private Counter<int> DropoutCounter { get; }

        public string MetricName { get; }

        public Metrics(string meterName = IMetrics.METRIC_NAME)
        {
            var meter = new Meter(meterName);
            MetricName = meterName;

            RetryCounter = meter.CreateCounter<int>(
                "notification_rmq_queue_retry_count",
                description: "количество повторных попыток обработки сообщений из очереди");

            DropoutCounter = meter.CreateCounter<int>(
                "notification_rmq_queue_dropout_count",
                description: "количество сообщений, изъятых из очереди");
        }

        public void IncRetryCount(string queueName, string messageId)
        {
            RetryCounter.Add(
                            1, 
                            new KeyValuePair<string, object>("notification.rmq.queue.name", queueName),
                            new KeyValuePair<string, object>("notification.rmq.message.id", messageId));
        }

        public void IncDropoutCount(string queueName, string messageId)
        {
            DropoutCounter.Add(
                            1, 
                            new KeyValuePair<string, object>("notification.rmq.queue.name", queueName),
                            new KeyValuePair<string, object>("notification.rmq.message.id", messageId));
        }
    }
}
