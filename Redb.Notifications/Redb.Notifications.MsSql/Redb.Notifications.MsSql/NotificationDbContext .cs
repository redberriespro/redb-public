﻿using Microsoft.EntityFrameworkCore;
using Redb.Notifications.Core.Entities;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace Redb.Notifications.MsSql
{
    public class NotificationDbContext : DbContext, INotificationDbContext
    {
        private readonly string _connectionString;// = "Server=127.0.0.1,7610;Database=notifications-dev;User Id=sa;Password=sAsdasdijllf2342s3mkx3923x;MultipleActiveResultSets=True;";

        public NotificationDbContext()
        {
        }

        public NotificationDbContext(SqlServerConfig config)
        {
            _connectionString = config.ConnectionString;
        }

        public NotificationDbContext(DbContextOptions<NotificationDbContext> options, SqlServerConfig config)
            : base(options)
        {
            _connectionString = config.ConnectionString;
        }

        public DbSet<EmailMessageQueueEntity> EmailMessageQueue { get; set; }

        public DbSet<SmsMessageQueueEntity> SmsMessageQueue { get; set; }

        public DbSet<NotificationTemplateEntity> NotificationTemplates { get; set; }

        public Task SaveChangesAsync()
        {
            return SaveChangesAsync(CancellationToken.None);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            NotificationDbContextConfiguration.ConfigureModel(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
            {
                return;
            }

            if (string.IsNullOrEmpty(_connectionString))
            {
                throw new ConfigurationErrorsException("SqlServerConfig section is not provided properly. Autofac config wrong?");
            }

            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}
