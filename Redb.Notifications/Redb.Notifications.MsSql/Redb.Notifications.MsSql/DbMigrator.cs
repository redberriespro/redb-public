﻿using Microsoft.EntityFrameworkCore;

namespace Redb.Notifications.MsSql
{
    public class DbMigrator
    {
        private readonly NotificationDbContext _context;

        public DbMigrator(NotificationDbContext context)
        {
            _context = context;
        }

        public void Run()
        {
            _context.Database.Migrate();
        }
    }
}
