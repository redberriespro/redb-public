﻿using Microsoft.EntityFrameworkCore;
using Redb.Notifications.Core.Entities;

namespace Redb.Notifications.MsSql
{
    public class NotificationDbContextFactory : INotificationDbContextFactory
    {
        private readonly SqlServerConfig _config;
        private readonly DbContextOptions<NotificationDbContext> _options;

        public NotificationDbContextFactory(DbContextOptions<NotificationDbContext> options, SqlServerConfig config)
        {
            _config = config;
            _options = options;
        }

        public INotificationDbContext CreateContext() => new NotificationDbContext(_options, _config);
    }
}
