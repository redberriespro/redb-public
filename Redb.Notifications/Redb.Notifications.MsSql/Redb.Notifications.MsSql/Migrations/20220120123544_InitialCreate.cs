﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Redb.Notifications.MsSql.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "notifications_email_q",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    scheduled = table.Column<DateTime>(nullable: false),
                    sent = table.Column<DateTime>(nullable: true),
                    subject = table.Column<string>(nullable: true),
                    body = table.Column<byte[]>(maxLength: 31457280, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notifications_email_q", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "notifications_sms_q",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    scheduled = table.Column<DateTime>(nullable: false),
                    sent = table.Column<DateTime>(nullable: true),
                    text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notifications_sms_q", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "notifications_templates",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    subject = table.Column<string>(nullable: false),
                    body = table.Column<string>(nullable: false),
                    master_id = table.Column<string>(nullable: true),
                    is_html = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notifications_templates", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_notifications_email_q_scheduled_sent",
                table: "notifications_email_q",
                columns: new[] { "scheduled", "sent" });

            migrationBuilder.CreateIndex(
                name: "IX_notifications_sms_q_scheduled_sent",
                table: "notifications_sms_q",
                columns: new[] { "scheduled", "sent" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "notifications_email_q");

            migrationBuilder.DropTable(
                name: "notifications_sms_q");

            migrationBuilder.DropTable(
                name: "notifications_templates");
        }
    }
}
