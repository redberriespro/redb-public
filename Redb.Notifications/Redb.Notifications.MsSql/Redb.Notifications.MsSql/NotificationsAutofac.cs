using Autofac;
using Redb.Notifications.Core.Entities;

namespace Redb.Notifications.MsSql
{
    public class NotificationsAutofac: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<NotificationDbContext>()
                .AsSelf()
                .As<INotificationDbContext>();

            builder.RegisterType<NotificationDbContextFactory>()
                .As<INotificationDbContextFactory>();

            builder.RegisterType<DbMigrator>().AsSelf().SingleInstance();
        }
    }
}